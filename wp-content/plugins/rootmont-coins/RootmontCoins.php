<?php
/*
Plugin Name: Rootmont Coins
Plugin URI: http://rootmont.com
Description: Coin Management for Rootmont
Author: Roy Sivan
Version: 1.0.0
Author URI: http://roysivan.com
Textdomain: rootmont-coin
*/

namespace RootmontCoins;

require_once 'vendor/autoload.php';
require_once 'RootmontACF.php';

use RootmontCoins\API\RootmontAPI;

/**
 * Class RootmontCoins
 * @package RootmontCoins
 */
class RootmontCoins {

	use Singleton;

	public $api;

	/**
	 * RootmontCoins constructor.
	 */
	public function __construct() {
		$this->api = RootmontAPI::init();
		add_action( 'init', [ $this, 'register_coin_cpt' ] );
		add_action( 'save_post', [ $this, 'save_new_coin' ] );
	}

	/**
	 * Register Coins CPT
	 */
	public function register_coin_cpt() {
		$labels = array(
			'name'               => _x( 'Coins', 'post type general name', 'rootmont-coin' ),
			'singular_name'      => _x( 'Coin', 'post type singular name', 'rootmont-coin' ),
			'menu_name'          => _x( 'Coins', 'admin menu', 'rootmont-coin' ),
			'name_admin_bar'     => _x( 'Coin', 'add new on admin bar', 'rootmont-coin' ),
			'add_new'            => _x( 'Add New', 'Coin', 'rootmont-coin' ),
			'add_new_item'       => __( 'Add New Coin', 'rootmont-coin' ),
			'new_item'           => __( 'New Coin', 'rootmont-coin' ),
			'edit_item'          => __( 'Edit Coin', 'rootmont-coin' ),
			'view_item'          => __( 'View Coin', 'rootmont-coin' ),
			'all_items'          => __( 'All Coins', 'rootmont-coin' ),
			'search_items'       => __( 'Search Coins', 'rootmont-coin' ),
			'parent_item_colon'  => __( 'Parent Coins:', 'rootmont-coin' ),
			'not_found'          => __( 'No Coins found.', 'rootmont-coin' ),
			'not_found_in_trash' => __( 'No Coins found in Trash.', 'rootmont-coin' )
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Description.', 'rootmont-coin' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'coin' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'exclude_from_search' => true,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
		);
		register_post_type( 'coins', $args );
	}

	public function save_new_coin( $post_id ) {
		if ( wp_is_post_revision( $post_id ) || 'coins' !== get_post_type( $post_id ) ) {
			return;
		}

		$apiUrl = 'https://api.coinmarketcap.com/v2/listings/';

		if ( $current_cmp_id = get_field( 'coin_market_cap_id', $post_id ) ) {
			return;
		}

		$symbol = get_field( 'symbol', $post_id );

		$listings = wp_safe_remote_get( $apiUrl );
		$listings = wp_remote_retrieve_body( $listings );
		$listings = json_decode( $listings );

		$listings = $listings->data;

		foreach ( $listings as $listing ) {
			if ( strtoupper( $symbol ) === $listing->symbol ) {
				update_field( 'coin_market_cap_id', $listing->id, $post_id );
				continue;
			}
		}

	}

}
global $rootmontCoins;
$rootmontCoins = new RootmontCoins();