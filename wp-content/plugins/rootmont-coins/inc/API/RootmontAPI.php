<?php

namespace RootmontCoins\API;

use RootmontCoins\Singleton;


/**
 * Class RootmontAPI
 */
class RootmontAPI {

	use Singleton;

	private $version;
	private $rootmont_api;
	private $available_cats;

	/**
	 * RootmontAPI constructor.
	 */
	public function __construct() {
		// set API namespace
		$this->version = 'rootmont/v1';
        $this->rootmont_api = get_api_location();
		$this->s3_api = "https://s3.amazonaws.com/coin-logos/";

		add_action( 'rest_api_init', [ $this, 'rootmont_api_init' ] );

		$this->set_available_cats();
	}

	/**
	 * Set Available Categories - transient if possible
	 */
	private function set_available_cats() {
		if ( $cats = get_transient( 'rootmont_coin_categories' ) ) {
			$this->available_cats = $cats;
			$this->available_cats->transient = true;
		}
		$categories = wp_safe_remote_get( $this->rootmont_api . '/all-categories' );
		$categories = wp_remote_retrieve_body( $categories );
		$this->available_cats = json_decode( $categories );
		set_transient( 'rootmont_coin_categories', $this->available_cats, 60*60*11 );

	}

	public function validate_user() {
		$user = wp_get_current_user();
		return rcp_is_active( $user->ID );
	}

	/**
	 * Register API
	 */
	public function rootmont_api_init()
    {

        // all coins
        register_rest_route($this->version, '/coins/', [
            'methods' => 'GET',
            'callback' => [$this, 'get_all_coins']
        ]);

        // search-coins
        register_rest_route($this->version, '/coin-search/', array(
            'methods' => 'GET',
            'callback' => [$this, 'search_coins']
        ));

        // coin-info
        register_rest_route($this->version, '/coin-info/', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_coins'],
            'permission_callback' => function () {
                return $this->validate_user();
            }
        ));

        // coin-info/<symbol>
        register_rest_route($this->version, '/current-stats/', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_current_stats_wp'],
            'args' => [
                'name' => [
                    'required' => true,
                ],
            ]
        ));


        // coin-catalog/category
        register_rest_route($this->version, '/coin-catalog/', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_coin_catalog'],
            'permission_callback' => function () {
                return true;
            }
        ));

        register_rest_route($this->version, '/coin-catalog/(?P<category>\w+)/', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_coin_catalog'],
            'permission_callback' => function () {
                return $this->validate_user();
            }
        ));

        // coin-catalog/category/term
        register_rest_route($this ->version, '/coin-catalog/(?P<category>\w+)/(?P<term>\w+)', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_coin_catalog'],
            'permission_callback' => function () {
                return $this->validate_user();
            }
        ));

        // metcalfe-chart/
        register_rest_route($this->version, '/metcalfe-chart/(?P<symbol>\w+)/', array(
            'methods' => 'GET',
            'args' => [
                'symbol' => [
                    'required' => true,
                ]
            ],
            'callback' => [$this, 'get_metcalfe_chart'],
//            'permission_callback' => function () {
//                return $this->validate_user();
//            }
        ));

        // price-chart/
        register_rest_route($this->version, '/price-chart/(?P<symbol>\w+)/', array(
            'methods' => 'GET',
            'args' => [
                'symbol' => [
                    'required' => true,
                ]
            ],
            'callback' => [$this, 'get_price_chart'],
//            'permission_callback' => function () {
//                return $this->validate_user();
//            }
        ));


        register_rest_route($this->version, '/prices/', array(
            'methods' => 'GET',
            'callback' => [$this, 'get_prices']
        ));

        // logo/
        register_rest_route($this->version, '/logo/(?P<symbol>\w+)/', array(
            'methods' => 'GET',
            'args' => [
                'symbol' => [
                    'required' => true,
                ]
            ],
            'callback' => [$this, 'get_logo']
        ));


        // coin-benchmark/
        register_rest_route($this->version, '/coin-benchmark/(?P<type>\w+)/', array(
            'methods' => 'GET',
            'args' => [
                'type' => [
                    'required' => true,
                ],
            ],
            'callback' => [$this, 'get_benchmark_data'],
            'permission_callback' => function () {
                return $this->validate_user();
            }
        ));
    }

	public function search_coins( \WP_REST_Request $request ) {

		$data = $request->get_params();
		$term = isset( $data['term'] ) ? $data['term'] : false;

		if ( ! $term ) {
			return new \WP_Error( 'response-error', [ 'message' => 'missing search term' ], array( 'code' => 401 ) );
		}

		if ( $this->validate_user() ) {
			$args = [
				'post_type' => ['post', 'coins'],
				's'         => $term
			];
		} else {
			$args = [
				's' => $term
			];
		}

		$search = new \WP_Query( $args );

		$response = [
			'valid_user' => $this->validate_user(),
			'results' => $search->have_posts() ? $search->posts : false,
		];

		if ( ! $response['results'] ) {
			$response['args'] = $args;
		}

		if ( is_array( $response['results'] ) ) {
			$response['autocomplete'] = [];
			foreach( $response['results'] as $key => $value ) {
				if ( ! $response['valid_user'] && 'post' !== get_post_type( $value->ID ) ) {
					continue;
				}
				$response['autocomplete'][] = [
					'id' => $value->ID,
					'label' => $value->post_title,
					'value' => $value->post_title,
					'url' => get_permalink( $value->ID )
				];
			}
		}

		return new \WP_REST_Response( $response );
	}

	/**
	 * Get Coin Info
	 * /coin-symbol/symbol/<symbol>
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function get_coin_info( \WP_REST_Request $request ) {

		$data = $request->get_params();
//
//		if (
//			( isset( $data['cache_bust'] ) && ! $data['cache_bust'] ) &&
//			$trans = get_transient( '_coin_data_' . $data['symbol'] )
//		) {
//			$trans['transient'] = true;
//			return new \WP_REST_Response( $trans );
//		}

		$api_url = $this->rootmont_api . '/coin-report/' . $data['symbol'];

		$response = wp_remote_get( $api_url, [
			'timeout' => 10000
		] );

		if ( is_wp_error( $response ) ) {
			return new \WP_Error( 'response-error', array( 'message' => 'coin-info/symbol ' . $response->get_error_message() ), array( 'code' => 400 ) );
		}

		$response = json_decode( $response['body'] );

		if ( ! $response ) {
			return new \WP_Error( 'response-json-error', array( 'message' => 'coin-info/symbol - not JSON' ), array( 'response' => $response, 'code' => 400 ) );
		}

		$coin_data = [
			'symbol'     => $data['symbol'],
			'logo'       => $this->s3_api . strtoupper( $data['symbol'] ) . '.png',
			'current_stats' => $this->get_current_stats( $data['name'] ),
			'coin_info'  => $response
		];

		// Set Transient for 48 hours.
		set_transient( '_coin_data_' . $data['symbol'], $coin_data, 60*60*6 );

		$coin_data['transient'] = false;

		return new \WP_REST_Response( $coin_data );
	}

    public function get_current_stats_wp( \WP_REST_Request $request ) {
        $data = $request->get_params();
        return $this->get_current_stats($data['name']);
    }

        /**
	 * Get Market Cap Info from API
	 *
	 * @param bool $id
	 *
	 * @return array|bool|mixed|object
	 */
	private function get_current_stats( $name ) {
		$response =  wp_remote_retrieve_body( wp_remote_get( get_api_location() . '/current-stats/' . $name ) );
		$data = json_decode( $response );
		$data->price = number_format( $data->price, 2, '.', ',' );
		$data->volume = $this->nice_number( $data->volume );
		$data->supply = $this->nice_number( $data->supply );
        $data->marketcap = $this->nice_number( $data->marketcap );
		return $data;
	}

    private function nice_number($number) {
        // first strip any formatting;
        //$number = (0+str_replace(",", "", $number));

        // is this a number?
        if (!is_numeric($number)) return false;

        // now filter it;
        if ($number > 1000000000000) return round(($number/1000000000000), 2).' T';
        elseif ($number > 1000000000) return round(($number/1000000000), 2).' B';
        elseif ($number > 1000000) return round(($number/1000000), 2).' M';
        elseif ($number > 1000) return round(($number/1000), 2).' K';

        return number_format($number);
    }

	/**
	 * Get All Coins
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_REST_Response
	 */
	public function get_coins( \WP_REST_Request $request ) {
		$data = $request->get_params();
		$response = $data;

		$args = [
			'post_type' => 'coins',
			'nopaging'  => true,
		];

		$coins = new \WP_Query( $args );
		$response = [];
		global $post;
		while ( $coins->have_posts() ) : $coins->the_post();
			$symbol = get_field( 'symbol', $post->ID );
			$cap_id = get_field( 'coin_market_cap_id', $post->ID );
			$coin_request = new \WP_REST_Request( 'GET', '/rootmont/v1/coin-info/' );
			$coin_request->set_query_params([
                'name' => get_the_title()
			]);
			$coin_response = $this->get_coin_info( $coin_request );
			$response[] = $coin_response->data;
		endwhile;


		return new \WP_REST_Response( $response );
	}

	public function get_coin_catalog( \WP_REST_Request $request ) {
		$data = $request->get_params();
		$response = $data;
		$data['term'] = isset( $data['term'] ) ? str_replace( '_', '%20', $data['term'] ) : '';
		$response['all_coin_cats'] = $this->available_cats;

		$data['category'] = ! isset( $data['category'] ) ? 'global' : $data['category'];
		$data['category'] = ( 'industry' === $data['category'] ) ? 'industries' : $data['category'];

		$url = $this->rootmont_api . '/catalog/';

		if ( 'global' === $data['category'] ) {
			$url .= 'global';
		}

		// If category and a term are set
		if ( isset( $data['category'] ) && ! empty( $data['term'] ) ) {
			// Check if term is part of category
			$response['cat_available'] = 'industry' !== $data['category'] ? $this->available_cats->{$data['category']} : $this->available_cats->industries;

			if ( ! is_array( $response['cat_available'] ) ) {
				$response['cat_available'] = (array) $response['cat_available'];
			}

			$term = str_replace( '%20', ' ', $data['term'] );
			$response['cat_available'] = in_array( $term, $response['cat_available'], true );

			if ( ! $response['cat_available'] ) {
				return new \WP_Error( 400, 'Term not available', [ 'cat' => $data['category'], 'term' => $data['term'], 'available' => ( 'industry' !== $data['category'] ) ? $this->available_cats->{$data['category']} : $this->available_cats->industries ] );
			}

			$url .= $data['category'] . '/' . $data['term'];
		}

		$response['url'] = $url;

		$get_response = wp_safe_remote_get( $url, [
			'timeout' => 10000
		]);

		if ( is_wp_error( $get_response ) ) {
			return new \WP_Error( 400, $get_response->get_error_message(), $get_response );
		}

		$get_response = json_decode( wp_remote_retrieve_body( $get_response ) );

		if ( ! is_array( $get_response ) || empty( $get_response ) ) {
			return new \WP_Error( 400, 'Empty Response', [ 'url' => $url, 'response' => $get_response ] );
		}

		foreach( $get_response as $key => $coin ) {
			$get_response[ $key ]->permalink = $this->get_coin_by_symbol( $coin->symbol );
		}

		$response['total_coins'] = count( $get_response );
		$response['catalog'] = $get_response;
		set_transient( '_coin_catalog_' . $data['category'] . '_' . $data['term'], $get_response, 60*60*48 );
		$response['trans'] = false;

		return new \WP_REST_Response( $response );
	}

	private function get_coin_by_symbol( $symbol ) {
		global $wpdb;
		$table = $wpdb->prefix . 'postmeta';

		$query = $wpdb->get_results($wpdb->prepare(
			"SELECT * FROM $table WHERE meta_key = 'symbol' AND meta_value LIKE %s",
			$symbol
		));

		if ( ! $query || empty( $query ) || ! $query[0]->post_id ) {
			return false;
		}

		return get_permalink( $query[0]->post_id );
	}

	/**
	 * Get Metcalfe Chart Data (for Coin Template)
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function get_metcalfe_chart( \WP_REST_Request $request ) {
		$data = $request->get_params();

		$symbol = $data['symbol'];

		// if ( ! in_array( $chart, $charts, true ) ) {
		// 	return new \WP_Error( 'wrong chart', __( 'No Such Chart Exists' ), [ 'chart' => $chart, 'symbol' => $symbol ] );
		// }

        $url = $this->rootmont_api . '/metcalfe-charts/' . strtoupper( $symbol );
		$chartData = wp_safe_remote_get( $url, [
			'timeout' => 1000
		] );
		$chartData = wp_remote_retrieve_body( $chartData );
		$chartResponse = json_decode( $chartData );
		$data = $chartResponse;

		if ( ! $data ) {
			return new \WP_Error( 'no data', __( 'No Chart Data' ), [ 'data' => $data, 'url' => $url, 'response' => $chartResponse ] );
		}

		return new \WP_REST_Response( $data );
	}

    /**
     * Get Price Chart Data (for Coin Template)
     *
     * @param \WP_REST_Request $request
     *
     * @return \WP_Error|\WP_REST_Response
     */
    public function get_price_chart( \WP_REST_Request $request ) {
        $data = $request->get_params();

        $symbol = $data['symbol'];

        // if ( ! in_array( $chart, $charts, true ) ) {
        // 	return new \WP_Error( 'wrong chart', __( 'No Such Chart Exists' ), [ 'chart' => $chart, 'symbol' => $symbol ] );
        // }

        $url = $this->rootmont_api . '/price-charts/' . strtoupper( $symbol );
        $chartData = wp_safe_remote_get( $url, [
            'timeout' => 1000
        ] );
        $chartData = wp_remote_retrieve_body( $chartData );
        $chartResponse = json_decode( $chartData );
        $data = $chartResponse;

        if ( ! $data ) {
            return new \WP_Error( 'no data', __( 'No Chart Data' ), [ 'data' => $data, 'url' => $url, 'response' => $chartResponse ] );
        }

        return new \WP_REST_Response( $data );
    }


    /**
	 * Get Benchmark Data
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_REST_Response
	 */
	public function get_benchmark_data( \WP_REST_Request $request ) {

		$data = $request->get_params();

		$url = $this->rootmont_api . '/benchmark/' . $data['type'];
		$response = wp_safe_remote_get( $url, [
			'timeout' => 10000
		]);

		if ( is_wp_error( $response ) ) {
			return new \WP_Error( 'get benchmark', $response->get_error_message(), [ 'data' => $data ] );
		}

		$data['response'] = json_decode( wp_remote_retrieve_body( $response ) );

		return new \WP_REST_Response( $data );
	}


	public function get_all_coins( \WP_REST_Request $request ) {

		// All Possible Coins
		$args = [ 'post_type' => 'coins', 'nopaging' => true ];
		$query = new \WP_Query( $args );
		return new \WP_REST_Response( $query->posts  );
	}

	 public function get_logo( \WP_REST_Request $request ) {
		 $data = $request->get_params();

		 $symbol = $data['symbol'];

		 $url = $this->s3_api . strtoupper( $symbol ) . '.png';

		 $response = wp_safe_remote_get( $url, [
			 'timeout' => 1000
		 ] );

		 if ( is_wp_error( $response ) ) {
			 return new \WP_Error( 'no logo', $response->get_error_message(), [ 'trans' => $trans, 'data' => $data, 'url' => $url ] );
		 }

		 $body =  wp_remote_retrieve_body( $response );
		 //$b64 = base64_encode($body);
		 header('content-type: image/png');
		 $png = imagecreatefromstring($body);
		 imagepng($png);
		 //return new \WP_REST_Response( $img );

	 }

    public function get_prices( \WP_REST_Request $request ) {
        $url = $this->rootmont_api . '/prices';
        $response = wp_safe_remote_get( $url, [
            'timeout' => 1000
        ] );
        if ( is_wp_error( $response ) ) {
            return new \WP_Error( 'no logo', $response->get_error_message(), [ 'url' => $url ] );
        }

        $data['response'] = json_decode( wp_remote_retrieve_body( $response ) );

        return new \WP_REST_Response( $data );

     }
}

function get_api_location() {
    if ($_ENV["PANTHEON_ENVIRONMENT"] == "dev") {
        $api_location = "http://dev.rootmont.io";
    } else if ($_ENV["PANTHEON_ENVIRONMENT"] == "test") {
        $api_location = "http://staging.rootmont.io";
    } else {
        $api_location = "https://rootmont.io";
    }

    return $api_location;
}
