<?php
/*
Plugin Name: Rootmont Dashboard
Plugin URI: https://rootmont.com
Description: v2 of the Rootmont Dashboard
Version: 2.0
Author: Rootmont Engineering Team
Author URI: https://rootmont.com
Text Domain: rootmont-dashboard
Domain Path: /languages
*/

namespace RootmontDashboard;

require_once 'vendor/autoload.php';

use RootmontDashboard\Scripts;
use RootmontDashboard\API;
use RootmontDashboard\Dashboard;

define( 'ROOTMONT_DASH_URL', plugin_dir_url( __FILE__ ) );
define( 'ROOTMONT_DASH_DIR', plugin_dir_path( __FILE__ ) );

class RootmontDashboard {

	public $dashboard;
	public function __construct() {
		new Scripts();
		new API();

		$this->dashboard = Dashboard::init();
	}

}

global $rootmontDashboard;
$rootmontDashboard = new RootmontDashboard();