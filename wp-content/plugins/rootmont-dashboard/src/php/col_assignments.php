<?php
global $assignment_col_category;
$assignment_col_category = [
	// development
	'github_contributions' => 'development',
	'github_contributors'  => 'development',
	'github_forks'         => 'development',
	'github_open_issues'   => 'development',
	'github_stars'         => 'development',
	'github_stargazers'    => 'development',

	// price performance
	'alpha'                => 'price-performance',
	'beta'                 => 'price-performance',
	'cov'                  => 'price-performance',
	'downside'             => 'price-performance',
	'marketcap'            => 'price-performance',
	'r-squared'            => 'price-performance',
	'sharpe'               => 'price-performance',
	'treynor'              => 'price-performance',
	'up/Down'              => 'price-performance',
	'upside'               => 'price-performance',

	// social
	'bitcointalk_posts'    => 'social',
	'reddit_members'       => 'social',
	'hype'                 => 'social',
	'total_tweets'         => 'social',
	'twitter_followers'    => 'social',
	'twitter_leadership'   => 'social',

	// usage
	'daily_transactions'   => 'usage',
	'metcalfe_correlation' => 'usage',
	'metcalfe_exponent'    => 'usage',
	'metcalfe_proportion'  => 'usage',
	'normalized_metcalfe'  => 'usage',
	'raw_metcalfe'         => 'usage',
	'trading_volume'       => 'usage',
];