<?php

namespace RootmontDashboard;

global $assignment_col_category;
require_once ROOTMONT_DASH_DIR . 'src/php/col_assignments.php';

/**
 * Class Dashboard
 *
 * THIS IS THE v1 of the Dashboard all PHP & Admin AJAX
 *
 * @package RootmontDashboard
 */
class Dashboard {

	use Singleton;

	public $user_data;

	private $cols;
	private $coins;

	private $movers_data;

	private $api_location;

	public function __construct() {
		// Load Dash Data
		add_action( 'init', [ $this, 'load_dash_data'], 10 );
		// Body Class
		add_filter( 'body_class', [ $this, 'dash_body_class'], 10, 1 );
		// Save Columns AJAX
		add_action( 'wp_ajax_save_cols', [ $this, 'dash_save_cols'], 10 );
		add_action( 'wp_ajax_nopriv_save_cols', [ $this, 'dash_save_cols'], 10 );
		// Save Coins AJAX
		add_action( 'wp_ajax_save_coins', [ $this, 'dash_save_coins'], 10 );
		add_action( 'wp_ajax_nopriv_save_coins', [ $this, 'dash_save_coins'], 10 );
		$this->api_location = get_api_location();
	}

	/**
	 * Dash Body Class
	 *
	 * @param $classes
	 *
	 * @return array
	 */
	public function dash_body_class( $classes ) {

		if ( is_page( 'dashboard' ) ) {
			$classes[] = 'user-dashboard';
		}


		return $classes;
	}

	/**
	 * Load Initial Data
	 */
	public function load_dash_data() {
		// All User Data Keys
		$this->user_data = [
			'_rootmont_dashboard_top_coins',
			'_rootmont_dashboard_top_cols',
			'_rootmont_dashboard_price_movers',
			'_rootmont_dashboard_ntx_movers',
		];

		// All Possible Columns
		$url = "$this->api_location/master-table/columns";
		$cols = wp_safe_remote_get( $url, [
			'timeout' => 2000
		]);
		$cols = wp_remote_retrieve_body( $cols );
		$this->cols = json_decode( $cols );

//		// All Possible Coins
//		$args = [ 'post_type' => 'coins', 'nopaging' => true, 'order' => 'title', 'orderby' => 'ASC' ];
//		$query = new \WP_Query( $args );
//		$this->coins = $query->posts;

        // All Possible Coins
        $url = "$this->api_location/master-table/rows";
        $coins = wp_safe_remote_get( $url, [
            'timeout' => 2000
        ]);
        $coins = wp_remote_retrieve_body( $coins );
        $this->coins = json_decode( $coins );


        $this->setup_mover_data();

	}

	/**
	 * Get User Meta for each $this->>user_data
	 *
	 * @param $user_id
	 *
	 * @return array
	 */
	public function get_user_info( $user_id ) {

		$user_data = [];
		foreach ( $this->user_data as $meta_key ) {
			$user_data[ $meta_key ] = get_user_meta( $user_id, $meta_key, true );
		}

		if ( ! $user_data['_rootmont_dashboard_top_coins'] ) {
			$user_data['_rootmont_dashboard_top_coins'] = [
				'Ethereum',
				'Bitcoin',
				'Ripple',
				'Bitcoin Cash',
				'Litecoin',
			];
			update_user_meta( $user_id, '_rootmont_dashboard_top_coins', $user_data['_rootmont_dashboard_top_coins'] );
		}

		if ( ! $user_data['_rootmont_dashboard_top_cols'] ) {
			$user_data['_rootmont_dashboard_top_cols'] = [
				'alpha',
				'beta',
				'marketcap',
				'sharpe',
			];
			update_user_meta( $user_id, '_rootmont_dashboard_top_cols', $user_data['_rootmont_dashboard_top_cols'] );
		}

		return $user_data;
	}

	/**
	 * Get Coin Table Data
	 *
	 * @param $coins
	 * @param $cols
	 *
	 * @return array|mixed|object|string|\WP_Error
	 */
	public function get_coin_table_data( $coins, $cols ) {

		if ( ! is_array( $coins ) || ! is_array( $cols ) ) {
			return new \WP_Error( 'get_coin_table_data', __( 'Coins or Cols not array ' ) );
		}

		$coin_url = '[';
		foreach ( $coins as $key => $coin ) {
			$coin_url .= '"' . $coin . '"';
			if ( $key < count( $coins ) - 1 ) {
				$coin_url .= ',';
			}
		}
		$coin_url .= ']';

		$col_url = '[';
		foreach ( $cols as $key => $col ) {
			$col_url .= '"' . $col . '"';
			if ( $key < count( $cols ) - 1 ) {
				$col_url .= ',';
			}
		}
		$col_url .= ']';

		$url = "$this->api_location/master-table/?rows=$coin_url&cols=$col_url";

		$response = wp_safe_remote_get( $url, [
			'timeout' => 4000
		]);

		$data = wp_remote_retrieve_body( $response );
		$data = json_decode( $data );
		return $data;
	}

	/**
	 * Return Cols
	 *
	 * @return mixed
	 */
	public function get_user_cols() {
		return $this->cols;
	}

	/**
	 * Save Columns - AJAX callback
	 */
	public function dash_save_cols() {
		$user = wp_get_current_user();
		if ( ! isset( $_POST, $_POST['cols'] ) || ! $user->ID ) {
			echo json_encode([
				'error' => true,
				'error_message' => 'Missing Columns or No User',
			]);
		}

		$cols = $_POST['cols'];

		if ( ! is_array( $cols ) ) {
			echo json_encode([
				'error' => true,
				'error_message' => 'Cols not array',
			]);
		}

		$cols = array_map( 'esc_attr', $cols );

		echo json_encode([
			'error' => false,
			'user' => $user->ID,
			'cols' => $cols,
			'update' => update_user_meta( $user->ID, '_rootmont_dashboard_top_cols', $cols ),
		]);

		die();
	}

	/**
	 * Return Coins
	 *
	 * @return mixed
	 */
	public function get_user_coins() {
		return $this->coins;
	}

	/**
	 * Save Coin - AJAX callback
	 */
	public function dash_save_coins() {
		$user = wp_get_current_user();
		if ( ! isset( $_POST, $_POST['coins'] ) || ! $user->ID ) {
			echo json_encode([
				'error' => true,
				'error_message' => 'Missing Columns or No User',
			]);
		}

		$coins = $_POST['coins'];

		if ( ! is_array( $coins ) ) {
			echo json_encode([
				'error' => true,
				'error_message' => 'Cols not array',
			]);
		}

		$coins = array_map( 'esc_attr', $coins );

		echo json_encode([
			'error' => false,
			'user' => $user->ID,
			'coins' => $coins,
			'update' => update_user_meta( $user->ID, '_rootmont_dashboard_top_coins', $coins ),
		]);

		die();
	}

    /**
     * Get all movers data
     *
     * @return int
     */
    public function get_mover_data( \WP_REST_Request $request ) {

        $data = $request->get_params();
        $days = $data['days'];


        $url = "$this->api_location/movers/" . $days . "/10";
        $request = wp_safe_remote_get( $url, [
            'timeout' => 3000
        ]);

        if ( is_wp_error( $request ) ) {
            $this->movers_data = $request;
            $this->price_movers = $request;
            return false;
        }

        $data = wp_remote_retrieve_body( $request );
        $data = json_decode( $data );

        return $data;
    }


    /**
	 * Setup all movers data
	 *
	 * @return bool
	 */
	private function setup_mover_data() {
        $url = "$this->api_location/movers/7/10";
        $request = wp_safe_remote_get( $url, [
            'timeout' => 3000
        ]);

        if ( is_wp_error( $request ) ) {
            $this->movers_data = $request;
            $this->price_movers = $request;
            return false;
        }

        $data = wp_remote_retrieve_body( $request );
        $data = json_decode( $data );

        $this->movers_data = $data;

	}

	/**
	 * Get Price Movers Data
	 *
	 * @return mixed
	 */
	public function get_price_movers_data() {
		return [
			'winners' => (array) $this->movers_data->{ 'price_winners' },
			'losers' => (array) $this->movers_data->{ 'price_losers' },
			'active' => (array) $this->movers_data->{ 'price_active' }
		];
	}

	public function get_ntx_movers_data() {
		return [
			'winners' => (array) $this->movers_data->{ 'ntx_winners' },
			'losers' => (array) $this->movers_data->{ 'ntx_losers' },
			'active' => (array) $this->movers_data->{ 'ntx_active' }
		];
	}

	/**
	 * Favorite Column Options - Get column options based on group
	 *
	 * @param $all_cols
	 * @param string $group
	 *
	 * @return string
	 */
	public function favorite_column_options( $all_cols, $group = '' ) {
		if ( ! $group ) { return false; }
		global $user_data, $assignment_col_category;
		$options = '';
		$cols = $user_data['_rootmont_dashboard_top_cols'];

		$group_label = str_replace( '-', ' ', $group );
		$group_label = ucwords( $group_label );

		$options .= '<optgroup label="' . $group_label . '">';

		foreach( $all_cols as $key => $col ) {
			if ( in_array( $col, $cols,false ) ) { continue; }
			$options .= $assignment_col_category[ $col ] === $group ? '<option value="' . $col . '"> ' . ucwords( str_replace( '_', ' ', $col ) ) . '</option>' : '';
		}

		$options .= '</optgroup>';

		return $options;
	}

}

