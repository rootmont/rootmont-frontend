<?php

namespace RootmontDashboard;


class Charts {

	use Singleton;

	private $user;
	private $colors;
    private $api_location;

	public function __construct() {
		$this->colors = [
			'#1c3650',
			'#d0924a',
			'#808080',
			'#7b8a9a',
		];
        $this->api_location = get_api_location();
	}

	/**
	 * Get Charts Data
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function get_charts( \WP_REST_Request $request ) {

		$current_user = wp_get_current_user();
		if ( ! $current_user ) {
			return new \WP_Error( 'get_charts', 'No Charts', [ 'user' => $current_user ] );
		}
		$this->user = $current_user;

		$charts = get_user_meta( $current_user->ID, '_rootmont_dashboard_charts', true );

		if ( $charts ) {
			$charts = (array) $charts;
		}

		if ( ! $charts || ! is_array( $charts ) ) {
			$coins_meta = get_user_meta( $current_user->ID, '_rootmont_dashboard_top_coins', true );
			$coins = [];
			foreach( $coins_meta as $coin ) {
				$coin_obj = get_page_by_title( $coin, OBJECT, 'coins' );
				$coins[] = $coin_obj->ID;
			}

			$charts = [
				[
					'type'  => 'line',
					'data'  => [ 'mcap' ],
					'coins' => $coins,
					'width' => 'full-width',
					'uid'   => $this->uid_generator(),
				],
			];
		}

		if ( is_array( $charts ) && ! empty( $charts ) ) {
			foreach( $charts as $key => $chart ) {
				$charts[$key]['chart_data'] = $this->get_chart_data( $chart );
				$charts[$key]['chartjs_data'] = $this->_chart_data_transform( $chart['type'], $charts[$key]['chart_data'], $charts[$key]['data'] );
			}
		} else {
			$charts = false;
		}

		$response_data = [
			'charts' => $charts ?: [],
		];

		return new \WP_REST_Response( $response_data );
	}

	/**
	 * Save Chart to user meta
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_Error|\WP_REST_Response
	 */
	public function save_chart( \WP_REST_Request $request ) {

		$data = $request->get_params();

		$current_user = wp_get_current_user();
		if ( ! $current_user ) {
			return new \WP_Error( 'get_charts', 'No Charts', [ 'user' => $current_user ] );
		}

		$chart = $data['chart'];

		$editing = (bool) $chart['editing'];
		unset( $chart['editing'] );

		$currentCharts = get_user_meta( $current_user->ID, '_rootmont_dashboard_charts', true );

		if ( ! $editing ) {
			if ( ! $currentCharts ) {
				$currentCharts = [ $chart ];
			} else {
				$currentCharts[] = $chart;
			}
		} else {
			foreach( $currentCharts as $key => $currentChart ) {
				if ( $chart['uid'] === $currentChart['uid'] ) {
					$currentCharts[$key] = $chart;
				}
			}
		}

		$update = update_user_meta( $current_user->ID, '_rootmont_dashboard_charts', $currentCharts );

		$response_data = [
			'update_success' => $update,
			'new_chart'      => $chart,
			'charts'         => get_user_meta( $current_user->ID, '_rootmont_dashboard_charts', true ),
		];
		return new \WP_REST_Response( $response_data );
	}

	/**
	 * Get Chart Data
	 *
	 * @param $chart
	 *
	 * @return array|bool|mixed|object|string
	 */
	private function get_chart_data( $chart ) {

		$coins = $chart['coins'];

		if ( ! $coins ) {
			return false;
		}

		if ( 'bar' === $chart['type'] ) {
			$coin_url = '[';
			foreach ( $coins as $key => $coin_id ) {
				$coin = get_the_title( $coin_id );
				$coin_url .= '"' . $coin . '"';
				if ( $key < count( $coins ) - 1 ) {
					$coin_url .= ',';
				}
			}
			$coin_url .= ']';

			if ( ! is_array( $chart['data'] ) ) {
				$chart_data = $this->_get_col( $chart['data'] );
				$col_url = '[' . $chart_data . ']';
			} else {
				$col_url = '[';
				foreach ( $chart['data'] as $key => $col ) {
					$col = $this->_get_col( $col );
					$col_url .= '"' . $col . '"';
					if ( $key < count( $chart['data'] ) - 1 ) {
						$col_url .= ',';
					}
				}
				$col_url .= ']';
			}

            $url = "$this->api_location/master-table/?rows=$coin_url&cols=$col_url";

			if( defined( 'WP_ENV' ) && 'LOCAL' === WP_ENV && get_transient( $url ) ) {
				$response = get_transient( $url );
			} else {
				$response = wp_safe_remote_get( $url, [
					'timeout' => 1000
				]);
			}

			$data = wp_remote_retrieve_body( $response );
			$data = json_decode( $data );

		} elseif ( 'line' === $chart['type'] ) {
			$coin_url = '';
			foreach ( $coins as $key => $coin_id ) {
				$coin = get_field( 'symbol', $coin_id );
				$coin_url .= $coin;
				if ( $key < count( $coins ) - 1 ) {
					$coin_url .= ',';
				}
			}

			$urls = [];
			foreach( $chart['data'] as $key => $data ) {
                $base_url = "$this->api_location/";
				switch( $data ){
					case 'mcap' :
						$urls[] = $base_url . 'mcap/?symbols=' . $coin_url;
						break;
					case 'ntx' :
						$urls[] = $base_url . 'ntx/?symbols=' . $coin_url;
						break;
					case 'price' :
						$urls[] = $base_url . 'price/?symbols=' . $coin_url;
						break;
					case 'vol' :
						$urls[] = $base_url . 'vol/?symbols=' . $coin_url;
						break;

				}
			}

			$data = [];

			foreach( $urls as $line_url ) {
				if( defined( 'WP_ENV' ) && 'LOCAL' === WP_ENV && get_transient( $line_url ) ) {
					$response = get_transient( $line_url );
				} else {
					$response = wp_safe_remote_get( $line_url, [
						'timeout' => 1000
					]);
				}

				$data_response = wp_remote_retrieve_body( $response );
				$data[] = json_decode( $data_response );
			}
		}

		$data = [
			'chart_data' => $data ?: false,
		];
        // only populate this on dev bc we don't want frontend to know about rootmont.io
        if ($_ENV["PANTHEON_ENVIRONMENT"] == "dev") {
            $data->url = isset($url) ? $url : $urls;
        }
		return $data;
	}

	/**
	 * Convert Chart Data to Column Data for Rootmont API call
	 *
	 * @param $col
	 *
	 * @return bool|string
	 */
	private function _get_col( $col ) {
		switch( $col ) {
			case 'upside-downside' :
				return '"up/Down"';
			break;

		}
		return $col;
	}

	/**
	 * Chart Data to ChartJS Data Transform
	 *
	 * @param $type
	 * @param $data
	 *
	 * @return array
	 */
	private function _chart_data_transform( $type, $data, $dataTypes ) {
		$chartData = [];
		if ( ! is_array( $dataTypes ) ) {
			$dataTypes = [ $dataTypes ];
		}
		switch( $type ) {
			case 'bar':
				$data['chart_data'] = (array) $data['chart_data'];
				$chartData['labels'] = [];
				$chartData['datasets'] = [];

				foreach ( $dataTypes as $dataKey => $dataType ) {

					$chartData['datasets'][$dataKey] = [
						'backgroundColor' => $this->colors[$dataKey],
						'data' => [],
					];

					foreach ( $data['chart_data'] as $key => $value ) {
						if ( ! in_array( $key, $chartData['labels'], true ) ) {
							$chartData['labels'][] = $key;
						}
						$value = (array) $value;
						$chartData['datasets'][$dataKey]['data'][] = $value[ $dataType ];
					}

					// Social Hype Title
					if ( 'hype' === $dataType ) {
						$dataType = 'Social Hype';
					}
					// Up / Down Title
					if ( 'up_down' === $dataType ) {
					    $dataType = 'Upside / Downside';
                    }

					$chartData['datasets'][$dataKey]['label'] = ucwords( str_replace( '_', ' ', $dataType ) );
				}

			break;

			case 'line':
				$chartData['datasets'] = [];
				foreach( $data['chart_data'] as $chart_data_key => $chart_data ) {
					$chart_data = (array) $chart_data;

					$chartData['labels'] = [];

					if ( 1 == count( $chart_data ) ) {
						$chartData['datasets'][ $chart_data_key ] = [
							'label'           => ucwords( str_replace( '_', ' ', $dataTypes[ $chart_data_key ] ) ),
							'backgroundColor' => $this->colors[ $chart_data_key ],
							'borderColor'     => $this->colors[ $chart_data_key ],
							'fill'            => false,
							'borderWidth'     => 2,
							'data'            => [],
						];

						foreach ( $chart_data as $coin_key => $coin ) {
							foreach( $coin as $value_date => $value ) {
								$date = date( 'm/d/y', strtotime( $value_date ) );
								$chartData['datasets'][$chart_data_key]['data'][] = [
									'x' => date( 'D M d Y H:i:s O', strtotime( $value_date ) ),
									'y' => $value,
								];
							}
						}
					} else {
						$counter = 0;
						foreach ( $chart_data as $coin_key => $coin ) {
							$chartData['datasets'][ $counter ] = [
								'label'           => ucwords( str_replace( '_', ' ', $dataTypes[ $counter ] ) ),
								'backgroundColor' => $this->colors[ $counter ],
								'borderColor'     => $this->colors[ $counter ],
								'fill'            => false,
								'borderWidth'     => 2,
								'data'            => [],
							];
							if ( 1 < count( $chart_data ) ) {
								$chartData['datasets'][$counter]['label'] = ucwords( str_replace( '_', ' ', $coin_key ) );
							}
							foreach( $coin as $value_date => $value ) {
								$date = date( 'm/d/y', strtotime( $value_date ) );
								$chartData['datasets'][$counter]['data'][] = [
									'x' => date( 'D M d Y H:i:s O', strtotime( $value_date ) ),
									'y' => $value,
								];
							}
							$counter++;
						}
					}

				}
			break;
		}

		return $chartData;
	}

	/**
	 * Delete Chart
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_REST_Response
	 */
	public function delete_chart( \WP_REST_Request $request ) {

		$current_user = wp_get_current_user();
		$charts = get_user_meta( $current_user->ID, '_rootmont_dashboard_charts', true );

		if ( ! $charts || ! is_array( $charts ) ) {
			return new \WP_Error( 'delete_chart', __( 'No Charts Found' ), [ 'data' => $charts ] );
		}

		$data = $request->get_params();
		$chart_id = $data['chart'];
		$count_before = count( $charts );

		foreach( $charts as $key => $chart ) {
			if ( $chart_id === $chart['uid'] ) {
				unset( $charts[$key] );
			}
		}

		$count_after = count( $charts );

		if( $count_before === $count_after ) {
			return new \WP_Error( 'delete_chart', __( 'Chart not found' ) );
		}

		update_user_meta( $current_user->ID, '_rootmont_dashboard_charts', $charts );

		$response_data = [
			'chart_removed' => $chart_id
		];
		return new \WP_REST_Response( $response_data );
	}

	/**
	 * Format Line Dates
	 *
	 * @param $data
	 *
	 * @return array
	 */
	private function _format_line_dates( $data ) {

		$dates = [];

		foreach( $data as $coin_key => $coin ) {
			foreach( $coin as $date_string => $value ) {
				$date = date( 'm/d/y', strtotime( $date_string ) );
				if ( ! in_array( $date, $dates, true ) ) {
					$dates[] = $date;
				}
			}
		}

		return $dates;
	}

	/**
	 * Generate UID
	 *
	 * @return bool|string
	 */
	private function uid_generator() {
		$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		return substr( str_shuffle( $chars ), 0, 11 );
	}

}

function get_api_location() {
    if (isset($_ENV["PANTHEON_ENVIRONMENT"]) == false) {
        $_ENV["PANTHEON_ENVIRONMENT"] = "dev";
    }
    if ($_ENV["PANTHEON_ENVIRONMENT"] == "dev") {
        $api_location = "http://dev.rootmont.io";
    } else if ($_ENV["PANTHEON_ENVIRONMENT"] == "test") {
        $api_location = "http://staging.rootmont.io";
    } else {
        $api_location = "https://rootmont.io";
    }
    return $api_location;
}
