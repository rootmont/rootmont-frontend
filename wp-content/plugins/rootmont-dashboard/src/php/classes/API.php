<?php

namespace RootmontDashboard;

use RootmontDashboard\Charts;
use RootmontDashboard\Dashboard;

class API {

	private $namespace;

	private $charts;

	public function __construct() {
		$this->namespace = 'rootmont/v1';
		$this->charts = Charts::init();
        $this->dashboard = Dashboard::init();
		add_action( 'rest_api_init', [ $this, 'RootmontDashboardApiInit' ] );
	}

	public function RootmontDashboardApiInit() {

		// Dashboard Charts
		register_rest_route( $this->namespace, 'dashboard/charts', [
			'methods' => 'GET',
			'callback' => [ $this->charts, 'get_charts' ],
			'permission_callback' => function() {
				return is_user_logged_in();
			}
		]);

        // Dashboard Movers
        register_rest_route( $this->namespace, 'dashboard/movers', [
            'methods' => 'GET',
            'callback' => [ $this->dashboard, 'get_mover_data' ],
            'args' => [
                'days' => [ 'required' => true ]
            ],
//  TODO: removing auth check bc I don't know how to authenticate from js
//            'permission_callback' => function() {
//                return is_user_logged_in();
//            }
        ]);


        register_rest_route( $this->namespace, 'dashboard/charts', [
			'methods' => 'POST',
			'callback' => [ $this->charts, 'save_chart' ],
			'args' => [
				'chart' => [ 'required' => true ]
			],
			'permission_callback' => function() {
				return is_user_logged_in();
			}
		]);

		register_rest_route( $this->namespace, 'dashboard/charts/(?P<chart>[\w-]+)', [
			'methods' => 'DELETE',
			'callback' => [ $this->charts, 'delete_chart' ],
			'args' => [
				'chart' => [ 'required' => true ]
			],
			'permission_callback' => function() {
				return is_user_logged_in();
			}
		]);

	}

}