<?php
namespace RootmontDashboard;

class Scripts {

	public $version;

	public function __construct() {
		$this->version = '1.1.0';
		add_action( 'wp_enqueue_scripts', [ $this, 'dashboard_enqueue_scripts' ] );
	}


	public function dashboard_enqueue_scripts() {
		if ( is_page( 'dashboard' ) ) {
			wp_enqueue_script( 'hammer', 'https://hammerjs.github.io/dist/hammer.min.js', [ 'jquery' ], null, true );
			$script_loc = ROOTMONT_DASH_URL . 'dist/';
			if ( defined( 'WP_ENV' ) && 'LOCAL2' === WP_ENV ) {
				$script_loc = '//localhost:4200/';
				$scripts = [
					[
						'key' => 'runtime',
						'script' => 'runtime.js',
					],
					[
						'key' => 'polyfills',
						'script' => 'polyfills.js',
					],
					[
						'key' => 'styles',
						'script' => 'styles.js',
					],
					[
						'key' => 'vendor',
						'script' => 'vendor.js',
					],
					[
						'key' => 'main',
						'script' => 'main.js',
					],
				];
			} else {
				$scripts = [
					[
						'key' => 'runtime',
						'script' => 'runtime.js',
					],
					[
						'key' => 'polyfills',
						'script' => 'polyfills.js',
					],
					[
						'key' => 'main',
						'script' => 'main.js',
					],
				];
				wp_enqueue_style( 'rootmont-dashboard-styles', $script_loc . 'styles.css', [], null, 'all' );
			}

			foreach ( $scripts as $key => $value ) {
				$prev_key = ( $key > 0 ) ? $scripts[$key-1]['key'] : 'hammer';
				wp_enqueue_script( $value['key'], $script_loc . $value['script'], array( $prev_key ), $this->version, true );
			}
			wp_localize_script( 'main', 'rootmontDashboard', array(
				'root' => esc_url_raw( rest_url() ) . 'rootmont/v1',
				'nonce' => wp_create_nonce( 'wp_rest' )
			) );
		}
	}

}