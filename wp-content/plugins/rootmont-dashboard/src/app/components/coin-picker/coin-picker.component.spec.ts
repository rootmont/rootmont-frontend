import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoinPickerComponent } from './coin-picker.component';

describe('CoinPickerComponent', () => {
  let component: CoinPickerComponent;
  let fixture: ComponentFixture<CoinPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoinPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoinPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
