import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'dashboard-coin-picker',
  templateUrl: './coin-picker.component.html',
  styleUrls: ['./coin-picker.component.scss']
})
export class CoinPickerComponent implements OnInit {

  @Input()
  coins: any;

  @Input()
  activeCoins: any = [];

  @Input()
  max: any = false;

  @Output() saveCoins: EventEmitter<any> = new EventEmitter();

  searchText: string = '';

  allCoinsIds: any;

  selectedCoins: any = [];
  selectedCoinsIds: any;

  constructor() { }

  ngOnInit() {
    if ( this.activeCoins && this.activeCoins.length ) {
      this.coins.forEach((coin, index) => {
        if ( -1 !== this.activeCoins.indexOf( coin.ID ) ) {
          this.selectedCoins.push( coin );
        }
      });
    }
  }

  preAddCheck() {
    if ( ! this.max ) {
      return true;
    }

    let to_add = this.allCoinsIds.length;
    let current = this.selectedCoins.length;
    let new_total = to_add + current;

    if ( this.max < new_total ) {
      console.log( 'you cannot have more than ' + this.max.toString() + ' coins, your new total would be: ' + new_total );
      return false;
    }

    return true;
  }

  move( direction ) {
    // Add to Selected
    if ( 'right' === direction && this.allCoinsIds.length && this.preAddCheck() ) {
      for( let coin of this.coins ) {
        if ( -1 !== this.allCoinsIds.indexOf( coin.ID.toString() ) && -1 === this.selectedCoins.indexOf( coin ) ) {
          this.selectedCoins.push( coin );
        }
      }
      this.allCoinsIds = [];
    }

    // Remove from Selected
    if ( 'left' === direction && this.selectedCoins.length ) {
      for ( let id of this.selectedCoinsIds ) {
        this.selectedCoins.forEach((coin, index) => {
          if ( coin.ID.toString() === id ) {
            this.selectedCoins.splice( index, 1 );
          }
        });
      }
      this.selectedCoinsIds = [];
    }
    this.saveCoins.emit( this.selectedCoins );
  }

  moveAll( direction ) {
    this.selectedCoinsIds = [];
    if ( 'right' === direction ) {
      this.selectedCoins = this.coins;
    }

    if ( 'left' === direction ) {
      this.selectedCoins = [];
    }

    this.saveCoins.emit( this.selectedCoins );
  }


}
