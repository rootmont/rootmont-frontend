import {Component, Input, AfterViewInit, Output, EventEmitter, TemplateRef } from '@angular/core';

import Chart from 'chart.js';
import 'chartjs-plugin-zoom';
import 'chartjs-plugin-downsample';

import { BsModalRef, BsModalService } from "ngx-bootstrap";

@Component({
  selector: 'dashboard-single-chart',
  templateUrl: './single-chart.component.html',
  styleUrls: ['./single-chart.component.scss']
})
export class SingleChartComponent implements AfterViewInit {

  @Input()
  chartData: any;

  @Output() removeChart: EventEmitter<any> = new EventEmitter();
  @Output() editChart: EventEmitter<any> = new EventEmitter();

  private chart: Chart<any>;
  modalRef: BsModalRef;

  constructor( private modalService: BsModalService ) { }

  ngAfterViewInit() {
    let id = 'single-chart-' + this.chartData.uid;

    Chart.defaults.global.defaultFontSize = 18;

    if ( 'bar' === this.chartData.type ) {
      this.chart = new Chart( document.getElementById( id ), {
        type : this.chartData.type,
        data: this.chartData.chartjs_data,
        options: {}
      });
    } else if( 'line' === this.chartData.type ) {

      // Convert PHP Timestring Date into Date Object
      this.chartData.chartjs_data.datasets.forEach((dataset, dataset_index) => {
        dataset.data.forEach((value, value_index) => {
          this.chartData.chartjs_data.datasets[dataset_index].data[value_index] = { x: new Date( value.x ), y: value.y };
        });
        dataset.yAxisID = this.getYLabel(dataset_index);
      });

      let config = {
        type : this.chartData.type,
        data: this.chartData.chartjs_data,
        options: {
          responsive: true,
          tooltips: {
            mode: 'index',
            intersect: false,
            callbacks: {
              label: function( tooltipItem, data ) {
                let parts = tooltipItem.yLabel.toString().split('.' );
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',' );
                return data.datasets[tooltipItem.datasetIndex].label.toUpperCase() + ': ' + parts.join( '.' );
              }
            }
          },
          scales: {
            xAxes: [
              {
                type: "time",
                time: {
                  parser: 'MM/DD/YYYY HH:mm',
                  round: 'day',
                  tooltipFormat: "ll HH:mm",
                  min: new Date( 'January 1, 2013' ),
                  max: new Date(),
                  minUnit: 'day',
                },
                scaleLabel: {
                  display: true,
                  labelString: "Date"
                }
              }
            ],
            yAxes: []
          },
          downsample: {
            enabled: true,
            threshold: 500,
          },
          pan: {
            enabled: true,
            mode: 'x',
            rangeMin: {
              x: new Date( 'January 1, 2013' ),
              y: null,
            },
            rangeMax: {
              x: new Date(),
              y: null
            }
          },
          zoom: {
            enabled: true,
            mode: 'x',
            limits: {
              max: 10,
              min: 0.5
            },
            onZoom: function() { console.log( 'Zooomed' ); }
          },
        },
      };

      // Add Left Y Axis
      config.options.scales.yAxes.push({
          position: 'left',
          id: this.getYLabel(0),
          scaleLabel: {
            display: true,
            labelString: this.getYLabel(0)
          },
          ticks: {
            callback: function( value, index, values ) {
              let parts = value.toString().split('.' );
              parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',' );
              return parts.join( '.' );
            }
          }
      });

      // Multiple Data Points - Add Right Y Axis
      if ( 1 < this.chartData.data.length ) {
        config.options.scales.yAxes.push({
          position: 'right',
          id: this.getYLabel(1),
          scaleLabel: {
            display: true,
            labelString: this.getYLabel(1)
          },
          ticks: {}
        });
      }

      this.chart = new Chart( document.getElementById( id ), config );
    }
  }

  getYLabel( index ) {
    if ( ! this.chartData.data ) {
      return '';
    }

    if ( 1 === this.chartData.data.length ) {
      return this.chartData.data[0];
    }

    return this.chartData.data[index]
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
  }

  removeChartInit() {
    this.modalRef.hide();
    this.removeChart.emit( this.chartData );
  }

  editChartInit() {
    this.editChart.emit( this.chartData );
  }

}
