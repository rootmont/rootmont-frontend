import { Component, OnInit } from '@angular/core';

import { SingleChartComponent } from '../single-chart/single-chart.component';
import { NewChartComponent } from '../new-chart/new-chart.component';

import { RestApiService } from '../../../services/api.service';

@Component({
  selector: 'dashboard-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss'],
  providers: [RestApiService]
})
export class ChartsComponent implements OnInit {

  private api: any;

  public charts: any = [];
  public loading: boolean = true;
  public editChartData: any;

  public allCoins: any;

  constructor( private apiService: RestApiService ) { }

  ngOnInit() {
    this.api = this.apiService.dashboard;
    this.apiService.getAllCoins().subscribe((data) => {
      this.allCoins = data;
    });
    this.loadCharts();
  }

  newChartInit( evt ) {
    if ( ! evt.uid ) {
      evt.uid = Math.random().toString( 11 ).replace('0.', '');
      evt.editing = false;
      this.charts.push( evt );
    } else {
      delete evt.chart_data;
      delete evt.chartjs_data;
      evt.editing = true;
    }
    this.loading = true;

    this.api.saveChart( evt ).subscribe((data) => {
      this.editChartData = {};
      this.loadCharts();
    });
  }

  loadCharts() {
    this.api.getCharts().subscribe((data) => {
      let charts = [];
      if ( data.charts instanceof Object ) {
        Object.keys(data.charts).map(function(key) {
          charts.push( data.charts[key] );
        });
      } else {
        charts = data.charts;
      }
      this.loading = false;
      this.charts = charts;
    });
  }

  removeChart( evt ) {
    this.loading = true;
    this.api.deleteChart( evt.uid ).subscribe((data) => {
      //this.loadCharts();
      location.reload();
    });
  }

  editChart( evt ) {
    this.editChartData = evt;
  }

}
