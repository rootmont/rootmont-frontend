import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

import { BsModalService, ModalDirective } from "ngx-bootstrap";

import { RestApiService } from '../../../services/api.service';


@Component({
  selector: 'dashboard-new-chart',
  templateUrl: './new-chart.component.html',
  styleUrls: ['./new-chart.component.scss'],
  providers: [BsModalService]
})
export class NewChartComponent implements OnInit {

  availableCharts: any;

  currentStep: any = 1;
  currentChoiceSelected: boolean = false;

  availableChoices: any = {};
  activeChoices: any = [];

  newChart: any = {};
  editing_mode: boolean = false;

  maxCoins: number = 4;

  constructor( private modalService: BsModalService, private api: RestApiService ) { }

  @Input()
  allCoins : any;

  @Output() newChartInit: EventEmitter<any> = new EventEmitter();

  @ViewChild( 'lgModal' ) lgModal: ModalDirective;

  @Input()
  set editChartData( editChartData: any ) {
    if ( editChartData && editChartData.uid ) {
      this.lgModal.show();
      this.editing_mode = true;
      this.newChart = editChartData;
      this.newChart.edited = true;
    }
  }

  ngOnInit() {
    this.currentStep = 1;

    this.availableChoices = {
      line: [
        {
          label: 'ntx',
          title: 'NTX',
          description: 'Daily transactions over time'
        },
        {
          label: 'mcap',
          title: 'Market Cap',
          description: 'Market capitalization over time'
        },
        {
          label: 'price',
          title: 'Price',
          description: 'Price (close) over time'
        },
        {
          label: 'vol',
          title: 'Trading Volumes',
          description: 'Trading volumes over time'
        }
      ],
      bar: [
        {
          label: 'hype',
          title: 'Hype',
          description: 'Market Cap / Social'
        },
        {
          label: 'reddit_members',
          title: 'Reddit Members',
        },
        {
          label: 'total_tweets',
          title: 'Total Tweets',
        },
        {
          label: 'twitter_leadership',
          title: 'Twitter Leadership',
          description: 'Twitter Followers / Twitter Following'
        },
        {
          label: 'twitter_followers',
          title: 'Twitter Followers',
        },
        {
          label : 'bitcointalk_posts',
          title: 'Bitcoin Talk Posts',
        },
        {
          label : 'github_size',
          title: 'GitHub Size',
          description: 'Size of GitHub Repository in Kilobytes'
        },
        {
          label : 'github_stargazers',
          title: 'GitHub Stargazers',
        },
        {
          label : 'github_forks',
          title: 'GitHub Forks',
        },
        {
          label : 'github_open_issues',
          title: 'GitHub Open Issues',
        },
        {
          label : 'github_contributors',
          title: 'GitHub Contributors',
        },
        {
          label : 'github_contributions',
          title: 'GitHub Contributions',
        },
        {
          label: 'up_down',
          title: 'Upside/Downside',
        }
      ]
    };
  }

  changeStep( prev ) {
    this.currentChoiceSelected = false;
    if ( ! prev ) {
        this.currentStep+=1;
    } else {
      this.currentStep-=1;
    }
    this.setActiveChoices();
  }

  setChoice( option, value ) {
    const multiple_available = [ 'data', 'coins' ];
    const multiple_available_total = {
      data: {
        max: 2,
        min: 1
      },
      coins: {
        max: 20,
        min: 1
      }
    };

    // check allows multiple
    if ( -1 !== multiple_available.indexOf( option ) ) {
      if ( ! this.newChart[option] ) {
        this.newChart[option] = [];
      }

      // allows, and found in options
      if ( -1 !== this.newChart[option].indexOf( value ) ) {
        this.newChart[option].splice( this.newChart[option].indexOf( value ), 1 );
      } else if ( -1 === this.newChart[option].indexOf( value ) ) {
        // allows, not found, but can add?
        if ( multiple_available_total[option].max > this.newChart[option].length ) {
          this.newChart[option].push( value );
        }
      }

      // check if max value hit
      let total = this.newChart[option].length;
      if (
        multiple_available_total[option].max >= total &&
        multiple_available_total[option].min <= total
      ) {
        this.currentChoiceSelected = true;
      } else if( multiple_available_total[option].min >= total ) {
        this.currentChoiceSelected = false;
      }

    } else {
      this.newChart[option] = value;
      this.currentChoiceSelected = true;
    }

    if ( 'data' === option && 2 <= this.newChart[option].length ) {
      this.maxCoins = 1;
    } else if ( 'data' === option && 2 > this.newChart[option].length ) {
      this.maxCoins = multiple_available_total.coins.max;
    }
  }

  setCoins( evt ) {
    this.newChart['coins'] = [];
    for( let coin of evt ) {
      this.setChoice( 'coins', coin.ID );
    }
  }

  setActiveChoices() {
    this.activeChoices = [];
    if ( 2 === this.currentStep ) {
      this.activeChoices = this.availableChoices[ this.newChart.type ];
    }

    if ( 3 === this.currentStep ) {
      this.activeChoices = this.allCoins;
    }

    if ( 4 === this.currentStep ) {
      this.newChart.title = ! this.newChart.title ? this.newChart.data.toString().replace(/_/g,' ') : this.newChart.title;
      this.activeChoices = [
        {
          label: 'half-width',
          title: 'Half Width',
          description: 'Chart will be 50% width'
        },
        {
          label: 'full-width',
          title: 'Full Width',
          description: 'Chart will be 100% width, on its own line'
        }
      ];
      if ( ! this.newChart.width ) {
        this.newChart.width = 'half-width';
      }
      this.currentChoiceSelected = true;
    }
  }

  saveChart() {
    this.newChartInit.emit( this.newChart );
    this.resetChart();
    this.lgModal.hide();
  }

  resetChart() {
    this.newChart = {};
    this.currentStep = 1;
    this.activeChoices = [];
    this.editing_mode = false;
    this.editChartData = false;
  }

}
