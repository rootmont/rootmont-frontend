import { Injectable } from '@angular/core';

import { WindowService } from './window.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class RestApiService {

  private api_url: any;
  private nonce: any;

  public dashboard: any = {};

  constructor(private win: WindowService, private http: HttpClient) {

    // Set API URL
    this.api_url = (this.win.nativeWindow.rootmontDashboard) ? this.win.nativeWindow.rootmontDashboard.root : 'https://rootmont.test/wp-json/rootmont/v1';
    this.nonce = (this.win.nativeWindow.rootmontDashboard) ? this.win.nativeWindow.rootmontDashboard.nonce : false;

    // Set Dashboard Object
    this.dashboard = {
      url: this.api_url,
      getCharts: this.getCharts.bind(this),
      saveChart: this.saveChart.bind(this),
      deleteChart: this.deleteChart.bind(this)
    };
    this.dashboard.url += '/dashboard/';
  }

  // Return URL
  getURL() {
    return this.api_url;
  }

  // Return Dashboard Object
  getDashboard() {
    return this.dashboard;
  }

  // GET ALL COINS
  getAllCoins() {
    return this.http.get( this.api_url + '/coins' );
  }

  // Dashboard - Charts
  getCharts() {

    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.nonce,
        'X-WP-Nonce': this.nonce
      })
    };

    return this.http.get( this.dashboard.url + 'charts', httpOptions );
  }

  saveChart( chart ) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.nonce,
        'X-WP-Nonce': this.nonce
      })
    };

    return this.http.post( this.dashboard.url + 'charts', { chart: chart }, httpOptions );
  }

  deleteChart( chart_id ) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.nonce,
        'X-WP-Nonce': this.nonce
      })
    };

    return this.http.delete( this.dashboard.url + 'charts/' + chart_id, httpOptions );
  }

}