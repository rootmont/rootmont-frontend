import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { ChartsComponent } from './charts/components/charts/charts.component';
import { SingleChartComponent } from './charts/components/single-chart/single-chart.component';
import { NewChartComponent } from './charts/components/new-chart/new-chart.component';

import { ModalModule } from 'ngx-bootstrap';

import { WindowService} from './services/window.service';
import { SearchPipe } from './pipes/search.pipe';
import { CoinPickerComponent } from './components/coin-picker/coin-picker.component';

@NgModule({
  declarations: [
    AppComponent,
    ChartsComponent,
    SingleChartComponent,
    NewChartComponent,
    SearchPipe,
    CoinPickerComponent
  ],
  imports: [
    BrowserModule,
    ModalModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [
    WindowService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
