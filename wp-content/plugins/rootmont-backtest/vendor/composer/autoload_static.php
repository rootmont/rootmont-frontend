<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit20d342300beab63d4ac81efdccd5bd39
{
    public static $prefixLengthsPsr4 = array (
        'R' => 
        array (
            'RootmontBacktest\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'RootmontBacktest\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/php/classes',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit20d342300beab63d4ac81efdccd5bd39::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit20d342300beab63d4ac81efdccd5bd39::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
