<?php
/*
Plugin Name: Rootmont Backtest
Plugin URI: https://rootmont.com
Description: Rootmont Backtest Plugin
Version: 1.0.0
Author: Rootmont Engineering Team
Author URI: https://rootmont.com
Text Domain: rootmont-dashboard
Domain Path: /languages
*/

namespace RootmontBacktest;

require_once 'vendor/autoload.php';

define( 'ROOTMONT_BACKTEST_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'ROOTMONT_BACKTEST_PLUGIN_DIR', plugin_dir_url( __FILE__ ) );

class RootmontBacktest {

	public function __construct() {}

}

new RootmontBacktest();