<?php
namespace RootmontDashboard;

class Scripts {

	public $version;

	public function __construct() {
		$this->version = '1.0.0';
		add_action( 'wp_enqueue_scripts', [ $this, 'backtest_enqueue_scripts' ] );
	}


	public function dashboard_enqueue_scripts() {
		if ( is_page( 'dashboard' ) ) {
			$script_loc = ROOTMONT_BACKTEST_PLUGIN_URL . 'dist/';
			if ( defined( 'WP_ENV' ) && 'LOCAL' === WP_ENV ) {
				$script_loc = '//localhost:4200/';
			}

			$scripts = [
				[
					'key' => 'runtime',
					'script' => 'runtime.js',
				],
				[
					'key' => 'polyfills',
					'script' => 'polyfills.js',
				],
				[
					'key' => 'styles',
					'script' => 'styles.js',
				],
				[
					'key' => 'vendor',
					'script' => 'vendor.js',
				],
				[
					'key' => 'main',
					'script' => 'main.js',
				],
			];

			foreach ( $scripts as $key => $value ) {
				$prev_key = ( $key > 0 ) ? $scripts[$key-1]['key'] : 'hammer';
				wp_enqueue_script( $value['key'], $script_loc . $value['script'], array( $prev_key ), $this->version, true );
			}
			wp_localize_script( 'main', 'rootmontBacktest', array(
				'root' => esc_url_raw( rest_url() ) . 'rootmont/v1',
				'nonce' => wp_create_nonce( 'wp_rest' )
			) );
		}
	}

}