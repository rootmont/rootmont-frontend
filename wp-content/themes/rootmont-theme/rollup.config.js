import resolve from 'rollup-plugin-node-resolve';
import multiEntry from "rollup-plugin-multi-entry";
import visualizer from 'rollup-plugin-visualizer';
import handlebars from 'rollup-plugin-handlebars-plus';
import json from 'rollup-plugin-json';
// import handlebars from 'rollup-plugin-handlebars';
import scss from 'rollup-plugin-scss'
import { terser } from "rollup-plugin-terser";
import commonjs from 'rollup-plugin-commonjs';
// import handlebars from 'rollup-plugin-hbs';


export default {
    // tell rollup our main entry point
    input:
        // 'assets/js/test.js',
// [
        'assets/js/exp.js',
        // './node_modules/datatables.net/js/jquery.dataTables.js',
        // './node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
        // './node_modules/datatables.net-buttons/js/dataTables.buttons.js',
        // './node_modules/datatables.net-buttons/js/buttons.flash.js',
        // './node_modules/datatables.net-buttons/js/buttons.html5.js',
        // './node_modules/datatables.net-buttons/js/buttons.colVis.js',
        // './node_modules/datatables.net-buttons/js/buttons.print.js',
        // './node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.js',
        // './node_modules/datatables.net-fixedheader/js/dataTables.fixedHeader.js',
        // './node_modules/datatables.net-fixedheader-bs/js/fixedHeader.bootstrap.js',
        // './node_modules/datatables.net-keytable/js/dataTables.keyTable.js',
        // './node_modules/datatables.net-responsive/js/dataTables.responsive.js',
        // './node_modules/datatables.net-responsive-bs/js/responsive.bootstrap.js',
    // ],

    output: {
      name: 'rootmont',
      file: 'build/js/main.min.js',
        format: 'iife',
        globals: {
            jquery: '$'
        }
        // format: 'umd'
    },
    plugins: [
        resolve({
            // pass custom options to the resolve plugin
            customResolveOptions: {
              moduleDirectory: [ 'node_modules', '../../../wp-includes']
            }
        }),
        json(),
        visualizer({
            filename: './rollup_stats.html',
            open:true,
        }),
        commonjs({
            include: 'node_modules/**',
        }),
        handlebars({
            // helpers:['assets/js/handlebarsHelpers.js'],
             // templateExtension: '.html'
        })
        // scss(),
	    // multiEntry(),
        // terser(),
    ],
};
