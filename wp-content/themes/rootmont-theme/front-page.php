<?php
get_header(); the_post();
global $post, $rootmontCoins, $coin_info_data, $is_active_user;
?>

<?php

$request = new \WP_REST_Request( 'GET', '/rootmont/v1/coin-catalog/' );
$cache_bust = isset( $_GET, $_GET['CACHEBUST'] );
$coin_cat_term = false;
if ( ( $coin_cat = get_query_var( 'coin_cat' ) ) && ( $coin_cat_term = get_query_var( 'coin_cat_term' ) ) ) {
	$request->set_query_params( [
		'category' => $coin_cat,
		'term'     => $coin_cat_term,
		'cache_bust' => $cache_bust,
	] );
} else {
	$request->set_query_params([
		'category' => 'global',
		'cache_bust' => $cache_bust,
	]);
}
$coins = rest_ensure_response( $rootmontCoins->api->get_coin_catalog( $request ) );

if ( is_wp_error( $coins ) ) {
	//var_dump(  trailingslashit( get_site_url() ) . rest_get_url_prefix() . $request->get_route() . $coin_cat . '/' . $coin_cat_term );
	var_dump( $coins->get_error_message() );
}

$catalog = $coins->data['catalog'];
$available_cats = $coins->data['all_coin_cats'];

?>
<!-- ACTIVE USER -->
<section id="content" role="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
					<?php if ( ! empty( $catalog ) ) : ?>
                        <div class="coin-archive-table-wrapper">

                            <div class="page-title">
                                <h1>
                                    TOP <?php echo $coin_cat_term ?: ''; ?> CRYPTOCURRENCIES BY ROOT RANK
                                </h1>
                            </div>
                            <div class="page-content">
                                Root Rank is a metric that combines price performance, on-chain usage, social media presence and engineering activity into a single score. <a href="https://rootmont.com/root-rank-where-does-it-come-from/">See here for more information.</a>
                            </div>

							<?php if ( $coin_cat && $coin_cat_term ) : ?>
                                <h3>Category: <?php echo $coin_cat . ' - ' . $coin_cat_term; ?></h3>
							<?php else : ?>
                                <h3>All Coins</h3>
							<?php endif; ?>
                            <div class="coin-categories">
                                FILTERS:
								<?php foreach ( $available_cats as $cat => $terms ) : ?>
                                    <label for="_cat_<?php echo $cat; ?>">
										<?php echo $cat; ?>
                                        <select id="_cat_<?php echo $cat; ?>">
                                            <option value="-1">Select Category</option>
											<?php foreach( $terms as $key => $term ) : $value = str_replace( ' ', '_', $term ); ?>
                                                <option value="<?php echo $value; ?>" <?php if ( $coin_cat_term ) { selected( $value, $coin_cat_term ); } ?>><?php echo $term; ?></option>
											<?php endforeach; ?>
                                        </select>
                                    </label>
								<?php endforeach; ?>
								<?php if ( $coin_cat_term ) : ?>
                                    <a href="/">reset</a>
								<?php endif; ?>
                            </div>
                            <table id="dataTable" class="dataTable coin-archive-table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Symbol</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Price Change (24hr)</th>
                                    <th>Industry Cluster</th>
                                    <th>Market Cap</th>
                                    <th>Market Cap Cluster</th>
                                    <th>Age Cluster</th>
                                    <th>Root Rank</th>
                                    <th width="25">Social</th>
                                    <th width="25">Development</th>
                                    <th width="25">Usage</th>
                                    <th width="25">Price Performance</th>
                                </tr>
                                </thead>
								<?php foreach( $catalog as $coin ) : ?>
                                    <tr>
                                        <td></td>
                                        <td>
											<?php if ( $coin->permalink ) : ?>
                                                <a href="<?php echo $coin->permalink; ?>">
													<?php echo $coin->symbol; ?>
                                                </a>
											<?php else : ?>
												<?php echo $coin->symbol; ?>
											<?php endif; ?>
                                        </td>
                                        <td>
											<?php echo $coin->name; ?>
                                        </td>
                                        <td id="<?php echo $coin->name; ?>-price">
		                                    <?php echo '$' . number_format( $coin->price, 2 ); ?>
                                        </td>
                                        <td id="<?php echo $coin->name; ?>-24h">
		                                    <?php echo number_format( 100 * $coin->{'24h'}, 2 ) . '%'; ?>
                                        </td>
                                        <td>
		                                    <?php echo $coin->{'industry_cluster'}; ?>
                                        </td>
                                        <td id="<?php echo $coin->name; ?>-mcap">
											<?php echo '$' . number_format( (float)$coin->marketcap ); ?>
                                        </td>
                                        <td>
		                                    <?php echo $coin->{'marketcap_cluster'}; ?>
                                        </td>
                                        <td>
		                                    <?php echo $coin->{'age_cluster'}; ?>
                                        </td>
                                        <td>
											<?php echo round( $coin->{'overall percentile'}, 2 ) * 100; ?>
                                        </td>
                                        <td>
											<?php echo round( $coin->{'social percentile'}, 2 ) * 100; ?>
                                        </td>
                                        <td>
											<?php echo round( $coin->{'development percentile'}, 2 ) * 100; ?>
                                        </td>
                                        <td>
											<?php echo round( $coin->{'usage percentile'}, 2 ) * 100; ?>
                                        </td>
                                        <td>
											<?php echo round( $coin->{'price percentile'}, 2 ) * 100; ?>
                                        </td>
                                    </tr>
								<?php endforeach; ?>
                            </table>
                        </div>
					<?php else : ?>
                        <div class="coin-archive-table-wrapper">
                            <h2>No Results</h2>
                            <p>
                                No Coins Found. View <a href="/">all coins.</a>
                            </p>
                        </div>
					<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/keytable/2.5.0/css/keyTable.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.css"/>
    <script src="/wp-content/themes/bb-theme/js/theme.min.js"></script>
    <script src="/wp-includes/js/admin-bar.min.js"></script>
    <script src="/wp-includes/js/jquery/ui/widget.min.js"></script>
    <script
            src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous">

    </script>

    <script>
        if ( rootmont ) {
          setInterval(rootmont.updateCatalog, 15000);
          rootmont.frontpage();
        }
    </script>
</section>

<?php get_footer(); ?>
