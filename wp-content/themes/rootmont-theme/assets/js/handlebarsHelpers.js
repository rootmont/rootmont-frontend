export default function(Handlebars) {
    Handlebars.registerHelper( 'percent', function( number ) {
        let num = number * 100;
        num = Math.round( num * 100 ) / 100;
        return num;
    });
}
