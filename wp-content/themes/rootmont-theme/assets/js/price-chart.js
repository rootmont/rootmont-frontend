import {dispatch as d3Dispatch, timeParse as d3TimeParse, json as d3Json} from 'd3';
import {oscillatorChart} from './oscillator.js';

// TODO: Resize
// TODO: Click to zoom
export function priceCharts(symbol) {
    const BASEURL = window.rootmontAPI.root + 'rootmont/v1/price-chart/';
    const url = BASEURL + symbol + '/';

    const initialDaysToDisplay = 90;
    const downSampleThreshold = 400;

    // Charts' options
    const priceMACDChartOptions = {
        chartName: "priceMACDChart",
        chartType: "oscillator",
        chartId: "macd-chart",
        containerHeight: 400,
        keys: [
            { key: "divergence", name: "Divergence" },
            { key: "price_first_derivative", name: "Fast Derivative" },
            { key: "price_smooth_derivative", name: "Slow Derivative" },
            { key: "price", name: "Price" }
        ]
    };

    const priceVolMcapChartOptions = {
        chartName: "priceVolMcapChart",
        chartType: "line-vol",
        chartId: "price-vol-mcap-chart",
        containerHeight: 400,
        keys: [
            { key: "price", name: "Price" },
            { key: "trading_volume", name: "Trading Volume" },
            { key: "marketcap", name: "Market Cap" },
        ]
    };

    const chartOptions = [
        priceMACDChartOptions,
        priceVolMcapChartOptions,
    ];

    // Global variables
    let data; // Parsed json data
    const charts = []; // Rendered charts

    let priceMACDChart,
        priceVolMcapChart;

    const parseTime = d3TimeParse("%Y-%m-%dT%H:%M:%S.%LZ");

    // Sync charts with custom events
    const dispatch = d3Dispatch(
        "zoom",
        "showTooltip",
        "updateTooltip",
        "hideTooltip",
        "enableInteractions"
    );
    dispatch.on("zoom", params => {
        const startDate = params.startDate;
        const endDate = params.endDate;
        const chartId = params.chartId;
        charts.forEach(chart => {
            if (chart.chartId === chartId) return;
            chart.syncZoom(startDate, endDate);
        });
    });
    dispatch.on("showTooltip", () => {
        charts.forEach(chart => {
            if (chart.chartType !== "line") return;
            chart.showTooltip();
        });
    });
    dispatch.on("updateTooltip", function(d) {
        charts.forEach(chart => {
            function addInitOverlay() {
                const overlay = svg
                    .append("g")
                    .attr("class", "overlay")
                    .style("pointer-events", "all")
                    .style("cursor", "pointer")
                    .on("click", function() {
                        d3Dispatch("enableInteractions");
                    });
                overlay
                    .append("rect")
                    .attr("width", "100%")
                    .attr("height", "100%")
                    .style("fill", "#fff")
                    .style("fill-opacity", 0.6);
                overlay
                    .append("text")
                    .attr("x", "50%")
                    .attr("y", "50%")
                    .style("font-size", "1.5em")
                    .attr("dy", "0.35em")
                    .style("text-anchor", "middle")
                    .text("Click to enable chart interactions");
            }

            chart.enableInteractions = function() {
                svg.select(".overlay").remove();
            };
            if (chart.chartType !== "line") return;
            chart.updateTooltip.call(this, d);
        });
    });
    dispatch.on("hideTooltip", () => {
        charts.forEach(chart => {
            if (chart.chartType !== "line") return;
            chart.hideTooltip();
        });
    });
    dispatch.on("enableInteractions", () => {
        charts.forEach(chart => {
            chart.enableInteractions();
        });
    });

    // Fetch data
    d3Json(url).then(function(json) {
        processData(json);
        renderCharts();
    });

    // Process data
    function processData(json) {
        json.forEach(d => {
            d.date = parseTime(d.date);
        });
        data = json;
    }

    // Render charts
    function renderCharts() {
        const d = data[0];
        const initialStartingDate =
            data.length < initialDaysToDisplay
                ? data[0].date
                : data[data.length - initialDaysToDisplay].date;

        chartOptions.forEach(chartOption => {
            // Pass shared options
            chartOption.data = data;
            chartOption.downSampleThreshold = downSampleThreshold;
            chartOption.initialStartingDate = initialStartingDate;
            chartOption.dispatch = dispatch;

            if (chartOption.chartType === "oscillator") {
                // If value is null, don't render this chart
                if (
                    chartOption.keys
                        .slice(0, 3)
                        .map(key => key.key)
                        .some(key => d[key] === null)
                )
                    return;

                if (chartOption.chartName === "priceMACDChart") {
                    priceMACDChart = oscillatorChart(priceMACDChartOptions);
                    charts.push(priceMACDChart);
                }

            } else if (chartOption.chartType === "line-vol") {
                // If value is null, don't render this chart
                if ( null === d[chartOption.keys[0].key] ) {
                    console.log( 'd', d );
                    return;
                }

                // if (chartOption.chartName === "priceVolMcapChart") {
                //     priceVolMcapChart = lineVolChart(priceVolMcapChartOptions);
                //     charts.push(priceVolMcapChart);
                // }
            }

        });
    }

    // Responsive
    // Throttle resize event
    // https://developer.mozilla.org/en-US/docs/Web/Events/resize
    window.addEventListener("resize", resizeThrottler, false);

    let resizeTimeout;
    function resizeThrottler() {
        if (!resizeTimeout) {
            resizeTimeout = setTimeout(function() {
                resizeTimeout = null;
                resize();
            }, 100);
        }
    }

    function resize() {
        charts.forEach(chart => {
            chart.resize();
        });
    }
}
