(function($){
  import {autocomplete} from 'jquery-ui-dist';
  $(document).ready( function() {

    let $body = $('body');

    // Popups
    const $popovers = $('.rootmont-popup');

    $.each( $popovers, function( value, key ) {
      let $this = $(this);
      let popup_name = $this.data('popup');
      if ( window.rootmontPopupData && window.rootmontPopupData[ popup_name ] ) {
        // get data from localized ACF object
        let popup_data = window.rootmontPopupData[ popup_name ];

        // setup popover
        $this.popover({
          placement: 'top',
          html: true,
          content: popup_data.popup_message,
          title: popup_data.title + '<i class="fa fa-times pull-right close-popover" data-popup="' + popup_name +'"></i>'
        });

        // after show - set hide timeout
        $this.on( 'shown.bs.popover', function() {
          setTimeout(function() {
            $this.popover('hide');
          }, 10000 );
        })

      } else {
        //console.log( 'popup ' + popup_name + ' data not found.' );
      }
    });

    // Click to close popover
    $body.on('click', '.close-popover', function() {
      let popup_name = $(this).data('popup');
      let $popup = $( '.rootmont-popup[data-popup="' + popup_name + '"]');
      $popup.popover('hide');
    });


    // dataTables
    const $dataTable = $( '.dataTable' );

    if ( $dataTable.length ) {
      $.each( $dataTable, function(key, value) {
        if ( $(this).hasClass( 'enterprise' ) ) {
          let $table = $(this).DataTable({
            dom: 'frBtilp',
            pageLength: "25",
            order: [],
            fixedHeader: {
              header: false,
              footer: false,
              headerOffset: $('header.fl-page-header').outerHeight() - 46,
            },
            buttons: [
              'csv', 'excel', 'pdf'
            ]
          });
        } else if ( $(this).hasClass( 'non-enterprise' ) ) {
          let $table = $(this).DataTable({
            dom: 'frtilp',
            pageLength: "25",
            order: [],
            fixedHeader: {
              header: false,
              footer: false,
              headerOffset: $('header.fl-page-header').outerHeight() - 46,
            }
          });
        } else if ( $(this).hasClass( 'coin-archive-table' ) ) {
          let $table = $(this).DataTable({
            columnDefs: [{
              targets: [0],
              orderable: false,
            },{
              targets: [1],
              width: '3%'
            }],
            dom: 'frtilp',
            pageLength: "100",
            order: [ [ 9, "desc" ] ],
            fixedHeader: {
              header: true,
              headerOffset: 63,
              footer: false,
            }
          });

          $table.on( 'order.dt search.dt', function () {
            $table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
          } ).draw();
        }
      })
    }

    // Coin Archive
    if ( $body.hasClass('post-type-archive-coins') || $body.hasClass('home') ) {
      $( '.coin-categories select' ).on( 'change', function() {
        let term = $(this).val(),
            id = $(this).attr('id'),
            cat = id.replace( '_cat_', '' );

        window.location.href = '/filtered/' + cat + '/' + term;
      })
    }

    // Single Coin
    if( $body.hasClass('single-coins' ) ) {
      $body.css({
        'padding-top': 20
      });

      // Single Coin Sub Nav
      $body.on( 'click', 'ul.coin-subnav li a ', function(e) {
        e.preventDefault();
        let href = $(this).attr('href');
        let pos = $(href).position();

        $('body,html').animate({
          scrollTop: '#content' === href ? 0 : pos.top
        });
      });

      $body.on( 'click', '.scrollTo', function( e ) {
        e.preventDefault();
        let location = $(this).data('destination');
        let $destination = $( location );
        $('body,html').animate({
          scrollTop: $destination.position().top
        });
      });

    }

    // Search Autocomplete.
    if ( $( '.fl-search-input' ).length ) {
      const $form = $( '.fl-page-nav-search form' );
      const $input = $( '.fl-search-input' );

      $form.on( 'submit', function(e) {
        e.preventDefault();
      });

      $input.autocomplete({
        source: function( request, response ) {
          $input.siblings('.error').remove();
          $.ajax({
            method: 'GET',
            beforeSend: function(xhr){
              xhr.setRequestHeader( 'X-WP-Nonce',  rootmontAPI.nonce );
            },
            url: rootmontAPI.root + 'rootmont/v1/coin-search/',
            data: {
              term: request.term
            },
            success: function( data ) {
              if ( data.autocomplete ) {
                response( data.autocomplete );
              } else {
                console.log( 'no autocomplete ', data );
                $input.after('<p class="error"><em>no results</em></p>');
              }
            }
          })
        },
        minLength: 2,
        select: function( event, ui ) {
          if ( ui.item.value ) {
            if ( ui.item.url ) {
              window.location = ui.item.url;
            }
          }
        }
      });
    }

  });

}(jQuery));
