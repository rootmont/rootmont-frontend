import 'jquery';
let $ = jQuery;

export function updateCatalog() {
    let v = Math.random();
    $.ajax({
        method: 'GET',
        beforeSend: function(xhr){
            xhr.setRequestHeader( 'X-WP-Nonce', rootmontAPI.nonce );
        },
        url: rootmontAPI.root + 'rootmont/v1/prices/?v=' + v,
        success: function( data ) {
            data.response.forEach(x => {
                updateCatalogField(x.token_name + '-price', x.price);
                updateCatalogField(x.token_name + '-mcap', x.marketcap);
                let pctChange = (x.price - x.price_open) / x.price_open;
                updateCatalogField(x.token_name + '-24h', pctChange, true);
            });
        }
    })
}

function updateCatalogField(field, value, pct=false) {
    let id = '[id="' + field + '"]';
    let s = $(id);
    let g = s.get();
    if (g.length === 0) return;
    let el = g[0];
    let currentValue;
    if (pct) {
        currentValue = parseFloat(el.innerHTML.replace('%','').replace(/,/g,''));
    } else {
        currentValue = parseFloat(el.innerHTML.replace('$','').replace(/,/g,''));
    }
    let newValue = parseFloat(value);
    newValue = Math.floor(newValue * 100) / 100;
    if (newValue > currentValue) {
        s.css('color', 'green')
    } else if (newValue < currentValue) {
        s.css('color', 'red')
    // } else {
    //     s.css('color', '#484747')
    }
    newValue = newValue.toLocaleString('en');
    if (pct) {
        el.innerHTML = newValue + '%';
    } else {
        el.innerHTML = '$' + newValue;
    }
}

export function updateCoinReportStats(coinName) {
    let v = Math.random();
    $.ajax({
        method: 'GET',
        beforeSend: function(xhr){
            xhr.setRequestHeader( 'X-WP-Nonce',  rootmontAPI.nonce );
        },
        url: rootmontAPI.root + 'rootmont/v1/current-stats/?v=' + v + '&name=' + coinName,
        success: function( data ) {
            updateCoinReportField('price', data.price);
            updateCoinReportField('mcap', data.marketcap);
            updateCoinReportField('vol', data.volume);
            updateCoinReportField('supply', data.supply);
        }
    })
}

function updateCoinReportField(field, value) {
    let id = '#coin-' + field;
    let s = $(id);
    let precision = 100;
    let g = s.get();
    if (g.length === 0) return;
    let el = g[0];
    if (field == 'supply') {
        el.innerHTML = value;
    } else {
        el.innerHTML = '$' + value;
    }
}

