import techan from 'techan-js';

let d3macd = techan.plot.macd;
let d3Crosshair = techan.plot.crosshair;
let axisannotation = techan.plot.axisannotation;
let financetime = techan.scale.financetime;


import {scaleLinear, select as d3Select, extent as d3Extent, zoomIdentity} from 'd3';
import {event as d3Event, bisector, line as d3Line, curveMonotoneX} from 'd3';
import {axisBottom, timeFormat, axisTop, axisLeft, axisRight, set as d3Set} from 'd3';
import {format as d3Format, zoom as d3Zoom, scaleTime as d3ScaleTime } from 'd3';
import {getDownSampledData} from "./utils";
import {throttle} from "./utils";

export function oscillatorChart(options) {
  const chart = {};
  chart.chartId = options.chartId;
  chart.chartType = options.chartType;

  const chartId = options.chartId;
  const containerHeight = options.containerHeight || 400;

  const differenceKey = options.keys[0];
  const signalKey = options.keys[1];
  const macdKey = options.keys[2];
  const macdKeys = ["signal", "macd", "difference", "zero"];
  const priceKey = options.keys[3];

  // Get chart data
  const data = options.data.map(d => ({
    date: d.date,
    signal: d[signalKey.key],
    macd: d[macdKey.key],
    difference: d[differenceKey.key],
    zero: 0
  }));
  if (priceKey) {
    data.forEach((d, i) => {
      d.price = options.data[i][priceKey.key];
    });
  }
  const downSampledData = getDownSampledData(data, options.downSampleThreshold, 'date', macdKeys);

  const initialStartingDate = options.initialStartingDate;

  const chartContainer = d3Select("#" + chartId);

  const margin = { top: 20, right: 80, bottom: 30, left: 80 };
  const containerWidth = parseInt(chartContainer.style("width"));

  const width = containerWidth - margin.left - margin.right;
  const height = containerHeight - margin.top - margin.bottom;

  const annotationTimeFormat = timeFormat("%Y-%m-%d");
  const annotationMACDFormat = d3Format(",.2f");

  const bisectDate = bisector(function(d) {
    return d.date;
  }).left;

  let selectedLegendItems;

  const macd = d3macd();

  const accessor = macd.accessor();

  let x = financetime()
    .range([0, width])
    .domain(data.map(d => d.date));
  let xInit = x.copy();
  let xZoomableInit = x.zoomable().copy();

  let xContinuous = d3ScaleTime()
    .domain([data[0].date, data[data.length - 1].date])
    .range([0, width]);
  const xContinuousInit = xContinuous.copy();

  const y = scaleLinear()
    .domain(techan.scale.plot.macd(data, accessor).domain())
    .range([height, 0]);

  macd.xScale(x).yScale(y);

  let yPrice, line;
  if (priceKey) {
    yPrice = scaleLinear()
      .domain(d3Extent(data, d => d.price))
      .range([height, 0]);

    line = d3Line()
      .curve(curveMonotoneX)
      .x(d => xContinuous(d.date))
      .y(d => yPrice(d.price));
  }

  const xAxisBottom = axisBottom().scale(x);
  const xAxisTop = axisTop().scale(x);

  const yAxisLeft = axisLeft().scale(y);
  const yAxisRight = axisRight();
  if (priceKey) {
    yAxisRight.scale(yPrice);
  } else {
    yAxisRight.scale(y);
  }

  const macdAnnotationLeft = axisannotation()
    .axis(yAxisLeft)
    .orient("left")
    .format(annotationMACDFormat);

  const macdAnnotationRight = axisannotation()
    .axis(yAxisRight)
    .orient("right")
    .translate([width, 0])
    .format(annotationMACDFormat);

  const timeAnnotationBottom = axisannotation()
    .axis(xAxisBottom)
    .orient("bottom")
    .format(annotationTimeFormat)
    .width(65)
    .translate([0, height]);

  const timeTopAnnotation = axisannotation()
    .axis(xAxisTop)
    .orient("top")
    .format(annotationTimeFormat)
    .width(65);

  const crosshair = d3Crosshair()
    .xScale(x)
    .yScale(y)
    .xAnnotation([timeAnnotationBottom, timeTopAnnotation])
    .yAnnotation([macdAnnotationLeft, macdAnnotationRight])
    .on("enter", enter)
    .on("out", out)
    .on("move", move);

  chart.zoom = d3Zoom()
    .scaleExtent([1, Infinity])
    .translateExtent([[0, 0], [width, height]])
    .extent([[0, 0], [width, height]])
    .on("zoom", zoomed);

  const svg = chartContainer
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);

  const defs = svg.append("defs");

  // Add one clipPath to limit zoom area
  const clipZoom = defs
    .append("clipPath")
    .attr("id", `${chartId}-clip-zoom`)
    .append("rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", width)
    .attr("height", height);

  // Add two clipPath for different colors
  const clipPos = defs
    .append("clipPath")
    .attr("id", `${chartId}-clip-pos`)
    .append("rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", width)
    .attr("height", y(0));

  const clipNeg = defs
    .append("clipPath")
    .attr("id", `${chartId}-clip-neg`)
    .append("rect")
    .attr("x", 0)
    .attr("y", y(0))
    .attr("width", width)
    .attr("height", height - y(0));

  const g = svg
    .append("g")
    .attr("transform", `translate(${margin.left},${margin.top})`);

  const zoomG = g.append("g").attr("clip-path", `url(#${chartId}-clip-zoom)`);

  const coordsText = g
    .append("text")
    .style("text-anchor", "end")
    .attr("class", "coords")
    .attr("x", width - 5)
    .attr("y", 15);

  // Add axes
  // Bottom x axis
  g.append("g")
    .attr("class", "x axis bottom")
    .attr("transform", `translate(0,${height})`)
    .call(xAxisBottom)
    .append("text")
    .attr("class", "axis-label")
    .attr("x", width / 2)
    .attr("dy", "3em")
    .style("text-anchor", "middle")
    .text("Time");
  // Top x axis
  g.append("g")
    .attr("class", "x axis top")
    .call(xAxisTop);
  // Left y axis
  g.append("g")
    .attr("class", "y axis left")
    .call(yAxisLeft)
    .append("text")
    .attr("class", "axis-label")
    .attr("transform", `translate(0,${height / 2})rotate(-90)`)
    .attr("dy", "-4em")
    .style("text-anchor", "middle")
    .text("Score");
  // Right y axis
  g.append("g")
    .attr("class", "y axis right")
    .attr("transform", `translate(${width},0)`)
    .call(yAxisRight);
  if (priceKey) {
    g.select(".y.axis.right")
      .append("text")
      .attr("class", "axis-label")
      .attr("transform", `translate(0,${height / 2})rotate(90)`)
      .attr("dy", "-4em")
      .style("text-anchor", "middle")
      .text("Price");
  }

  zoomG
    .selectAll(".macd")
    .data(["pos", "neg"])
    .enter()
    .append("g")
    .attr("class", d => `macd macd-${d}`)
    .attr("clip-path", d => `url(#${chartId}-clip-${d})`)
    .datum(data)
    .call(macd);

  if (priceKey) {
    zoomG
      .append("g")
      .append("path")
      .attr("class", "price")
      .datum(data)
      .attr("d", line);
  }

  const crosshairG = g
    .append("g")
    .attr("class", "crosshair")
    .datum({ x: x.domain()[x.domain().length - 1], y: null })
    .call(crosshair)
    .call(chart.zoom)
    .each(function(d) {
      move(d);
    }); // Display the last data point

  draw();

  drawLegend();

  addInitOverlay();

  const throttledDraw = throttle(draw);

  function draw() {
    // Select visible subset of data
    const xDomain = xContinuous.domain();
    const startIndex = bisectDate(data, xDomain[0]);
    let endIndex = bisectDate(data, xDomain[1]);
    if (endIndex !== data.length) endIndex++;
    let visibleData = [];
    if (endIndex - startIndex > options.downSampleThreshold) {
      visibleData = downSampledData;
    } else {
      visibleData = data.slice(startIndex, endIndex);
    }

    // Draw chart elements
    clipPos.attr("height", Math.max(0, y(0)));
    clipNeg.attr("y", y(0)).attr("height", Math.max(0, height - y(0)));
    g.select("g.macd-pos").datum(visibleData).call(macd);
    g.select("g.macd-neg").datum(visibleData).call(macd);
    g.select("g.x.axis.bottom").call(xAxisBottom);
    g.select("g.x.axis.top").call(xAxisTop);
    g.select("g.y.axis.left").call(yAxisLeft);
    g.select("g.y.axis.right").call(yAxisRight);
    if (priceKey) {
      g.select("path.price").datum(visibleData).attr("d", line);
    }
  }

  function enter() {
    coordsText.style("display", "inline");
  }

  function out() {
    coordsText.style("display", "none");
  }

  function move(coords) {
    coordsText.text(
      timeAnnotationBottom.format()(coords.x) +
        ", " +
        macdAnnotationLeft.format()(coords.y)
    );
  }

  function zoomed() {
    const transform = d3Event.transform;
    transform.y = 0;

    xContinuous = transform.rescaleX(xContinuousInit);

    // Emulates D3 behaviour, required for financetime due to secondary zoomable scale
    x.zoomable().domain(transform.rescaleX(xZoomableInit).domain());

    updateYDomains();

    throttledDraw();

    if (d3Event.sourceEvent && d3Event.sourceEvent.type !== "zoom") {
      options.dispatch.call("zoom", this, {
        chartId: chartId,
        startDate: xContinuous.domain()[0],
        endDate: xContinuous.domain()[1]
      });
    }
  }

  function updateYDomains() {
    // Rescale y scale
    const startIndex = bisectDate(data, xContinuous.domain()[0]);
    let endIndex = bisectDate(data, xContinuous.domain()[1], startIndex);
    if (endIndex !== data.length) endIndex++;

    const slicedData = data.slice(startIndex, endIndex);

    let filteredMacdKeys = macdKeys.slice();
    if (!selectedLegendItems.empty()) {
      filteredMacdKeys = macdKeys.filter(key => {
        if (selectedLegendItems.has(key) || key === "zero") {
          return true;
        } else {
          return false;
        }
      });
    }
    const yDomain = slicedData.reduce(
      (currentExtent, d) => {
        const dExtent = d3Extent(filteredMacdKeys, key => d[key]);
        const yMinNew =
          dExtent[0] < currentExtent[0] ? dExtent[0] : currentExtent[0];
        const yMaxNew =
          dExtent[1] > currentExtent[1] ? dExtent[1] : currentExtent[1];
        return [yMinNew, yMaxNew];
      },
      [0, 0]
    );
    const yDomainPadding = (yDomain[1] - yDomain[0]) * 0.1;
    y.domain([yDomain[0] - yDomainPadding, yDomain[1] + yDomainPadding]);

    if (priceKey) {
      const yPriceDomain = d3Extent(slicedData, d => d.price);
      const yPriceDomainPadding = (yPriceDomain[1] - yPriceDomain[0]) * 0.1;
      yPrice.domain([
        yPriceDomain[0] - yPriceDomainPadding,
        yPriceDomain[1] + yPriceDomainPadding
      ]);
    }
  }

  function syncZoom(startDate, endDate) {
    const firstDate = data[0].date;
    const lastDate = data[data.length - 1].date;

    xContinuous.domain([startDate, endDate]);

    const scale =
      (xContinuous(lastDate) - xContinuous(firstDate)) /
      (xContinuous(endDate) - xContinuous(startDate));
    const translateX =
      (xContinuous(firstDate) - xContinuous(startDate)) / scale;

    crosshairG.call(
      chart.zoom.transform,
      zoomIdentity.scale(scale).translate(translateX, 0)
    );
  }

  const throttledSyncZoom = throttle(syncZoom);

  chart.syncZoom = function(startDate, endDate) {
    throttledSyncZoom(startDate, endDate);
  };

  //Initial zoom
  crosshairG.call(
    chart.zoom.transform,
    zoomIdentity
      .scale(width / (x(data[data.length - 1].date) - x(initialStartingDate)))
      .translate(-x(initialStartingDate), 0)
  );

  // Legend
  function drawLegend() {
    const legendData = ["difference", "signal", "macd"];
    if (priceKey) {
      legendData.push("price");
    }
    selectedLegendItems = d3Set(legendData);
    const legend = chartContainer
      .append("div")
      .attr("class", "legend-container");
    const legendItem = legend
      .selectAll(".legend-item")
      .data(legendData)
      .enter()
      .append("div")
      .attr("class", "legend-item")
      .on("click", legendClicked);
    const legendCircle = legendItem.append("div").attr("class", function(d) {
      return "legend-circle " + d;
    });
    const legendLabel = legendItem
      .append("div")
      .attr("class", "legend-label")
      .text(function(d, i) {
        return options.keys[i].name;
      });

    function legendClicked(d) {
      if (selectedLegendItems.has(d)) {
        selectedLegendItems.remove(d);
        if (selectedLegendItems.empty()) {
          selectedLegendItems = d3Set(legendData);
        }
      } else {
        selectedLegendItems.add(d);
      }

      legendCircle.attr("class", function(d) {
        if (selectedLegendItems.has(d)) {
          return "legend-circle " + d;
        } else {
          return "legend-circle mute";
        }
      });

      g.select("g.macd-pos")
        .select(".difference")
        .style(
          "display",
          selectedLegendItems.has("difference") ? "inline" : "none"
        );
      g.select("g.macd-pos")
        .select(".macd")
        .style("display", selectedLegendItems.has("macd") ? "inline" : "none");
      g.select("g.macd-pos")
        .select(".signal")
        .style(
          "display",
          selectedLegendItems.has("signal") ? "inline" : "none"
        );
      g.select("g.macd-neg")
        .select(".difference")
        .style(
          "display",
          selectedLegendItems.has("difference") ? "inline" : "none"
        );
      g.select("g.macd-neg")
        .select(".macd")
        .style("display", selectedLegendItems.has("macd") ? "inline" : "none");
      g.select("g.macd-neg")
        .select(".signal")
        .style(
          "display",
          selectedLegendItems.has("signal") ? "inline" : "none"
        );
      g.select("path.price").style(
        "display",
        selectedLegendItems.has("price") ? "inline" : "none"
      );

      updateYDomains();

      draw();
    }
  }

  function addInitOverlay() {
    const overlay = svg
      .append("g")
      .attr("class", "overlay")
      .style("pointer-events", "all")
      .style("cursor", "pointer")
      .on("click", function() {
        options.dispatch.call("enableInteractions");
      });
    overlay
      .append("rect")
      .attr("width", "100%")
      .attr("height", "100%")
      .style("fill", "#fff")
      .style("fill-opacity", 0.6);
    overlay
      .append("text")
      .attr("x", "50%")
      .attr("y", "50%")
      .style("font-size", "1.5em")
      .attr("dy", "0.35em")
      .style("text-anchor", "middle")
      .text("Click to enable chart interactions");
  }

  chart.enableInteractions = function() {
    svg.select(".overlay").remove();
  };

  // Resize
  chart.resize = function() {};

  return chart;
}
