// import 'jquery';
// import 'datatables';
// import {} from 'datatables';
// import 'datatables.net';
// import {DataTable} from 'datatables.net-dt';
import './lib/multiselect';

// import MoversTemplate from '../../assets/hbs/compiled.js';

// import reg from '../../assets/js/handlebarsHelpers.js';
import MoversTemplate from '../hbs/pct.hbs';
// import bootstrap from 'bootstrap';
// window.bootstrap = bootstrap;


// THIS IS IMPORTANT
import dt from 'datatables.net';
let $ = jQuery;
dt(window, $);

import bs from 'datatables.net-bs';
bs(window, $);

import resbs from 'datatables.net-responsive-bs';
resbs(window,$);

// import keybs from 'datatables.net-keytable-bs';
// keybs(window,$);

import bsbuttons from 'datatables.net-buttons-bs';
bsbuttons(window, $);

// import fhbs from 'datatables.net-fixedheader-bs';
// fhbs(window,$);
//


let appInit = function( dash ){

  // Coin Table
  dash.$table = $('#dataTable.dataTable.dashboard-coin');
  dash.table = dash.$table.DataTable({
  // dash.table = DataTable({
    //dom: 'Bft',
    dom: '<Bf<"coinDataTableWrapper"t>>',
    buttons: [
      {
        className: 'btn-success',
        text: '<i class="fa fa-plus"></i> Coin',
        action: function ( e, dt, node, config ) {
          dash.addCoinRow();
        }
      },
      {
        className: 'btn-success',
        text: '<i class="fa fa-plus"></i> Column',
        action: function ( e, dt, node, config ) {
          dash.addCoinCol();
        }
      }
    ],
    pageLength: "100",
    fixedHeader: {
      header: true,
      headerOffset: 63,
      footer: false,
    },
    drawCallback: function() {
      // Remove btn-default
      $('.dt-buttons .btn').removeClass('btn-default');
    },
    responsive: true
  });


  // Coin Table - Columns
  dash.allCols = $('#all_cols_reset').html();
  dash.userCols = $('#cols_reset').html();

  dash.addCoinCol = function() {
    $('#addColModal').modal('show');
  };

  dash.col_select = $('#columns_from').multiselect({
    sort: false,
    right: '#columns_to',
    rightAll: '#cols_right_all',
    rightSelected: '#cols_right',
    leftSelected: '#cols_left',
    leftAll: '#cols_left_all',
    moveUp: '#cols_up',
    moveDown: '#cols_down',
    search: {
      right: '<input type="text" name="q" class="form-control" placeholder="Search Your Columns..." />',
      left: '<input type="text" name="q" class="form-control" placeholder="Search Available Columns..." />',
    }
  });

  dash.save_cols = function(){
    let $select = $('#columns_to');
    $select.find('option').prop('selected', true);
    let cols = $select.val();

    $.ajax({
      method: 'POST',
      url: rootmontAPI.admin_ajax_url,
      data: {
        action: 'save_cols',
        cols: cols
      }
    }).done(function(res){
      $('.modal .btn, .modal select').prop('disabled',false);

      if ( ! res.error ) {
        window.location.reload();
      } else {
        console.log( res );
      }

    });
  };

  // Coin Table - Coins (rows)
  dash.allCoins = $('#all_coins_reset').html();
  dash.userCoins = $('#coins_reset').html();

  dash.addCoinRow = function() {
    $('#addCoinModal').modal('show');
  };

  dash.coin_select = $('#coins_from').multiselect({
    sort: true,
    right: '#coins_to',
    rightAll: '#coins_right_all',
    rightSelected: '#coins_right',
    leftSelected: '#coins_left',
    leftAll: '#coins_left_all',
    moveUp: '#coins_up',
    moveDown: '#coins_down',
    search: {
      right: '<input type="text" name="q" class="form-control" placeholder="Search Your Coins..." />',
      left: '<input type="text" name="q" class="form-control" placeholder="Search All Coins..." />',
    }
  });

  dash.save_coins = function(){
    let $select = $('#coins_to');
    $select.find('option').prop('selected', true);
    let coins = $select.val();

    $.ajax({
      method: 'POST',
      url: rootmontAPI.admin_ajax_url,
      data: {
        action: 'save_coins',
        coins: coins
      }
    }).done(function(res){
      $('.modal .btn, .modal select').prop('disabled',false);

      if ( ! res.error ) {
        window.location.reload();
      } else {
        console.log( res );
      }

    });
  };

  // Coin Table - Events

  dash.reset_modal = function(e) {
    if ( ! $(this).hasClass('coins') ) {
      $('#columns_from').html('').html( dash.allCols );
      $('#columns_to').html('').html( dash.userCols );
    } else {
      $('#coins_from').html('').html( dash.allCoins );
      $('#coins_to').html('').html( dash.userCoins );
    }
  };

  dash.save_modal = function( e ) {
    e.preventDefault();
    $('.modal .btn, .modal select').prop('disabled',true);

    if ( ! $(this).hasClass('coins') ) {
      dash.save_cols();
    } else {
      dash.save_coins();
    }
  };


  dash.ntx_losers_table = $('.movers-tables').DataTable({
    dom: 't',
    order: [ [ 1, 'asc' ] ],
    columnDefs: [
      { "width": "50%", "targets": 0 }
    ],
    autoWidth: false
  });

  dash.movers_change = function( e ) {
    e.preventDefault();

    let days = $(this).val();

    if ( ! days ) {
      return false;
    }

    $('table.movers-tables tbody tr').remove();
    $('table.movers-tables tbody').html('<tr><td id="loader-gif" colspan="2"></td></tr>');

    $.ajax({
      url: window.rootmontAPI.root + 'rootmont/v1/dashboard/movers?days=' + days,
      method: 'GET',
    }).done(function(res){

      dash.ntx_losers_table.destroy();

      dash.load_ntx_movers( res );
      dash.load_price_movers( res );

      dash.ntx_losers_table = $('.movers-tables').DataTable({
        dom: 't',
        order: [ [ 1, 'asc' ] ],
        columnDefs: [
          { "width": "50%", "targets": 0 }
        ],
        autoWidth: false
      });

    });

  };

  dash.load_ntx_movers = function( res ) {
    // const source   = document.getElementById('ntx-rows-template').innerHTML;
    // const template = compile(source);
    const template = MoversTemplate;
    if ( res.ntx_winners ) {
      let html = template({
        data: res.ntx_winners
      });
      $('table#ntx-winners-table tbody').html( html );
      // let html = formatTable(res.ntx_winners);
      // $('table#ntx-winners-table tbody').html( html );
    }

    if ( res.ntx_losers ) {
      let html = template({
        data: res.ntx_losers
      });
      $('table#ntx-losers-table tbody').html( html );
      // let html = formatTable(res.ntx_losers);
      // $('table#ntx-losers-table tbody').html( html );
    }

    if ( res.ntx_active ) {
      let html = template({
        data: res.ntx_active
      });
      $('table#ntx-active-table tbody').html( html );
      // let html = formatTable(res.ntx_active);
      // $('table#ntx-active-table tbody').html( html );
    }

  };

  dash.load_price_movers = function( res ) {
    // const source   = document.getElementById('price-rows-template').innerHTML;
    // const template = compile(source);
    const template = MoversTemplate;
    if ( res.price_winners ) {
      let html = template({
        data: res.price_winners
      });
      $('table#price-winners-table tbody').html( html );
      $('table#ntx-active-table tbody').html( html );
      // let html = formatTable(res.price_winners);
      // $('table#price-winners-table tbody').html( html );
    }

    if ( res.price_losers ) {
      let html = template({
        data: res.price_losers
      });
      $('table#price-losers-table tbody').html( html );
      // let html = formatTable(res.price_losers);
      // $('table#price-losers-table tbody').html( html );
    }

    if ( res.price_active ) {
      let html = template({
        data: res.price_active
      });
      $('table#price-active-table tbody').html( html );
      // let html = formatTable(res.price_active);
      // $('table#price-active-table tbody').html( html );
    }

  };

  return dash;
};


function formatTable(data) {
  let html = '', coin;
  for (coin in data) {
    html += `<tr><td width="50%">${coin}</td><td>${prettyPct(data[coin])}</td></tr>`;
  }
  return html;
}

function prettyPct( number ) {
  let num = number * 100;
  num = Math.round( num * 100 ) / 100;
  return num + '%';
}

export function dashboardCharts() {

  const $body = $('body');
  if ( ! $body.hasClass('user-dashboard') ) {
    return false;
  }

  let dashboard = {};

  dashboard.app  = appInit( dashboard.app || {} );

  // Event Triggers
  $('#addColModal').on( 'hidden.bs.modal', dashboard.app.reset_col_modal );
  $body.on( 'click', '#reset-cols', dashboard.app.reset_modal );
  $body.on( 'click', '#save-cols',dashboard.app.save_modal );
  $body.on( 'change', '#movers-range', dashboard.app.movers_change );

}

