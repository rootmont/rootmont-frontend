import {select, timeFormat, bisector, scaleTime, axisBottom, axisLeft, line as d3Line } from 'd3';
import {scaleLinear, extent, format as d3Format, curveMonotoneX} from 'd3';
import {zoom as d3Zoom, area as d3Area, mouse as d3Mouse, event as d3Event, zoomIdentity} from 'd3';
import {getDownSampledData} from "./utils";
import {throttle} from "./utils";

export function lineChart(options) {
  const chart = {};
  chart.chartId = options.chartId;
  chart.chartType = options.chartType;

  const chartId = options.chartId;
  const containerHeight = options.containerHeight || 400;
  const key = options.keys[0];

  // Get chart data
  const data = options.data;
  const downSampledData = getDownSampledData(data, options.downSampleThreshold, 'date', options.keys);

  const initialStartingDate = options.initialStartingDate;

  const chartContainer = select("#" + chartId);

  const margin = { top: 20, right: 20, bottom: 30, left: 80 };
  const containerWidth = parseInt(chartContainer.style("width"));

  const width = containerWidth - margin.left - margin.right;
  const height = containerHeight - margin.top - margin.bottom;

  const focusCircleRadius = 6;

  const formatTime = timeFormat("%b %-d, %Y");

  const bisectDate = bisector(function(d) {
    return d.date;
  }).left;

  let x = scaleTime()
    .range([0, width])
    .domain([data[0].date, data[data.length - 1].date]);
  const xInit = x.copy();

  const y = scaleLinear()
    .range([height, 0])
    .domain(extent(data, d => d[key.key]));

  const xAxis = axisBottom()
    .scale(x)
    .tickSizeOuter(0)
    .tickSizeInner(-height)
    .ticks(getXAxisTickNumbers());

  const yAxis = axisLeft()
    .scale(y)
    .tickSizeOuter(0)
    .tickSizeInner(-width)
    .tickFormat(d => (d >= 1000 ? d3Format(".4s")(d) : d))
    .ticks(getYAxisTickNumbers());

  const line = d3Line()
    .defined(d => d)
    .curve(curveMonotoneX)
    .x(d => x(d.date))
    .y(d => y(d[key.key]));

  const area = d3Area()
    .defined(d => d)
    .curve(curveMonotoneX)
    .x(d => x(d.date))
    .y1(d => y(d[key.key]))
    .y0(d => y(0));

  chart.zoom = d3Zoom()
    .scaleExtent([1, Infinity])
    .translateExtent([[0, 0], [width, height]])
    .extent([[0, 0], [width, height]])
    .on("zoom", zoomed);

  const svg = chartContainer
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);

  // Add a clipPath to limit zoom area
  svg
    .append("clipPath")
    .attr("id", `${chartId}-clip`)
    .append("rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", width)
    .attr("height", height);

  const g = svg
    .append("g")
    .attr("transform", `translate(${margin.left},${margin.top})`);

  // Zoom base rectangle
  // This rectangle also captures mouse hover for tooltip
  const zoomRect = g
    .append("rect")
    .attr("class", "zoom-rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", width)
    .attr("height", height)
    .style("fill", "none")
    .style("pointer-events", "all")
    .on("mouseover", () => {
      options.dispatch.call("showTooltip");
    })
    .on("mousemove", function(d) {
      options.dispatch.call("updateTooltip", this, d);
    })
    .on("mouseout", () => {
      options.dispatch.call("hideTooltip");
    })
    .call(chart.zoom);

  // Add axes
  const xAxisG = g
    .append("g")
    .attr("class", "x axis")
    .attr("transform", `translate(0,${height})`)
    .call(xAxis);

  xAxisG
    .append("line")
    .attr("class", "domain")
    .attr("x1", 0)
    .attr("x2", width)
    .attr("y1", -height)
    .attr("y2", -height);

  xAxisG
    .append("text")
    .attr("class", "axis axis-label")
    .attr("transform", `translate(${width / 2},${height})`)
    .attr("dy", "2em")
    .style("text-anchor", "middle")
    .text("Time");

  const yAxisG = g
    .append("g")
    .attr("class", "y axis")
    .call(yAxis);

  yAxisG
    .append("line")
    .attr("class", "domain")
    .attr("x1", width)
    .attr("x2", width)
    .attr("y1", 0)
    .attr("y2", height);

  yAxisG
    .append("text")
    .attr("class", "axis axis-label")
    .attr("transform", `translate(0,${height / 2})rotate(-90)`)
    .attr("y", -margin.left + 20)
    .style("text-anchor", "middle")
    .text(key.name);

  // Area
  const areaPath = g
    .append("path")
    .attr("class", "area")
    .attr("clip-path", `url(#${chartId}-clip)`)
    .datum(data)
    .attr("d", area);

  // Line
  const linePath = g
    .append("path")
    .attr("class", "line")
    .attr("clip-path", `url(#${chartId}-clip)`)
    .datum(data)
    .attr("d", line);

  // Set up tooltip focus
  const focus = g
    .append("g")
    .attr("class", "focus")
    .style("pointer-events", "none")
    .style("opacity", 0); // Focus line and circles for the line chart

  focus
    .append("circle")
    .attr("class", "focus-circle")
    .attr("r", focusCircleRadius)
    .style("pointer-events", "none");

  const focusValue = focus
    .append("text")
    .attr("class", "focus-label")
    .attr("y", -focusCircleRadius)
    .attr("dy", "-0.2em")
    .style("text-anchor", "middle");

  focusValue
    .append("tspan")
    .attr("class", "focus-label-value")
    .attr("x", 0);

  focusValue
    .append("tspan")
    .attr("class", "focus-label-date")
    .attr("dy", "-1.1em")
    .attr("x", 0);

  chart.showTooltip = function() {
    focus.style("opacity", 1);
  };

  chart.updateTooltip = function() {
    const x0 = x.invert(d3Mouse(this)[0]);
    const xRound = roundDate(x0);
    const d = data
      ? data.find(d => d.date.getTime() === xRound.getTime())
      : undefined;
    const xPos = x(xRound);
    // Move the focus circle and update labels
    focus.attr("transform", d ? `translate(${xPos},${y(d[key.key])})` : "");
    focusValue.select(".focus-label-date").text(formatTime(xRound));
    const value = d[key.key];
    focusValue
      .select(".focus-label-value")
      .text(value >= 1000 ? d3Format(".4s")(value) : d3Format(".4")(value));
  };

  chart.hideTooltip = function() {
    focus.style("opacity", 0);
  };

  // Zoom handlers
  function zoomed() {
    chart.hideTooltip();
    const transform = d3Event.transform;
    transform.y = 0;

    // Select visible subset of data
    const xDomain = x.domain();
    const startIndex = bisectDate(data, xDomain[0]);
    let endIndex = bisectDate(data, xDomain[1], startIndex);
    if (endIndex !== data.length) endIndex++;
    let visibleData = [];
    if (endIndex - startIndex > options.downSampleThreshold) {
      visibleData = downSampledData;
    } else {
      visibleData = data.slice(startIndex, endIndex);
    }

    // Rescale x scale
    x = transform.rescaleX(xInit);

    // Rescale y scale
    const yDomain = extent(
      visibleData,
      d => d[key.key]
    );
    const yDomainPadding = (yDomain[1] - yDomain[0]) * 0.1;
    y.domain([yDomain[0] - yDomainPadding, yDomain[1] + yDomainPadding]);

    // Update x axis
    xAxisG.call(xAxis.scale(x));
    yAxisG.call(yAxis.scale(y));

    // Update the line charts
    linePath
      .datum(visibleData)
      .attr("d", line);
    areaPath
      .datum(visibleData)
      .attr("d", area);

    if (d3Event.sourceEvent && d3Event.sourceEvent.type !== "zoom") {
      options.dispatch.call("zoom", this, {
        chartId: chartId,
        startDate: x.domain()[0],
        endDate: x.domain()[1]
      });
    }
  }

  function syncZoom(startDate, endDate) {
    const firstDate = data[0].date;
    const lastDate = data[data.length - 1].date;

    x.domain([startDate, endDate]);

    const scale = (x(lastDate) - x(firstDate)) / (x(endDate) - x(startDate));
    const translateX = (x(firstDate) - x(startDate)) / scale;

    zoomRect.call(
      chart.zoom.transform,
      zoomIdentity.scale(scale).translate(translateX, 0)
    );
  }

  const throttledSyncZoom = throttle(syncZoom);

  chart.syncZoom = function(startDate, endDate) {
    throttledSyncZoom(startDate, endDate);
  };

  // Initial zoom
  zoomRect.call(
    chart.zoom.transform,
    zoomIdentity
      .scale(width / (x(data[data.length - 1].date) - x(initialStartingDate)))
      .translate(-x(initialStartingDate), 0)
  );

  addInitOverlay();

  function addInitOverlay() {
    const overlay = svg
      .append("g")
      .attr("class", "overlay")
      .style("pointer-events", "all")
      .style("cursor", "pointer")
      .on("click", function() {
        options.dispatch.call("enableInteractions");
      });
    overlay
      .append("rect")
      .attr("width", "100%")
      .attr("height", "100%")
      .style("fill", "#fff")
      .style("fill-opacity", 0.6);
    overlay
      .append("text")
      .attr("x", "50%")
      .attr("y", "50%")
      .style("font-size", "1.5em")
      .attr("dy", "0.35em")
      .style("text-anchor", "middle")
      .text("Click to enable chart interactions");
  }

  chart.enableInteractions = function() {
    svg.select(".overlay").remove();
  };

  // Resize
  chart.resize = function() {
    const containerWidth = parseInt(chartContainer.style("width"));

    const width = containerWidth - margin.left - margin.right;

    xInit.range([0, width]);
    x.range([0, width]);

    chart.zoom
      .translateExtent([[0, 0], [width, height]])
      .extent([[0, 0], [width, height]]);

    svg.attr("width", width + margin.left + margin.right);

    svg
      .select("clipPath")
      .select("rect")
      .attr("width", width);

    zoomRect.attr("width", width);

    xAxisG.select("line.domain").attr("x2", width);

    xAxisG
      .select(".axis-label")
      .attr("transform", `translate(${width / 2},${height})`);

    yAxisG
      .select("line.domain")
      .attr("x1", width)
      .attr("x2", width);

    // Update zoom
    const firstDate = data[0].date;
    const lastDate = data[data.length - 1].date;
    const startDate = x.domain()[0];
    const endDate = x.domain()[1];

    const scale = (x(lastDate) - x(firstDate)) / (x(endDate) - x(startDate));
    const translateX = (x(firstDate) - x(startDate)) / scale;
    zoomRect.call(
      chart.zoom.transform,
      zoomIdentity.scale(scale).translate(translateX, 0)
    );
  };

  // Utilities
  // https://stackoverflow.com/a/27323864/7612054
  function roundDate(date) {
    const d = new Date(date.getTime());
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    return d;
  }

  function getXAxisTickNumbers() {
    return Math.floor(width / 100);
  }

  function getYAxisTickNumbers() {
    return Math.floor(height / 80);
  }

  return chart;
}
