// import {accessor,scale} from 'techan';
// import {timeParse, scaleLinear, line as d3Line, extent, axisBottom, axisLeft  } from 'd3';
// import {format, select, json, ascending} from 'd3';
//
// function volumeChart(symbol) {
//     const BASEURL = window.rootmontAPI.root + 'rootmont/v1/price-chart/';
//     const url = BASEURL + symbol + '/';
//
//     var margin = {top: 20, right: 20, bottom: 30, left: 50},
//         width = 960 - margin.left - margin.right,
//         height = 500 - margin.top - margin.bottom;
//
//     var data = [];
//
//     const parseTime = timeParse("%Y-%m-%dT%H:%M:%S.%LZ");
//
//     var x = scale.financetime()
//         .range([0, width]);
//
//     var y = scaleLinear()
//         .range([height, 0])
//         .domain(extent(data, d => d.price));
//
//     var volume = techan.plot.volume()
//         .accessor(accessor.ohlc())   // For volume bar highlighting
//         .xScale(x)
//         .yScale(y);
//
//     const line = d3Line()
//         .x(d => x(d.date))
//         .y(d => y(d.price));
//
//     var xAxis = axisBottom(x)
//         .scale(x);
//
//     var yAxis = axisLeft(y)
//         .tickFormat(format(",.3s"))
//         .scale(y);
//
//     var svg = select("body").append("svg")
//         .attr("width", width + margin.left + margin.right)
//         .attr("height", height + margin.top + margin.bottom)
//         .append("g")
//         .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
//
//     var accessor = volume.accessor();
//     // Fetch data
//     json(url).then(function(json) {
//         data = json.map(function (d) {
//             return {
//                 date: parseTime(d.date),
//                 volume: d.trading_volume,
//                 price: d.price
//             };
//         }).sort(function (a,b) {
//             return ascending(accessor.d(a), accessor.d(b));
//         });
//
//         svg.append("g")
//             .attr("class", "volume");
//
//         svg.append("g")
//             .attr("class", "x axis")
//             .attr("transform", "translate(0," + height + ")");
//
//         svg.append("g")
//             .attr("class", "y axis")
//             .append("text")
//             .attr("transform", "rotate(-90)")
//             .attr("y", 6)
//             .attr("dy", ".71em")
//             .style("text-anchor", "end")
//             .text("Volume");
//
//         // Line
//         svg.append("g")
//             .append("path")
//             .attr("class", "line")
// //            .attr("clip-path", `url(#${chartId}-clip)`)
//             .datum(data)
//             .attr("d", line);
//
//         draw(data);
//
//     });
//
//
//     function draw(data) {
//         x.domain(data.map(volume.accessor().d));
//         y.domain(scale.techan.scale(.volume(data, volume.accessor().v).domain());
//
//         svg.selectAll("g.volume").datum(data).call(volume);
//         svg.selectAll("g.line").datum(data).call(line);
//         svg.selectAll("g.x.axis").call(xAxis);
//         svg.selectAll("g.y.axis").call(yAxis);
//     }
// }