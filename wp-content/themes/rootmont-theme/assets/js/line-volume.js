function lineVolChart(options) {
  const chart = {};
  chart.chartId = options.chartId;
  chart.chartType = options.chartType;

  const chartId = options.chartId;
  const containerHeight = options.containerHeight || 400;
  const key = options.keys[0];
  const keys = ["volume", "open", "high", "low", "close"];

  const parseTime = d3.timeParse("%Y-%m-%dT%H:%M:%S.%LZ");

  // Get chart data

  const data = options.data.map(d => ({
      date: parseTime(d.date),
      volume: d.trading_volume,
      open: d.price,
      high: d.price,
      low: d.price,
      close: d.price
  }));

  const downSampledData = getDownSampledData(data, options.downSampleThreshold, 'date', keys);

  const initialStartingDate = options.initialStartingDate;

  const chartContainer = d3.select("#" + chartId);

  const margin = {top: 20, right: 20, bottom: 30, left: 50};
  const containerWidth = parseInt(chartContainer.style("width"));

  const width = containerWidth - margin.left - margin.right;
  const height = containerHeight - margin.top - margin.bottom;

  let x = d3
      .scaleTime()
      .range([0, width])
      .domain([data[0].date, data[data.length - 1].date]);

  const xInit = x.copy();

  let y = d3
      .scaleLinear()
      .range([height, 0])
      .domain(d3.extent(data, d => d.close));

  let yVolume = d3
      .scaleLinear()
      .range([y(0), y(0.4)])
      .domain(d3.extent(data, d => d.volume));

  let volume = techan.plot.volume()
      .accessor(techan.accessor.ohlc())   // For volume bar highlighting
      .xScale(x)
      .yScale(yVolume);

  var close = d3.line()
      .x(d => d.date)
      .y(d => d.close);

  var volumeAxis = d3
      .axisLeft(yVolume)
      .ticks(5)
      .tickFormat(d3.format("s",2));

  var volumeAnnotation = techan.plot.axisannotation()
      .axis(volumeAxis)
      .orient("right")
      .width(35);

  const xAxis = d3
      .axisBottom()
      .scale(x)
      .tickSizeOuter(0)
      .tickSizeInner(-height)
      .ticks(getXAxisTickNumbers());

  const yAxis = d3
      .axisLeft()
      .scale(y)
      .tickSizeOuter(0)
      .tickSizeInner(-width)
      .tickFormat(d => (d >= 1000 ? d3.format(".4s")(d) : d))
      .ticks(getYAxisTickNumbers());

  const svg = chartContainer
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom);

  const focusCircleRadius = 6;

  const formatTime = d3.timeFormat("%b %-d, %Y");

  const bisectDate = d3.bisector(function(d) {
    return d.date;
  }).left;

  chart.zoom = d3
    .zoom()
    .scaleExtent([1, Infinity])
    .translateExtent([[0, 0], [width, height]])
    .extent([[0, 0], [width, height]])
    .on("zoom", zoomed);

  const defs = svg.append("defs");

  // Add one clipPath to limit zoom area
  const clipZoom = defs
      .append("clipPath")
      .attr("id", `${chartId}-clip-zoom`)
      .append("rect")
      .attr("x", 0)
      .attr("y", 0)
      .attr("width", width)
      .attr("height", height);

  // Add two clipPath for different colors
  const clipPos = defs
      .append("clipPath")
      .attr("id", `${chartId}-clip-pos`)
      .append("rect")
      .attr("x", 0)
      .attr("y", 0)
      .attr("width", width)
      .attr("height", y(0));

  const clipNeg = defs
      .append("clipPath")
      .attr("id", `${chartId}-clip-neg`)
      .append("rect")
      .attr("x", 0)
      .attr("y", y(0))
      .attr("width", width)
      .attr("height", height - y(0));

  // Add a clipPath to limit zoom area
  svg
      .append("clipPath")
      .attr("id", `${chartId}-clip`)
      .append("rect")
      .attr("x", 0)
      .attr("y", 0)
      .attr("width", width)
      .attr("height", height);

  const g = svg
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

  // Zoom base rectangle
  // This rectangle also captures mouse hover for tooltip
  const zoomRect = g
      .append("rect")
      .attr("class", "zoom-rect")
      .attr("x", 0)
      .attr("y", 0)
      .attr("width", width)
      .attr("height", height)
      .style("fill", "none")
      .style("pointer-events", "all")
      .on("mouseover", () => {
        options.dispatch.call("showTooltip");
      })
      .on("mousemove", function(d) {
        options.dispatch.call("updateTooltip", this, d);
      })
      .on("mouseout", () => {
        options.dispatch.call("hideTooltip");
      })
      .call(chart.zoom);

  // Add axes
  const xAxisG = g
      .append("g")
      .attr("class", "x axis")
      .attr("transform", `translate(0,${height})`)
      .call(xAxis);

  xAxisG
      .append("line")
      .attr("class", "domain")
      .attr("x1", 0)
      .attr("x2", width)
      .attr("y1", -height)
      .attr("y2", -height);

  xAxisG
      .append("text")
      .attr("class", "axis axis-label")
      .attr("transform", `translate(${width / 2},${height})`)
      .attr("dy", "2em")
      .style("text-anchor", "middle")
      .text("Time");

  const yAxisG = g
      .append("g")
      .attr("class", "y axis")
      .call(yAxis);

  yAxisG
      .append("line")
      .attr("class", "domain")
      .attr("x1", width)
      .attr("x2", width)
      .attr("y1", 0)
      .attr("y2", height);

  // Set up tooltip focus
  const focus = g
    .append("g")
    .attr("class", "focus")
    .style("pointer-events", "none")
    .style("opacity", 0); // Focus line and circles for the line chart

  focus
    .append("circle")
    .attr("class", "focus-circle")
    .attr("r", focusCircleRadius)
    .style("pointer-events", "none");

  const focusValue = focus
    .append("text")
    .attr("class", "focus-label")
    .attr("y", -focusCircleRadius)
    .attr("dy", "-0.2em")
    .style("text-anchor", "middle");

  focusValue
    .append("tspan")
    .attr("class", "focus-label-value")
    .attr("x", 0);

  focusValue
    .append("tspan")
    .attr("class", "focus-label-date")
    .attr("dy", "-1.1em")
    .attr("x", 0);

  const volumePath = g
    .attr("class", "volume");
//    .attr("stroke", "#1121ff")
//    .attr("d", volume);

  const closePath = g
    .append("path")
    .attr("class", "line")
    .attr("clip-path", `url(#${chartId}-clip)`)
    .datum(data)
    .attr("stroke", "#ffc111")
    .attr("d", close);

  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")");

  svg.append("g")
    .attr("class", "volume axis");

  svg.append("g")
    .attr("class", "y axis")
    .append("text")
    //.attr("transform", "rotate(90)")
    .attr("y", -18)
    //.attr("x", 50)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .text("Price ($)");

  svg.append("g")
    .attr("transform", "translate(" + x(1) + ",0)")
    .attr("class", "axis right")
    .append("text")
    //.attr("stroke", "#1121ff")
    //.attr("transform", "rotate(90)")
    .attr("y", height * 0.55)
    .attr("dy", ".71em")
    //.attr("x", -height * 0.5)
    .style("text-anchor", "end")
    .text("Volume ($)");


  chart.showTooltip = function() {
    focus.style("opacity", 1);
  };

  chart.updateTooltip = function() {
    const x0 = x.invert(d3.mouse(this)[0]);
    const xRound = roundDate(x0);
    const d = data
      ? data.find(d => d.date.getTime() === xRound.getTime())
      : undefined;
    const xPos = x(xRound);
    // Move the focus circle and update labels
    focus.attr("transform", d ? `translate(${xPos},${y(d[key.key])})` : "");
    focusValue.select(".focus-label-date").text(formatTime(xRound));
    const value = d[key.key];
    focusValue
      .select(".focus-label-value")
      .text(value >= 1000 ? d3.format(".4s")(value) : d3.format(".4")(value));
  };

  chart.hideTooltip = function() {
    focus.style("opacity", 0);
  };

  // draw();

  addInitOverlay();

  // const throttledDraw = throttle(draw);
  //
  // function draw() {
  //   // Select visible subset of data
  //   const xDomain = x.domain();
  //   const startIndex = bisectDate(data, xDomain[0]);
  //   let endIndex = bisectDate(data, xDomain[1]);
  //   if (endIndex !== data.length) endIndex++;
  //   let visibleData = [];
  //   if (endIndex - startIndex > options.downSampleThreshold) {
  //     visibleData = downSampledData;
  //   } else {
  //     visibleData = data.slice(startIndex, endIndex);
  //   }
  //
  //   // Draw chart elements
  //   clipPos.attr("height", Math.max(0, y(0)));
  //   clipNeg.attr("y", y(0)).attr("height", Math.max(0, height - y(0)));
  //   //y.domain(techan.scale.plot.volume(data, volume.accessor().v).domain());
  //   //y.domain(d3.extent(data, d => d.price));
  //   y.domain(techan.scale.plot.ohlc(data, close.accessor()).domain());
  //
  //   svg.select("g.volume").datum(visibleData).call(volume);
  //   svg.select("g.axis.right").call(volumeAxis);
  //   svg.select("g.close").datum(visibleData).call(close);
  //   svg.select("g.x.axis").call(xAxis);
  //   svg.select("g.y.axis").call(yAxis);
  //
  // }


  // Zoom handlers
  function zoomed() {
    chart.hideTooltip();
    const transform = d3.event.transform;
    transform.y = 0;

    // Select visible subset of data
    const xDomain = x.domain();
    const startIndex = bisectDate(data, xDomain[0]);
    let endIndex = bisectDate(data, xDomain[1], startIndex);
    if (endIndex !== data.length) endIndex++;
    let visibleData = [];
    if (endIndex - startIndex > options.downSampleThreshold) {
      visibleData = downSampledData;
    } else {
      visibleData = data.slice(startIndex, endIndex);
    }

    // Rescale x scale
    x = transform.rescaleX(xInit);

    // Rescale y scale
    const yDomain = d3.extent(
        visibleData,
        d => d.close
    );
    const yDomainPadding = (yDomain[1] - yDomain[0]) * 0.1;
    y.domain([yDomain[0] - yDomainPadding, yDomain[1] + yDomainPadding]);

    // Update x axis
    xAxisG.call(xAxis.scale(x));
    yAxisG.call(yAxis.scale(y));

    // // Update the line charts
    // closePath
    //     .datum(visibleData)
    //     .attr("d", close);
    // volumePath
    //     .datum(visibleData)
    //     .call(volume);

    svg.select("g.volume").datum(visibleData).call(volume);
    svg.select("g.axis.right").call(volumeAxis);
    svg.select("g.line").datum(visibleData).call(close);

    if (d3.event.sourceEvent && d3.event.sourceEvent.type !== "zoom") {
      options.dispatch.call("zoom", this, {
        chartId: chartId,
        startDate: x.domain()[0],
        endDate: x.domain()[1]
      });
    }
  }


  function syncZoom(startDate, endDate) {
    const firstDate = data[0].date;
    const lastDate = data[data.length - 1].date;

    x.domain([startDate, endDate]);

    const scale = (x(lastDate) - x(firstDate)) / (x(endDate) - x(startDate));
    const translateX = (x(firstDate) - x(startDate)) / scale;

    zoomRect.call(
      chart.zoom.transform,
      d3.zoomIdentity.scale(scale).translate(translateX, 0)
    );
  }

  const throttledSyncZoom = throttle(syncZoom);

  chart.syncZoom = function(startDate, endDate) {
    throttledSyncZoom(startDate, endDate);
  };

  // Initial zoom
  zoomRect.call(
    chart.zoom.transform,
    d3.zoomIdentity
      .scale(width / (x(data[data.length - 1].date) - x(initialStartingDate)))
      .translate(-x(initialStartingDate), 0)
  );

  function addInitOverlay() {
    const overlay = svg
      .append("g")
      .attr("class", "overlay")
      .style("pointer-events", "all")
      .style("cursor", "pointer")
      .on("click", function() {
        options.dispatch.call("enableInteractions");
      });
    overlay
      .append("rect")
      .attr("width", "100%")
      .attr("height", "100%")
      .style("fill", "#fff")
      .style("fill-opacity", 0.6);
    overlay
      .append("text")
      .attr("x", "50%")
      .attr("y", "50%")
      .style("font-size", "1.5em")
      .attr("dy", "0.35em")
      .style("text-anchor", "middle")
      .text("Click to enable chart interactions");
  }

  chart.enableInteractions = function() {
    svg.select(".overlay").remove();
  };


  // Resize
  chart.resize = function() {};

  function getXAxisTickNumbers() {
    return Math.floor(width / 100);
  }

  function getYAxisTickNumbers() {
    return Math.floor(height / 80);
  }

  return chart;
}
