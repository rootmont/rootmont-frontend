const metcalfeLineChart = function(options) {
  const chartid = options.chartid || "chart";
  const symbol = options.symbol;
  const series = options.series;
  const height = options.height || 600;

  const BASEURL = window.rootmontAPI.root + 'rootmont/v1/coin-chart/';
  const url = BASEURL + symbol;

  const divChart = d3.select("#" + chartid);
  const divLoader = d3.select(".lds-roller-container.line");

  const marginLine = { top: 40, right: 20, bottom: 50, left: 80 };
  const containerWidth = parseInt(divChart.style("width"));
  console.log( series, ' width:', containerWidth );

  const widthLine = containerWidth - marginLine.left - marginLine.right;
  const heightLine = height - marginLine.top - marginLine.bottom;

  const numMonthsToDisplay = 3; // Number of months to display
  const focusCircleRadius = 6;

  // Data
  let dataLine;

  const parseTime = d3.timeParse("%Y-%m-%d");
  const formatTime = d3.timeFormat("%b %-d, %Y");

  const xLine = d3.scaleTime().range([0, widthLine]);

  let xLineNew = xLine; // Zoom transformed xLine scale

  const yLine = d3.scaleLinear().range([heightLine, 0]);

  const xAxisLine = d3
    .axisBottom()
    .scale(xLine)
    .tickSizeOuter(0)
    .tickSizeInner(-heightLine)
    .ticks(getXAxisTickNumbers());

  const yAxisLine = d3
    .axisLeft()
    .scale(yLine)
    .tickSizeOuter(0)
    .tickSizeInner(-widthLine)
    .tickFormat(d => (d >= 1000 ? d3.format(".2s")(d) : d))
    .ticks(getYAxisTickNumbers());

  const line = d3
    .line()
    .defined(d => d)
    .x(d => xLine(d[0]))
    .y(d => yLine(d[1]));

  const area = d3
    .area()
    .defined(d => d)
    .x(d => xLine(d[0]))
    .y1(d => yLine(d[1]))
    .y0(d => yLine(0));

  const zoom = d3
    .zoom()
    .extent([[0, 0], [widthLine, heightLine]])
    .scaleExtent([1, 1])
    .on("zoom", zoomed);

  const svgLine = divChart
    .append("svg")
    .attr("class", "line-chart")
    .attr("width", widthLine + marginLine.left + marginLine.right)
    .attr("height", heightLine + marginLine.top + marginLine.bottom);

  const gLine = svgLine
    .append("g")
    .attr("transform", `translate(${marginLine.left},${marginLine.top})`);

  const gLineLines = gLine.append("g").attr("class", "line-chart-lines");

  // Zoom base rectangle
  // This rectangle also captures mouse hover for tooltip
  const zoomRect = gLine
    .append("rect")
    .attr("class", "zoom-rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", widthLine)
    .attr("height", heightLine)
    .style("fill", "none")
    .style("pointer-events", "all")
    .on("mouseover", showLineTooltip)
    .on("mousemove", updateLineTooltip)
    .on("mouseout", hideTooltip)
    .call(zoom);

  // Add a clipPath to limit zoom area
  svgLine
    .append("clipPath")
    .attr("id", "clip")
    .append("rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", widthLine)
    .attr("height", heightLine);

  // Add tooltips
  const tooltip = d3
    .select("#" + chartid)
    .append("div")
    .attr("class", "tooltip")
    .on("mouseenter", function() {
      d3.select(this)
        .style("display", "inline")
        .transition()
        .style("opacity", 1);
    })
    .on("mouseleave", function() {
      d3.select(this)
        .transition()
        .style("opacity", 0)
        .on("end", () => {
          tooltip.style("display", "none");
        });
    });

  const focus = gLine
    .append("g")
    .attr("class", "focus")
    .style("opacity", 0); // Focus line and circles for the line chart

  //loading(divChart.node());
  // d3.json(url, {
  //   headers: {
  //     "X-WP-Nonce": window.rootmontAPI.nonce
  //   }
  // }).then(data => {
  //   processLineData(data);
  //   setupLineChart();
  //   renderLineChart();
  //   divLoader.style("display", "none");
  // });

  console.log( 'line pre ajax' );

  jQuery.ajax( {
    url: url,
    method: 'GET',
    beforeSend: function ( xhr ) {
      xhr.setRequestHeader( 'X-WP-Nonce', rootmontAPI.nonce );
    },
  } ).done( function ( data ) {
    console.log( 'chartId & data ', chartid, data );
    processLineData(data);
    setupLineChart();
    renderLineChart();
    divLoader.style("display", "none");
  } );

  function processLineData(data) {
    dataLine = data[series].map(d => [parseTime(d.date), +d.score]);
  }

  function setupLineChart() {
    // Add axes
    gLine
      .insert("g", ".line-chart-lines")
      .attr("class", "x axis")
      .attr("transform", `translate(0,${heightLine})`);

    gLine
      .append("text")
      .attr("class", "axis axis-label")
      .attr("transform", `translate(${widthLine / 2},${heightLine})`)
      .attr("dy", "2em")
      .style("text-anchor", "middle")
      .text("Time");

    gLine.insert("g", ".line-chart-lines").attr("class", "y axis");

    gLine
      .append("text")
      .attr("class", "axis axis-label")
      .attr("transform", `translate(0,${heightLine / 2})rotate(-90)`)
      .attr("y", -marginLine.left + 20)
      .style("text-anchor", "middle")
      .text(seriesToDisplay(series));

    // Set up tooltip focus
    focus
      .append("line")
      .attr("class", "focus-line")
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", 0)
      .attr("y2", heightLine)
      .style("pointer-events", "none");

    focus
      .append("circle")
      .attr("class", "focus-circle focus-circle")
      .attr("r", focusCircleRadius)
      .style("pointer-events", "none");
  }

  function renderLineChart() {
    // Update axes
    const [xMin, xMax] = d3.extent(dataLine, d => d[0]);
    const xStart = getStartingDate(xMin, xMax, numMonthsToDisplay);
    const truncatedDataLine = dataLine.filter(d=> d[0] >= xStart)

    xLine.domain([xStart, xMax]);
    //yLine.domain(d3.extent(dataLine, d => d[1])).nice();
    yLine.domain(d3.extent(truncatedDataLine, d => d[1])).nice();

    // Update zoom translateExtent
    zoom.translateExtent([[xLine(xMin), 0], [xLine(xMax), heightLine]]);
    zoomRect.call(zoom.transform, d3.zoomIdentity);

    // Add axes
    gLine.select(".x.axis").call(xAxisLine);

    gLine.select(".y.axis").call(yAxisLine);

    // Area
    gLineLines
      .append("path")
      .attr("class", "area area")
      .attr("clip-path", "url(#clip)")
      .datum(dataLine)
      .attr("d", area);

    // Line
    gLineLines
      .append("path")
      .attr("class", "line line")
      .attr("clip-path", "url(#clip)")
      .datum(dataLine)
      .attr("d", line);
  }

  function showLineTooltip() {
    tooltip.transition().style("opacity", 1);
    focus.style("opacity", 1);
  }

  function updateLineTooltip() {
    const x0 = xLineNew.invert(d3.mouse(this)[0]);
    const xRound = roundDate(x0);

    const d = dataLine
      ? dataLine.find(d => d[0].getTime() === xRound.getTime())
      : undefined;

    const xPos = xLineNew(xRound);

    // Move the focus line
    focus
      .select(".focus-line")
      .attr("x1", xPos)
      .attr("x2", xPos);

    // Move the focus circle
    focus
      .select(".focus-circle")
      .attr("transform", d ? `translate(${xPos},${yLine(d[1])})` : "")
      .style("opacity", d && xPos === xLineNew(d[0]) ? 1 : 0);

    // Update tooltip content
    let html = `
      <div>${formatTime(xRound)}</div>
      <div>${symbol}</div>
    `;

    if (d && xPos === xLineNew(d[0])) {
      html += `<div><span class="tooltip-circle"></span> ${seriesToDisplay(
        series
      )}: <span class="tooltip-value">${d[1]}</span></div>`;
    }

    tooltip.html(html);
    tooltip.attr("class", "tooltip").style("display", "inline");
    // Update tooltip position
    const tooltipBCR = tooltip.node().getBoundingClientRect();
    const top = d3.event.pageY - tooltipBCR.height / 2;
    const left =
      d3.event.pageX - tooltipBCR.width - 10 < 0
        ? d3.event.pageX + 10
        : d3.event.pageX - tooltipBCR.width - 10;
    tooltip.style("left", left + "px").style("top", top + "px");
  }

  function hideTooltip() {
    tooltip
      .transition()
      .style("opacity", 0)
      .on("end", () => {
        tooltip.style("display", "none");
      });
    focus.style("opacity", 0);
  }

  function zoomed() {
    const transform = d3.event.transform;
    // Rescale x scale
    xLineNew = transform.rescaleX(xLine);
    // Update x axis
    gLine.select(".x.axis").call(xAxisLine.scale(xLineNew));
    // Update the line charts
    line.x(d => xLineNew(d[0]));
    gLineLines
      .selectAll(".line")
      .filter(d => d)
      .attr("d", line);
    area.x(d => xLineNew(d[0]));
    gLineLines
      .selectAll(".area")
      .filter(d => d)
      .attr("d", area);
  }

  function loading(node) {
    const nodeBCR = node.getBoundingClientRect();
    divLoader
      .style("display", null)
      .style("left", nodeBCR.left + "px")
      .style("top", nodeBCR.top + "px")
      .style("width", nodeBCR.width + "px")
      .style("height", nodeBCR.height + "px");
  }

  function getStartingDate(xMin, xMax, numMonthsToDisplay) {
    return xMin > addMonths(xMax, -numMonthsToDisplay)
      ? xMin
      : addMonths(xMax, -numMonthsToDisplay);
  }

  function addMonths(date, months) {
    let result = new Date(date);
    result.setMonth(result.getMonth() + months);
    return result;
  }

  function seriesToDisplay(series) {
    return series.replace(/_/g, " ").replace(/\b\S/g, function(t) {
      return t.toUpperCase();
    });
  }

  function getXAxisTickNumbers() {
    return Math.floor(widthLine / 100);
  }

  function getYAxisTickNumbers() {
    return Math.floor(heightLine / 50);
  }

  // https://stackoverflow.com/a/27323864/7612054
  function roundDate(date) {
    const d = new Date(date.getTime());
    d.setHours(0);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    return d;
  }
};

let $ = jQuery;

$( document ).ready( function() {

  let $rawMetcalfe = $( '#raw-metcalfe' );
  let $metcalfeMovement = $( '#metcalfe_movement' );
  let $metcalfePriceRatio = $( '#metcalfe_price_ratio' );
  const symbol = $( '.line-charts-wrapper' ).data( 'symbol' );

  if ( ! symbol ) {
    return false;
  }

  let charts = [];

  if ( $rawMetcalfe.length ) {
    const options1 = {
      chartid: "raw-metcalfe",
      symbol: symbol,
      series: "raw_metcalfe",
      height: 400
    };
    charts.push( options1 );
  }

  if ( $metcalfeMovement.length ) {
    const options2 = {
      chartid: "metcalfe_movement",
      symbol: symbol,
      series: "metcalfe_movement",
      height: 400
    };
    charts.push( options2 );
  }

  if ( $metcalfePriceRatio.length ) {
    const options3 = {
      chartid: "metcalfe_price_ratio",
      symbol: symbol,
      series: "metcalfe_price_ratio",
      height: 400
    };
    charts.push( options3 );
  }

  for(i=0;i<charts.length;i++) {
    metcalfeLineChart( charts[i] );
  }

});
