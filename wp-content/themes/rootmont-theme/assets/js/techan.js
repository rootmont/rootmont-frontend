export const macd = function(accessor_macd, plot, plotMixin) {  // Injected dependencies
    return function() { // Closure function
        var p = {},  // Container for private, direct access mixed in variables
            differenceGenerator,
            macdLine = plot.pathLine(),
            signalLine = plot.pathLine();

        function macd(g) {
            var group = p.dataSelector(g);

            group.selection.append('path').attr('class', 'difference');
            group.selection.append('path').attr('class', 'zero');
            group.selection.append('path').attr('class', 'macd');
            group.selection.append('path').attr('class', 'signal');

            macd.refresh(g);
        }

        macd.refresh = function(g) {
            refresh(p.dataSelector.select(g), p.accessor, p.xScale, p.yScale, plot, differenceGenerator, macdLine, signalLine);
        };

        function binder() {
            differenceGenerator = plot.joinPath(differencePath);
            macdLine.init(p.accessor.d, p.xScale, p.accessor.m, p.yScale);
            signalLine.init(p.accessor.d, p.xScale, p.accessor.s, p.yScale);
        }

        function differencePath() {
            var accessor = p.accessor,
                x = p.xScale,
                y = p.yScale,
                width = p.width(x);

            return function(d) {
                var zero = y(0),
                    height = y(accessor.dif(d)) - zero,
                    xValue = x(accessor.d(d)) - width/2;

                return 'M ' + xValue + ' ' + zero + ' l 0 ' + height + ' l ' + width +
                    ' 0 l 0 ' + (-height);
            };
        }

        // Mixin 'superclass' methods and variables
        plotMixin(macd, p).plot(accessor_macd(), binder).width(binder).dataSelector(plotMixin.dataMapper.array);
        binder();

        return macd;
    };
};

function refresh(selection, accessor, x, y, plot, differenceGenerator, macdLine, signalLine) {
    selection.select('path.difference').attr('d', differenceGenerator);
    selection.select('path.zero').attr('d', plot.horizontalPathLine(accessor.d, x, accessor.z, y));
    selection.select('path.macd').attr('d', macdLine);
    selection.select('path.signal').attr('d', signalLine);
}

/*
 Finance time scale which is not necessarily continuous, is required to be plot continuous. Finance scale
 generally contains data points on days where a market is open but no points when closed, such as weekday
 and weekends respectively. When plot, is done so without weekend gaps.
 */
export const financetime = function(d3_scale_linear, d3_time, d3_bisect, techan_util_rebindCallback, scale_widen, techan_scale_zoomable) {  // Injected dependencies
    function financetime(tickMethods, genericFormat, index, domain, padding, outerPadding, zoomLimit, closestTicks, zoomable) {
        var dateIndexMap,
            tickState = { tickFormat: tickMethods.daily[tickMethods.daily.length-1][2] },
            band = 3;

        index = index || d3_scale_linear();
        domain = domain || [new Date(0), new Date(1)];
        padding = padding === undefined ? 0.2 : padding;
        outerPadding = outerPadding === undefined ? 0.65 : outerPadding;
        zoomLimit = zoomLimit || { domain: index.domain() }; // Wrap in object to carry onto zoomable
        closestTicks = closestTicks || false;
        zoomable = zoomable || techan_scale_zoomable(index, zoomed, zoomLimit);

        /**
         * Scales the value to domain. If the value is not within the domain, will currently brutally round the data:
         * - If before min domain, will round to 1 index value before min domain
         * - If after max domain, will round to 1 index value after min domain
         * - If within domain, but not mapped to domain value, uses d3.bisect to find nearest domain index
         *
         * This logic was not required until the domain was being updated and scales re-rendered and this line
         * https://github.com/mbostock/d3/blob/abbe1c75c16c3e9cb08b1d0872f4a19890d3bb58/src/svg/axis.js#L107 was causing error.
         * New scale generated ticks that old scale did not have, causing error during transform. To avoid error this logic
         * was added.
         *
         * @param x The value to scale
         * @param offset Apply an index offset to the mapped x (date) parameter
         * @returns {*}
         */
        function scale(x, offset) {
            var mappedIndex = dateIndexMap[x instanceof Date ? x.getTime() : +x];
            offset = offset || 0;

            // Make sure the value has been mapped, if not, determine if it's just before, round in, or just after domain
            if(mappedIndex === undefined) {
                if(domain[0] > x) mappedIndex = -1; // Less than min, round just out of domain
                else mappedIndex = d3_bisect(domain, x); // else let bisect determine where in or just after than domain it is
            }

            return index(mappedIndex + offset);
        }

        /**
         * Invert the passed range coordinate to the corresponding domain. Returns null if no valid domain available.
         *
         * @param y
         * @returns {null} If the range value cannot be mapped. eg, if range value is outside of the mapped domain
         */
        scale.invert = function(y) {
            var d = domain[scale.invertToIndex(y)];
            return d ? d : null;
        };

        /**
         * Inverts the coordinate to the corresponding domain. <b>NOTE: </b> May return values outside of the domain such
         * as negative indexes, or an index greater than what is available in the domain.
         *
         * @param y
         * @returns {number} A number representing the index in the domain the range value has been inverted to. May return
         * values outside of the domain such as negatives or value greater than domain().length-1
         */
        scale.invertToIndex = function(y) {
            return Math.round(index.invert(y));
        };

        /**
         * As the underlying structure relies on a full array, ensure the full domain is passed here,
         * not just min and max values.
         *
         * @param _ The full domain array
         * @returns {*}
         */
        scale.domain = function(_) {
            if (!arguments.length) {
                var visible = index.domain();

                if(visible[0] < 0 && visible[visible.length-1] < 0) return []; // if it's all negative return empty, nothing is visible

                visible = [
                    Math.max(Math.ceil(visible[0]), 0), // If min is fraction, it is partially out of view, but still partially visible, round up (ceil)
                    Math.min(Math.floor(visible[visible.length-1]), domain.length-1) // If max is fraction, is partially out of view, but still partially visible, round down (floor)
                ];
                return domain.slice(visible[0], visible[visible.length-1]+1); // Grab visible domain, inclusive
            }

            domain = _;
            return applyDomain();
        };

        function zoomed() {
            band = rangeBand(index, domain, padding);
            return scale;
        }

        function domainMap() {
            dateIndexMap = lookupIndex(domain);
        }

        function applyDomain() {
            domainMap();
            index.domain([0, domain.length-1]);
            zoomed();
            // Apply outerPadding and widen the outer edges by pulling the domain in to ensure start and end bands are fully visible
            index.domain(index.range().map(scale_widen(outerPadding, band)).map(index.invert));
            zoomLimit.domain = index.domain(); // Capture the zoom limit after the domain has been applied
            return zoomed();
        }

        scale.copy = function() {
            return financetime(tickMethods, genericFormat, index.copy(), domain, padding, outerPadding, zoomLimit, closestTicks);
        };

        /**
         * Equivalent to d3's ordinal.rangeBand(). It could not be named rangeBand as d3 uses the method
         * to determine how axis ticks should be rendered. This scale is a hybrid ordinal and linear scale,
         * such that scale(x) returns y at center of the band as does d3.scale.linear()(x) does, whereas
         * d3.scale.ordinal()(x) returns y at the beginning of the band. When rendering svg axis, d3
         * compensates for this checking if rangeBand is defined and compensates as such.
         * @returns {number}
         */
        scale.band = function() {
            return band;
        };

        scale.outerPadding = function(_) {
            if(!arguments.length) return outerPadding;
            outerPadding = _;
            return applyDomain();
        };

        scale.padding = function(_) {
            if(!arguments.length) return padding;
            padding = _;
            return applyDomain();
        };

        scale.zoomable = function() {
            return zoomable;
        };

        /*
         * Ticks based heavily on d3 implementation. Attempted to implement this using composition with d3.time.scale,
         * but in the end there were sufficient differences to 'roll my own'.
         * - Different base tick steps: millis not required (yet!)
         * - State based tick formatting given the non continuous, even steps of ticks
         * - Supporting daily and intraday continuous (no gaps) plotting
         * https://github.com/mbostock/d3/blob/e03b6454294e1c0bbe3125f787df56c468658d4e/src/time/scale.js#L67
         */
        /**
         * Generates ticks as continuous as possible against the underlying domain. Where continuous time ticks
         * fall on where there is no matching domain (such as weekend or holiday day), it will be replaced with
         * the nearest domain datum ahead of the tick to keep close to continuous.
         * @param interval
         * @param steps
         * @returns {*}
         */
        scale.ticks = function(interval, steps) {
            var visibleDomain = scale.domain(),
                indexDomain = index.domain();

            if(!visibleDomain.length) return []; // Nothing is visible, no ticks to show

            var method = interval === undefined ? tickMethod(visibleDomain, indexDomain, 10) :
                typeof interval === 'number' ? tickMethod(visibleDomain, indexDomain, interval) : null;

            tickState.tickFormat = method ? method[2] : tickMethod(visibleDomain, indexDomain, 10)[2];

            if(method) {
                interval = method[0];
                steps = method[1];
            }

            var intervalRange = interval.every(steps).range(visibleDomain[0], +visibleDomain[visibleDomain.length-1]+1);

            return intervalRange                                // Interval, possibly contains values not in domain
                .map(domainTicks(visibleDomain, closestTicks))    // Line up interval ticks with domain, possibly adding duplicates
                .reduce(sequentialDuplicates, []);                // Filter out duplicates, produce new 'reduced' array
        };

        function tickMethod(visibleDomain, indexDomain, count) {
            if(visibleDomain.length == 1) return genericFormat; // If we only have 1 to display, show the generic tick method

            var visibleDomainExtent = visibleDomain[visibleDomain.length-1] - visibleDomain[0],
                intraday = visibleDomainExtent/dailyStep < 1, // Determine whether we're showing daily or intraday data
                methods = intraday ? tickMethods.intraday : tickMethods.daily,
                tickSteps = intraday ? intradayTickSteps : dailyTickSteps,
                k = Math.min(Math.round(countK(visibleDomain, indexDomain)*count), count),
                target = visibleDomainExtent/k, // Adjust the target based on proportion of domain that is visible
                i = d3_bisect(tickSteps, target);

            return i == methods.length ? methods[i-1] : // Return the largest tick method
                i ? methods[target/tickSteps[i-1] < tickSteps[i]/target ? i-1 : i] : methods[i]; // Else return close approximation or first tickMethod
        }

        /**
         * By default `ticks()` will generate tick values greater than the nearest domain interval value, which may not be
         * best value, particularly for irregular intraday domains. Setting this to true will cause tick generation to choose
         * values closest to the corresponding domain value for the calculated interval.
         * @param _ Optional `boolean` value. If argument is passed, sets the value and returns this instance, if no argument, returns the current value
         */
        scale.closestTicks = function(_) {
            if(!arguments.length) return closestTicks;
            closestTicks = _;
            return scale;
        };

        /**
         * NOTE: The type of tick format returned is dependant on ticks that were generated. To obtain the correct
         * format for ticks, ensure ticks function is called first, otherwise a default tickFormat will be returned
         * which may not be the optimal representation of the current domain state.
         * @returns {Function}
         */
        scale.tickFormat = function() {
            return function(date) {
                return tickState.tickFormat(date);
            };
        };

        techan_util_rebindCallback(scale, index, zoomed, 'range');

        domainMap();
        return zoomed();
    }

    function rangeBand(linear, domain, padding) {
        return (Math.abs(linear(domain.length-1) - linear(0))/Math.max(1, domain.length-1))*(1-padding);
    }

    /**
     * Calculates the proportion of domain that is visible. Used to reduce the overall count by this factor
     * @param visibleDomain
     * @param indexDomain
     * @returns {number}
     */
    function countK(visibleDomain, indexDomain) {
        return visibleDomain.length/(indexDomain[indexDomain.length-1]-indexDomain[0]);
    }

    function lookupIndex(array) {
        var lookup = {};
        array.forEach(function(d, i) { lookup[+d] = i; });
        return lookup;
    }

    function domainTicks(visibleDomain, closest) {
        var visibleDomainLookup = lookupIndex(visibleDomain); // Quickly lookup index of the domain

        return function(d) {
            var value = visibleDomainLookup[+d];
            if (value !== undefined) return visibleDomain[value];
            var index = d3_bisect(visibleDomain, d);
            if (closest && index > 0) {
                // d3_bisect gets the index of the closest value that is the greater than d,
                // which may not be the value that is closest to d.
                // If the closest value that is smaller than d is closer, choose that instead.
                if ((+d - (+visibleDomain[index-1])) < (+visibleDomain[index] - +d)) {
                    index--;
                }
            }
            return visibleDomain[index];
        };
    }

    function sequentialDuplicates(previous, current) {
        if(previous.length === 0 || previous[previous.length-1] !== current) previous.push(current);
        return previous;
    }

    var dailyStep = 864e5,
        dailyTickSteps = [
            dailyStep,  // 1-day
            6048e5,     // 1-week
            2592e6,     // 1-month
            7776e6,     // 3-month
            31536e6     // 1-year
        ],
        intradayTickSteps = [
            1e3,    // 1-second
            5e3,    // 5-second
            15e3,   // 15-second
            3e4,    // 30-second
            6e4,    // 1-minute
            3e5,    // 5-minute
            9e5,    // 15-minute
            18e5,   // 30-minute
            36e5,   // 1-hour
            108e5,  // 3-hour
            216e5,  // 6-hour
            432e5,  // 12-hour
            864e5   // 1-day
        ];

    var dayFormat = d3_time.timeFormat('%b %e'),
        yearFormat = d3_v3_multi_shim([
            [d3_time.timeFormat('%b %Y'), function(d) { return d.getMonth(); }],
            [d3_time.timeFormat('%Y'), function() { return true; }]
        ]),
        intradayFormat = d3_v3_multi_shim([
            [d3_time.timeFormat(':%S'), function(d) {
                return d.getSeconds();
            }],
            [d3_time.timeFormat('%I:%M'), function(d) { return d.getMinutes(); }],
            [d3_time.timeFormat('%I %p'), function () { return true; }]
        ]),
        genericFormat = [d3_time.timeSecond, 1, d3_v3_multi_shim([
            [d3_time.timeFormat(':%S'), function(d) { return d.getSeconds(); }],
            [d3_time.timeFormat('%I:%M'), function(d) { return d.getMinutes(); }],
            [d3_time.timeFormat('%I %p'), function(d) { return d.getHours(); }],
            [d3_time.timeFormat('%b %e'), function() { return true; }]
        ])
        ];

    var dayFormatUtc = d3_time.utcFormat('%b %e'),
        yearFormatUtc = d3_v3_multi_shim([
            [d3_time.utcFormat('%b %Y'), function(d) { return d.getUTCMonth(); }],
            [d3_time.utcFormat('%Y'), function() { return true; }]
        ]),
        intradayFormatUtc = d3_v3_multi_shim([
            [d3_time.utcFormat(':%S'), function(d) { return d.getUTCSeconds(); }],
            [d3_time.utcFormat('%I:%M'), function(d) { return d.getUTCMinutes(); }],
            [d3_time.utcFormat('%I %p'), function () { return true; }]
        ]),
        genericFormatUtc = [d3_time.timeSecond, 1, d3_v3_multi_shim([
            [d3_time.utcFormat(':%S'), function(d) { return d.getUTCSeconds(); }],
            [d3_time.utcFormat('%I:%M'), function(d) { return d.getUTCMinutes(); }],
            [d3_time.utcFormat('%I %p'), function(d) { return d.getUTCHours(); }],
            [d3_time.utcFormat('%b %e'), function() { return true; }]
        ])
        ];

    var dailyTickMethod = [
            [d3_time.timeDay, 1, dayFormat],
            [d3_time.timeMonday, 1, dayFormat],
            [d3_time.timeMonth, 1, yearFormat],
            [d3_time.timeMonth, 3, yearFormat],
            [d3_time.timeYear, 1, yearFormat]
        ],
        intradayTickMethod = [
            [d3_time.timeSecond, 1, intradayFormat],
            [d3_time.timeSecond, 5, intradayFormat],
            [d3_time.timeSecond, 15, intradayFormat],
            [d3_time.timeSecond, 30, intradayFormat],
            [d3_time.timeMinute, 1, intradayFormat],
            [d3_time.timeMinute, 5, intradayFormat],
            [d3_time.timeMinute, 15, intradayFormat],
            [d3_time.timeMinute, 30, intradayFormat],
            [d3_time.timeHour, 1, intradayFormat],
            [d3_time.timeHour, 3, intradayFormat],
            [d3_time.timeHour, 6, intradayFormat],
            [d3_time.timeHour, 12, intradayFormat],
            [d3_time.timeDay, 1, dayFormat]
        ];

    var dailyTickMethodUtc = [
            [d3_time.utcDay, 1, dayFormatUtc],
            [d3_time.utcMonday, 1, dayFormatUtc],
            [d3_time.utcMonth, 1, yearFormatUtc],
            [d3_time.utcMonth, 3, yearFormatUtc],
            [d3_time.utcYear, 1, yearFormatUtc]
        ],
        intradayTickMethodUtc = [
            [d3_time.utcSecond, 1, intradayFormatUtc],
            [d3_time.utcSecond, 5, intradayFormatUtc],
            [d3_time.utcSecond, 15, intradayFormatUtc],
            [d3_time.utcSecond, 30, intradayFormatUtc],
            [d3_time.utcMinute, 1, intradayFormatUtc],
            [d3_time.utcMinute, 5, intradayFormatUtc],
            [d3_time.utcMinute, 15, intradayFormatUtc],
            [d3_time.utcMinute, 30, intradayFormatUtc],
            [d3_time.utcHour, 1, intradayFormatUtc],
            [d3_time.utcHour, 3, intradayFormatUtc],
            [d3_time.utcHour, 6, intradayFormatUtc],
            [d3_time.utcHour, 12, intradayFormatUtc],
            [d3_time.utcDay, 1, dayFormatUtc]
        ];

    function techan_scale_financetime() {
        return financetime({ daily: dailyTickMethod, intraday: intradayTickMethod }, genericFormat);
    }

    techan_scale_financetime.utc = function() {
        return financetime({ daily: dailyTickMethodUtc, intraday: intradayTickMethodUtc }, genericFormatUtc);
    };

    return techan_scale_financetime;
};

function d3_v3_multi_shim(multi) {
    return function(d) {
        for(var i = 0; i < multi.length; i++) {
            if(multi[i][1](d)) return multi[i][0](d);
        }
    };
}

/**
 * TODO Refactor this to techan.plot.annotation.axis()?
 */
export const axisannotation = function(d3_svg_axis, d3_scale_linear, accessor_value, plot, plotMixin) {  // Injected dependencies
    return function() { // Closure function
        var p = {},
            axis = d3_svg_axis(d3_scale_linear()),
            format,
            point = 4,
            height = 14,
            width = 50,
            translate = [0, 0],
            orient = 'bottom';

        function annotation(g) {
            var group = p.dataSelector.mapper(filterInvalidValues(p.accessor, axis.scale()))(g);

            group.entry.append('path');
            group.entry.append('text');

            annotation.refresh(g);
        }

        annotation.refresh = function(g) {
            var fmt = format ? format : (axis.tickFormat() ? axis.tickFormat() : axis.scale().tickFormat());
            refresh(p.dataSelector.select(g), p.accessor, axis, orient, fmt, height, width, point, translate);
        };

        annotation.axis = function(_) {
            if(!arguments.length) return axis;
            axis = _;
            return annotation;
        };

        annotation.orient = function(_) {
            if(!arguments.length) return orient;
            orient = _;
            return annotation;
        };

        annotation.format = function(_) {
            if(!arguments.length) return format;
            format = _;
            return annotation;
        };

        annotation.height = function(_) {
            if(!arguments.length) return height;
            height = _;
            return annotation;
        };

        annotation.width = function(_) {
            if(!arguments.length) return width;
            width = _;
            return annotation;
        };

        annotation.translate = function(_) {
            if(!arguments.length) return translate;
            translate = _;
            return annotation;
        };

        plotMixin(annotation, p).accessor(accessor_value()).dataSelector();

        return annotation;
    };
};

function refresh(selection, accessor, axis, orient, format, height, width, point, translate) {
    var neg = orient === 'left' || orient === 'top' ? -1 : 1;

    selection.attr('transform', 'translate(' + translate[0] + ',' + translate[1] + ')');
    selection.select('path').attr('d', backgroundPath(accessor, axis, orient, height, width, point, neg));
    selection.select('text').text(textValue(accessor, format)).call(textAttributes, accessor, axis, orient, neg);
}

function filterInvalidValues(accessor, scale) {
    return function(data) {
        var range = scale.range(),
            start = range[0],
            end = range[range.length - 1];

        range = start < end ? [start, end] : [end, start];

        return data.filter(function (d) {
            if (accessor(d) === null || accessor(d) === undefined) return false;
            var value = scale(accessor(d));
            return value !== null && !isNaN(value) && range[0] <= value && value <= range[1];
        });
    };
}

function textAttributes(text, accessor, axis, orient, neg) {
    var scale = axis.scale();

    switch(orient) {
        case 'left':
        case 'right':
            text.attr('x', neg*(Math.max(axis.tickSizeInner(), 0) + axis.tickPadding()))
                .attr('y', textPosition(accessor, scale))
                .attr('dy', '.32em')
                .style('text-anchor', neg < 0 ? 'end' : 'start');
            break;
        case 'top':
        case 'bottom':
        default:
            text.attr('x', textPosition(accessor, scale))
                .attr('y', neg*(Math.max(axis.tickSizeInner(), 0) + axis.tickPadding()))
                .attr('dy', neg < 0 ? '0em' : '.72em')
                .style('text-anchor', 'middle');
            break;
    }
}

function textPosition(accessor, scale) {
    return function(d) {
        return scale(accessor(d));
    };
}

function textValue(accessor, format) {
    return function(d) {
        return format(accessor(d));
    };
}

function backgroundPath(accessor, axis, orient, height, width, point, neg) {
    return function(d) {
        var scale = axis.scale(),
            value = scale(accessor(d)),
            pt = point;

        switch(orient) {
            case 'left':
            case 'right':
                var h = 0;

                if(height/2 < point) pt = height/2;
                else h = height/2-point;

                return 'M 0 ' + value + ' l ' + (neg*Math.max(axis.tickSizeInner(), 1)) + ' ' + (-pt) +
                    ' l 0 ' + (-h) + ' l ' + (neg*width) + ' 0 l 0 ' + height +
                    ' l ' + (neg*-width) + ' 0 l 0 ' + (-h);
            case 'top':
            case 'bottom':
                var w = 0;

                if(width/2 < point) pt = width/2;
                else w = width/2-point;

                return 'M ' + value + ' 0 l ' + (-pt) + ' ' + (neg*Math.max(axis.tickSizeInner(), 1)) +
                    ' l ' + (-w) + ' 0 l 0 ' + (neg*height) + ' l ' + width + ' 0 l 0 ' + (neg*-height) +
                    ' l ' + (-w) + ' 0';
            default: throw "Unsupported orient value: axisannotation.orient(" + orient + "). Set to one of: 'top', 'bottom', 'left', 'right'";
        }
    };
}

export const crosshair = function(d3_select, d3_event, d3_mouse, d3_dispatch, accessor_crosshair, plot, plotMixin) { // Injected dependencies
    return function() { // Closure function
        var p = {},  // Container for private, direct access mixed in variables
            dispatcher = d3_dispatch('enter', 'out', 'move'),
            verticalPathGenerator,
            horizontalPathGenerator,
            xAnnotationComposer = plot.plotComposer().scope('composed-annotation').plotScale(function(plot) { return plot.axis().scale(); }),
            yAnnotationComposer = plot.plotComposer().scope('composed-annotation').plotScale(function(plot) { return plot.axis().scale(); }),
            verticalWireRange,
            horizontalWireRange;

        function crosshair(g) {
            var group = p.dataSelector(g);

            group.entry.append('path').attr('class', 'horizontal wire');
            group.entry.append('path').attr('class', 'vertical wire');

            group.entry.append('g').attr('class', 'axisannotation x').call(xAnnotationComposer);
            group.entry.append('g').attr('class', 'axisannotation y').call(yAnnotationComposer);

            g.selectAll('rect').data([undefined]).enter().append('rect').style('fill', 'none').style('pointer-events', 'all');

            crosshair.refresh(g);
        }

        crosshair.refresh = function(g) {
            var xRange = p.xScale.range(),
                yRange = p.yScale.range(),
                group = p.dataSelector.select(g),
                pathVerticalSelection = group.select('path.vertical'),
                pathHorizontalSelection = group.select('path.horizontal'),
                xAnnotationSelection = group.select('g.axisannotation.x'),
                yAnnotationSelection = group.select('g.axisannotation.y');

            verticalPathGenerator = verticalPathLine();
            horizontalPathGenerator = horizontalPathLine();

            g.selectAll('rect')
                .attr('x', Math.min.apply(null, xRange))
                .attr('y', Math.min.apply(null, yRange))
                .attr('height', Math.abs(yRange[yRange.length-1] - yRange[0]))
                .attr('width', Math.abs(xRange[xRange.length-1] - xRange[0]))
                .on('mouseenter', function() {
                    dispatcher.call('enter', this);
                })
                .on('mouseout', function() {
                    dispatcher.call('out', this);
                    // Redraw with null values to ensure when we enter again, there is nothing cached when redisplayed
                    delete group.node().__coord__;
                    initialiseWire(group.datum()); // Mutating data, don't need to manually pass down
                    refresh(group, pathVerticalSelection, pathHorizontalSelection, xAnnotationSelection, yAnnotationSelection);
                })
                .on('mousemove', mousemoveRefresh(group, pathVerticalSelection, pathHorizontalSelection,
                    xAnnotationSelection, yAnnotationSelection)
                );

            refresh(group, pathVerticalSelection, pathHorizontalSelection, xAnnotationSelection, yAnnotationSelection);
        };

        function mousemoveRefresh(selection, pathVerticalSelection, pathHorizontalSelection,
                                  xAnnotationSelection, yAnnotationSelection) {
            return function() {
                // Cache coordinates past this mouse move
                selection.node().__coord__ = d3_mouse(this);
                refresh(selection, pathVerticalSelection, pathHorizontalSelection, xAnnotationSelection, yAnnotationSelection);
            };
        }

        function refresh(selection, xPath, yPath, xAnnotationSelection, yAnnotationSelection) {
            var coords = selection.node().__coord__;

            if(coords !== undefined) {
                var d = selection.datum(),
                    xNew = p.xScale.invert(coords[0]),
                    yNew = p.yScale.invert(coords[1]),
                    dispatch = xNew !== null && yNew !== null && (p.accessor.xv(d) !== xNew || p.accessor.yv(d) !== yNew);

                p.accessor.xv(d, xNew);
                p.accessor.yv(d, yNew);
                if(dispatch) dispatcher.call('move', selection.node(), d);
            }

            // Just before draw, convert the coords to
            xPath.attr('d', verticalPathGenerator);
            yPath.attr('d', horizontalPathGenerator);
            xAnnotationSelection.call(xAnnotationComposer.refresh);
            yAnnotationSelection.call(yAnnotationComposer.refresh);
            selection.attr('display', displayAttr);
        }

        crosshair.xAnnotation = function(_) {
            if(!arguments.length) return xAnnotationComposer.plots();
            xAnnotationComposer.plots(_ instanceof Array ? _ : [_]);
            return binder();
        };

        crosshair.yAnnotation = function(_) {
            if(!arguments.length) return yAnnotationComposer.plots();
            yAnnotationComposer.plots(_ instanceof Array ? _ : [_]);
            return binder();
        };

        crosshair.verticalWireRange = function(_) {
            if(!arguments.length) return verticalWireRange;
            verticalWireRange = _;
            return binder();
        };

        crosshair.horizontalWireRange = function(_) {
            if(!arguments.length) return horizontalWireRange;
            horizontalWireRange = _;
            return binder();
        };

        function binder() {
            xAnnotationComposer.accessor(p.accessor.xv).scale(p.xScale);
            yAnnotationComposer.accessor(p.accessor.yv).scale(p.yScale);
            return crosshair;
        }

        function horizontalPathLine() {
            var range = horizontalWireRange || p.xScale.range();

            return function(d) {
                if(p.accessor.yv(d) === null) return null;
                var value = p.yScale(p.accessor.yv(d));
                if(isNaN(value)) return null;
                return 'M ' + range[0] + ' ' + value + ' L ' + range[range.length-1] + ' ' + value;
            };
        }

        function verticalPathLine() {
            var range = verticalWireRange || p.yScale.range();

            return function(d) {
                if(p.accessor.xv(d) === null) return null;
                var value = p.xScale(p.accessor.xv(d)),
                    sr = p.xScale.range();
                if(value < Math.min(sr[0], sr[sr.length-1]) || value > Math.max(sr[0], sr[sr.length-1])) return null;
                return 'M ' + value + ' ' + range[0] + ' L ' + value + ' ' + range[range.length-1];
            };
        }

        function initialiseWire(d) {
            d = d || {};
            p.accessor.xv(d, null);
            p.accessor.yv(d, null);
            return d;
        }

        function isEmpty(d) {
            return d === undefined || p.accessor.xv(d) === null || p.accessor.yv(d) === null;
        }

        function displayAttr(d) {
            return isEmpty(d) ? 'none' : null;
        }

        // Mixin scale management and event listening
        plotMixin(crosshair, p).plot(accessor_crosshair(), binder)
            .dataSelector(function(d) {
                // Has the user set data? If not, put empty data ready for mouse over
                if(isEmpty(d)) return [ initialiseWire() ];
                else return [d];
            })
            .on(dispatcher);

        p.dataSelector.scope('crosshair');

        return binder();
    };
};