import {timeParse, dispatch as d3Dispatch, json as d3Json } from 'd3';
import {oscillatorChart} from './oscillator.js';
import {lineChart} from './line.js';

// TODO: Resize
// TODO: Click to zoom
export function metcalfeCharts(symbol) {
  const BASEURL = window.rootmontAPI.root + 'rootmont/v1/metcalfe-chart/';
  const url = BASEURL + symbol + '/';

  const initialDaysToDisplay = 90;
  const downSampleThreshold = 400;

  // Charts' options
  const metcalfeValueChartOptions = {
    chartName: "metcalfeValueChart",
    chartType: "line",
    chartId: "raw-metcalfe-chart",
    containerHeight: 400,
    keys: [{ key: "raw_metcalfe", name: "Raw Metcalfe" }]
  };

  const metcalfeMovementsChartOptions = {
    chartName: "metcalfeMovementsChart",
    chartType: "line",
    chartId: "metcalfe-movement-chart",
    containerHeight: 400,
    keys: [{ key: "metcalfe_movement", name: "Metcalfe Movements" }]
  };

  const metcalfePriceRatioChartOptions = {
    chartName: "metcalfePriceRatioChart",
    chartType: "line",
    chartId: "metcalfe-price-ratio-chart",
    containerHeight: 400,
    keys: [{ key: "metcalfe_price_ratio", name: "Metcalfe Price Ratio" }]
  };

  const metcalfeOscillatorChartOptions = {
    chartName: "metcalfeOscillatorChart",
    chartType: "oscillator",
    chartId: "metcalfe-oscillator-chart",
    containerHeight: 400,
    keys: [
      { key: "ntx_divergence", name: "Metcalfe Divergence" },
      { key: "price", name: "Price" },
      { key: "normalized_metcalfe", name: "Metcalfe Value" }
      //{ key: "ntx", name: "Daily Transactions" }
    ]
  };

  const chartOptions = [
    metcalfeValueChartOptions,
    metcalfeMovementsChartOptions,
    metcalfePriceRatioChartOptions,
    metcalfeOscillatorChartOptions
  ];

  // Global variables
  let data; // Parsed json data
  const charts = []; // Rendered charts

  let metcalfeValueChart,
    metcalfeMovementsChart,
    metcalfePriceRatioChart,
    metcalfeOscillatorChart;

  const parseTime = timeParse("%Y-%m-%dT%H:%M:%S.%LZ");

  // Sync charts with custom events
  const dispatch = d3Dispatch(
    "zoom",
    "showTooltip",
    "updateTooltip",
    "hideTooltip",
    "enableInteractions"
  );
  dispatch.on("zoom", params => {
    const startDate = params.startDate;
    const endDate = params.endDate;
    const chartId = params.chartId;
    charts.forEach(chart => {
      if (chart.chartId === chartId) return;
      chart.syncZoom(startDate, endDate);
    });
  });
  dispatch.on("showTooltip", () => {
    charts.forEach(chart => {
      if (chart.chartType !== "line") return;
      chart.showTooltip();
    });
  });
  dispatch.on("updateTooltip", function(d) {
    charts.forEach(chart => {
      function addInitOverlay() {
        const overlay = svg
          .append("g")
          .attr("class", "overlay")
          .style("pointer-events", "all")
          .style("cursor", "pointer")
          .on("click", function() {
            d3Dispatch("enableInteractions");
          });
        overlay
          .append("rect")
          .attr("width", "100%")
          .attr("height", "100%")
          .style("fill", "#fff")
          .style("fill-opacity", 0.6);
        overlay
          .append("text")
          .attr("x", "50%")
          .attr("y", "50%")
          .style("font-size", "1.5em")
          .attr("dy", "0.35em")
          .style("text-anchor", "middle")
          .text("Click to enable chart interactions");
      }

      chart.enableInteractions = function() {
        svg.select(".overlay").remove();
      };
      if (chart.chartType !== "line") return;
      chart.updateTooltip.call(this, d);
    });
  });
  dispatch.on("hideTooltip", () => {
    charts.forEach(chart => {
      if (chart.chartType !== "line") return;
      chart.hideTooltip();
    });
  });
  dispatch.on("enableInteractions", () => {
    charts.forEach(chart => {
      chart.enableInteractions();
    });
  });

  // Fetch data
  d3Json(url).then(function(json) {
    processData(json);
    renderCharts();
  });

  // Process data
  function processData(json) {
    json.forEach(d => {
      d.date = parseTime(d.date);
    });
    data = json;
  }

  // Render charts
  function renderCharts() {
    const d = data[0];
    const initialStartingDate =
      data.length < initialDaysToDisplay
        ? data[0].date
        : data[data.length - initialDaysToDisplay].date;

    chartOptions.forEach(chartOption => {
      // Pass shared options
      chartOption.data = data;
      chartOption.downSampleThreshold = downSampleThreshold;
      chartOption.initialStartingDate = initialStartingDate;
      chartOption.dispatch = dispatch;

      if (chartOption.chartType === "oscillator") {
        // If value is null, don't render this chart
        if (
          chartOption.keys
            .slice(0, 3)
            .map(key => key.key)
            .some(key => d[key] === null)
        )
          return;

        if (chartOption.chartName === "metcalfeOscillatorChart") {
          metcalfeOscillatorChart = oscillatorChart(
            metcalfeOscillatorChartOptions
          );
          charts.push(metcalfeOscillatorChart);
        }
      } else if (chartOption.chartType === "line") {

        // If value is null, don't render this chart
        if ( null === d[chartOption.keys[0].key] ) {
          console.log( 'd', d );
          return;
        }

        if (chartOption.chartName === "metcalfeValueChart") {
          metcalfeValueChart = lineChart(metcalfeValueChartOptions);
          charts.push(metcalfeValueChart);
        } else if (chartOption.chartName === "metcalfeMovementsChart") {
          metcalfeMovementsChart = lineChart(metcalfeMovementsChartOptions);
          charts.push(metcalfeMovementsChart);
        } else if (chartOption.chartName === "metcalfePriceRatioChart") {
          metcalfePriceRatioChart = lineChart(metcalfePriceRatioChartOptions);
          charts.push(metcalfePriceRatioChart);
        }
      }
    });
  }

  // Responsive
  // Throttle resize event
  // https://developer.mozilla.org/en-US/docs/Web/Events/resize
  window.addEventListener("resize", resizeThrottler, false);

  let resizeTimeout;
  function resizeThrottler() {
    if (!resizeTimeout) {
      resizeTimeout = setTimeout(function() {
        resizeTimeout = null;
        resize();
      }, 100);
    }
  }

  function resize() {
    charts.forEach(chart => {
      chart.resize();
    });
  }
}
