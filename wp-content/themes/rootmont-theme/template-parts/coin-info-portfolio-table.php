<?php
global $coin_info_data;
$risk = $coin_info_data['coin_info']->portfolio;

if ( ! $risk ) {
	return false;
}

$times = [ '1 week', '1 month', '3 months', '6 months', '1 year', '2 years' ];
$rows = [ 'age', 'industry', 'marketcap', 'global' ];
$skip_columns = [ 'percentile', 'cov', 'marketcap' ];

?>
<div class="container portfolio">
    <h2 class="coin-title rootmont-popup" data-popup="portfolio_theory">
        Portfolio Theory
        <i class="fa fa-info-circle"></i>
    </h2>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs" id="portfolioTabs" role="tablist">
		        <?php
		        foreach( $times as $key => $time ) :
			        $label = ucwords( $time );
			        $time_id = str_replace( ' ', '', $time );
			        ?>
                    <li class="nav-item <?php echo ( $key === 2 ) ? 'active' : ''; ?>">
                        <a data-toggle="tab" href="#<?php echo $time_id; ?>" role="tab" aria-controls="<?php echo $time_id; ?>" aria-selected="true">
					        <?php echo $label; ?>
                        </a>
                    </li>
		        <?php endforeach; ?>
            </ul>

            <div class="tab-content" id="portfolioTabsContents">
		        <?php
		        foreach( $times as $key => $time ) :
			        $label = ucwords( $time );
			        $time_id = str_replace( ' ', '', $time );
			        ?>
                    <div class="tab-pane fade <?php echo $key === 2 ? 'active in' : ''; ?>" id="<?php echo $time_id; ?>" role="tabpanel" aria-labelledby="<?php echo $time_id; ?>">
                        <table id="portfolio-table">
                            <thead>
                            <tr>
                                <th></th>
						        <?php foreach( $risk->{ $time }->{'industry'} as $title => $values ) : if ( in_array( $title, $skip_columns ) ) { continue; } ?>
                                    <th>
								        <div
                                            class="rootmont-popup"
                                            data-popup="<?php echo 'portfolio_' . $title; ?>"
                                        >
                                            <?php echo ucwords( $title ); ?>
                                            <i class="fa fa-info-circle"></i>
                                        </div>
                                    </th>
						        <?php endforeach; ?>
                            </tr>
                            </thead>
                            <tbody>
					        <?php foreach( $rows as $row ) : ?>
                                <tr>
                                    <td>
								        <?php
								        switch ( $row ) {
									        case 'global' : echo 'All Cryptocurrencies'; break;
									        case 'age': echo 'Age - ' . $coin_info_data['coin_info']->categories->{ 'age' }; break;
									        case 'industry': echo 'Industry - ' . $coin_info_data['coin_info']->categories->{ 'industry' }; break;
									        case 'marketcap': echo 'Market Cap - ' . $coin_info_data['coin_info']->categories->{ 'marketcap' }; break;
									        default: echo $row;
								        }
								        ?>
                                    </td>
							        <?php foreach( $risk->{ $time }->{$row} as $title => $values ) : if ( in_array( $title, $skip_columns ) ) { continue; } ?>
                                        <td><?php echo number_format( round( $values->score, 3 ), 3 ); ?></td>
							        <?php endforeach; ?>
                                </tr>
					        <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
		        <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
