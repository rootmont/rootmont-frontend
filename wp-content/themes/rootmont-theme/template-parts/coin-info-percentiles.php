<?php global $coin_info_data; ?>

<div class="container summary">
	<h2 class="coin-title">Performance Summary</h2>
	<h3 class="summary-title">Percentile Ranking (0-100)</h3>
	<div class="row percentiles">
		<div class="col-sm-4 percentiles--overall-wrapper">
			<div class="percentiles--single percentile-overall rootmont-popup"
                 data-popup="performance_overall"
			>
				<div class="percentiles--single-score">
					<?php echo (int) round( $coin_info_data['coin_info']->percentiles->overall * 100, 0 ); ?>
				</div>
				<div class="percentiles--single-label">
					Root Rank
				</div>
			</div>
		</div>
		<div class="col-sm-8">
			<?php foreach ( $coin_info_data['coin_info']->percentiles as $key => $value ) : if ( 'overall' === $key ) { continue; } ?>
				<div
					class="col-sm-6 percentiles--single percentile-<?php echo $key; ?> scrollTo"
					data-destination="#performance-<?php echo str_replace( ' ', '_', $key ); ?>"
				>
					<div class="percentiles--single-score">
						<?php echo (int) round( $value * 100, 0 ); ?>
					</div>
					<div class="percentiles--single-label">
						<?php echo $key; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
