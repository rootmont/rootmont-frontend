<?php
global $rootmontCoins, $rootmontDashboard;

$dashboard = $rootmontDashboard->dashboard;

$ntx_mover_data = $dashboard->get_ntx_movers_data();

?>

<div id="ntx-movers" class="movers-wrapper">

    <div class="row">
        <div class="col-sm-12">
            <h2>NTX Movers</h2>
        </div>
    </div>

    <!-- Nav tabs -->
    <ul class="dashboard-movers-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#ntx_winners" aria-controls="home" role="tab" data-toggle="tab">Gainers</a>
        </li><li role="presentation"><a href="#ntx_losers" aria-controls="profile" role="tab" data-toggle="tab">Losers</a>
        </li><li role="presentation"><a href="#ntx_actives" aria-controls="messages" role="tab" data-toggle="tab">Actives</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="ntx_winners">
            <table class="table movers-tables" id="ntx-winners-table">
                <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Change %
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach( $ntx_mover_data['winners'] as $coin => $percent ): ?>
                    <tr>
                        <td>
							<?php echo $coin; ?>
                        </td>
                        <td>
							<?php echo rootmont_number( $percent ) . '%'; ?>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="ntx_losers">
            <table class="table movers-tables" id="ntx-losers-table">
                <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Change %
                    </th>
                </tr>
                </thead>
                <tbody>
				<?php foreach( $ntx_mover_data['losers'] as $coin => $percent ): ?>
                    <tr>
                        <td>
							<?php echo $coin; ?>
                        </td>
                        <td>
							<?php echo rootmont_number( $percent ) . '%'; ?>
                        </td>
                    </tr>
				<?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="ntx_actives">
            <table class="table movers-tables" id="ntx-active-table">
                <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Change %
                    </th>
                </tr>
                </thead>
                <tbody>
		        <?php foreach( $ntx_mover_data['active'] as $coin => $percent ): ?>
                    <tr>
                        <td>
					        <?php echo $coin; ?>
                        </td>
                        <td>
					        <?php echo rootmont_number( $percent ) . '%'; ?>
                        </td>
                    </tr>
		        <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>