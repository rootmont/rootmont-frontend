<?php global $coin_info_data; ?>

<h2 class="coin-title rootmont-popup" data-popup="quantitative_performance">
    Quantitative Performance
    <i class="fa fa-info-circle"></i>
</h2>

<?php
$bubbles = [ 'social', 'usage', 'development', 'price performance' ];
foreach( $bubbles as $bubble ) :
	?>

	<?php if ( ! empty( $coin_info_data['coin_info']->$bubble ) ) : ?>
	<div class="container-fluid bubble-info <?php echo $bubble; ?>" id="performance-<?php echo str_replace( ' ', '_', $bubble ); ?>">
		<div class="container">
			<div class="row">
				<div
                    class="col-sm-5"
                >
					<div class="bubble-single main-title">
						<span class="bubble"><?php echo round( $coin_info_data['coin_info']->percentiles->$bubble * 100, 0 ); ?></span>
						<span class="bubble-title-score">
                            <span class="bubble-title rootmont-popup" data-popup="<?php echo str_replace( ' ', '_', $bubble ); ?>"><?php echo $bubble; ?> <i class="fa fa-info-circle"></i></span>
                        </span>
					</div>
				</div>
				<div class="col-sm-7">
					<?php foreach( $coin_info_data['coin_info']->$bubble as $key => $value ) : if ( 'percentile' === $key ) { continue; } ?>
						<div class="col-sm-4">
							<div class="bubble-single">
								<span class="bubble"><?php echo round( $value->percentile * 100, 0 ); ?></span>
								<span class="bubble-title-score">
                                    <span class="bubble-title">
                                        <?php
                                        $score_title = str_replace( [ '_', '/' ], [ ' ', ' / ' ], $key );
                                        echo $score_title;
                                        ?>
                                    </span>
                                    <span class="bubble-score">
                                        <?php
                                            if( 'trading_volume' === $key || 'marketcap' === $key ) {
                                                echo '$';
                                            }
                                        ?>
                                        <?php
                                            if ( $value->score < 1 ) {
                                                echo rootmont_number( $value->score, 3 );
                                            } else {
                                                echo rootmont_number( $value->score, 2 );
                                            }
                                        ?>
                                    </span>
                                </span>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
<?php endforeach; ?>
