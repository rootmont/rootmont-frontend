<?php
global $coin_info_data;
?>
<?php if ( 0 !== $coin_info_data['coin_info']->disclosure ) : ?>
	<div class="bar gray disclosure">
		<div class="container">
			<?php the_field( 'coin_page_disclosure', 'options' ); ?>
		</div>
	</div>
<?php endif; ?>