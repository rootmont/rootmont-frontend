<?php global $coin_info_data; ?>
<div class="container-fluid historical">
    <style>

        body {
            font: 10px sans-serif;
        }

        text {
            fill: #000;
        }

        button {
            position: absolute;
            right: 20px;
            top: 20px;
            display: none;
        }

        path.volume {
            fill: #AAAAAA;
            opacity: 0.5;
        }

        path.volume.up {
            fill: #00AA00;
        }

        path.volume.down {
            fill: #FF0000;
        }

        path.line {
            fill: none;
            stroke: #000000;
            stroke-width: 1;
        }

    </style>
    <div class="line-charts-wrapper text-center stacked-row" data-symbol="<?php echo $coin_info_data['symbol']; ?>">
        <!-- / LINE GRAPHS -->
        <div class="row stacked-row">
            <div class="col-sm-12 text-center">
                <h4 class="rootmont-popup" data-popup="macd_graph">
                    MACD <i class="fa fa-info-circle"></i>
                </h4>
                <!-- MACD -->
                <div class="macd-chart-wrapper">
                    <div id="macd-chart" class="chart-container oscillator-chart" data-symbol="<?php echo $coin_info_data['symbol']; ?>"></div>
                </div>
                <!-- / MACD -->
            </div>
        </div>
    </div>
    <script>
        rootmont.priceCharts("<?php echo $coin_info_data['symbol']; ?>");
    </script>
</div>
