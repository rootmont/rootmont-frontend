<?php global $coin_info_data; ?>
<header class="coin-header container text-center">
	<div class="row">
		<div class="col-sm-12">
			<h1 class="coin-logo">
				<img src="<?php echo esc_html( $coin_info_data['logo'] ); ?>" alt="coin rootmont">
				<?php echo ( true === get_field( 'show_title' ) ) ? get_the_title() : ''; ?>
				(<?php echo strtoupper( $coin_info_data['symbol'] ); ?>)
			</h1>
		</div>
		<?php if ( isset( $coin_info_data['coin_info']->categories ) && ! empty( $coin_info_data['coin_info']->categories ) ) { ?>
			<div class="col-sm-12">
				<div class="coin-categories">
					<ul>
						<?php
						foreach( $coin_info_data['coin_info']->categories as $key => $cat ) {
						    if ( 'industry' === $key ) {
						        $key = 'industries';
                            } else {
						        $key .= 's';
                            }
							echo '<li><a href="/filtered/' . $key . '/' . $cat . '">' . $key . ' - ' . $cat . '</a></li>';
						}
						?>
					</ul>
				</div>
			</div>
		<?php } ?>
	</div>
</header>