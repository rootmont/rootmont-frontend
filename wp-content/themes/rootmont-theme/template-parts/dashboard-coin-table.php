<?php

global $user_data, $rootmontCoins, $rootmontDashboard;

$dashboard = $rootmontDashboard->dashboard;

$cols = $user_data['_rootmont_dashboard_top_cols'];
$coins = $user_data['_rootmont_dashboard_top_coins'];

// Column Data
$all_cols = $dashboard->get_user_cols();

$coin_data = $dashboard->get_coin_table_data( $coins, $cols );


?>
<h2>Favorite Coins</h2>
<table id="dataTable" class="dataTable dashboard-coin">
    <thead>
    <tr>
        <th>
            Coin
        </th>
		<?php foreach ( $cols as $col ) : ?>
            <th>
				<?php
				$col = str_replace( '_', ' ', $col );
				$col = str_replace( '/', ' / ', $col );
				echo ucwords( $col );
				?>
            </th>
		<?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
	<?php foreach ( $coins as $coin ) : ?>
        <tr>
            <td>
				<?php echo $coin; ?>
            </td>
			<?php foreach ( $cols as $col ) : ?>
                <?php
                    if ( isset( $coin_data->{ $coin }->{ $col } ) ) {
                        str_replace( '/', ' / ', $coin_data->{ $coin }->{ $col } );
                    }
                ?>
                <td>
					<?php echo isset( $coin_data->{ $coin }->{ $col } ) ? rootmont_number( $coin_data->{ $coin }->{ $col } ) : 'Unavailable'; ?>
                </td>
			<?php endforeach; ?>
        </tr>
	<?php endforeach; ?>
    </tbody>
</table>

<?php get_template_part( 'template-parts/dashboard', 'coin-table-cols' ); ?>
<?php get_template_part( 'template-parts/dashboard', 'coin-table-coins' ); ?>
