<?php global $coin_info_data;

$white_paper_url = $coin_info_data['coin_info']->whitepaper_url ?: false;
$website_url = $coin_info_data['coin_info']->website_url ?: false;
?>
<div class="bar gray description">
	<div class="container">
		<h2 class="coin-title rootmont-popup" data-popup="coin_description">
			Description <i class="fa fa-info-circle"></i>
		</h2>
        <?php if ( $white_paper_url || $website_url ) : ?>
        <div class="coin-categories description">
            <ul>
				<?php
				echo $website_url ? '<li><a target="_blank" href="' . $website_url . '">Website</a></li>' : '';
				echo $white_paper_url ? '<li><a target="_blank" href="' . $white_paper_url . '">Whitepaper</a></li>' : '';
				?>
            </ul>
        </div>
        <?php endif; ?>
		<?php echo apply_filters( 'the_content', $coin_info_data['coin_info']->description->description ); ?>
	</div>
</div>
