<?php
global $rootmontCoins, $rootmontDashboard;

$dashboard = $rootmontDashboard->dashboard;

$price_mover_data = $dashboard->get_price_movers_data();

?>


<div id="price-movers" class="movers-wrapper">

    <div class="row">
        <div class="col-sm-12">
            <h2>Price Movers</h2>
        </div>
    </div>

    <!-- Nav tabs -->
    <ul class="dashboard-movers-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#price_winners" aria-controls="home" role="tab" data-toggle="tab">Gainers</a>
        </li><li role="presentation"><a href="#price_losers" aria-controls="profile" role="tab" data-toggle="tab">Losers</a>
        </li><li role="presentation"><a href="#price_actives" aria-controls="messages" role="tab" data-toggle="tab">Actives</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="price_winners">
            <table class="table movers-tables" id="price-winners-table">
                <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            Change %
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( $price_mover_data['winners'] as $coin => $percent ): ?>
                    <tr>
                        <td>
                            <?php echo $coin; ?>
                        </td>
                        <td>
                            <?php echo rootmont_number( $percent ) . '%'; ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="price_losers">
            <table class="table movers-tables" id="price-losers-table">
                <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Change %
                    </th>
                </tr>
                </thead>
                <tbody>
		        <?php foreach( $price_mover_data['losers'] as $coin => $percent ): ?>
                    <tr>
                        <td>
					        <?php echo $coin; ?>
                        </td>
                        <td>
					        <?php echo rootmont_number( $percent ) . '%'; ?>
                        </td>
                    </tr>
		        <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div role="tabpanel" class="tab-pane" id="price_actives">
            <table class="table movers-tables" id="price-active-table">
                <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Change %
                    </th>
                </tr>
                </thead>
                <tbody>
		        <?php foreach( $price_mover_data['active'] as $coin => $percent ): ?>
                    <tr>
                        <td>
					        <?php echo $coin; ?>
                        </td>
                        <td>
					        <?php echo rootmont_number( $percent ) . '%'; ?>
                        </td>
                    </tr>
		        <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

