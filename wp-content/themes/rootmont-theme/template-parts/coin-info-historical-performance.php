<?php global $coin_info_data; ?>
<div class="container-fluid historical">
   <?php if ( 0 !== (int) $coin_info_data['coin_info']->usage->{'daily_transactions'}->score ) : ?>
        <h2 class="coin-title">On-Chain Performance</h2>
        <div class="row stacked-row">
           <div class="col-sm-12 text-center">
               <h4 class="rootmont-popup" data-popup="metcalfe_oscillator_graph">
                   Metcalfe Oscillator <i class="fa fa-info-circle"></i>
               </h4>
               <!-- Metcalfe Oscillator -->
               <div class="met-osc">
                   <div id="metcalfe-oscillator-chart" class="chart-container oscillator-chart" data-symbol="<?php echo $coin_info_data['symbol']; ?>"></div>
               </div>
               <!-- / Metcalfe Oscillator -->
           </div>
        </div>
        <!-- LINE GRAPHS -->
        <div class="line-charts-wrapper text-center stacked-row" data-symbol="<?php echo $coin_info_data['symbol']; ?>">
            <div class="row">
                <div class="col-sm-4">
                    <h4 class="rootmont-popup" data-popup="metcalfe_ratio_graph">
                        Metcalfe Price Ratio <i class="fa fa-info-circle"></i>
                    </h4>
               <div id="metcalfe-price-ratio-chart" class="chart-container line-chart"></div>
                </div>
                <div class="col-sm-4">
                    <h4 class="rootmont-popup" data-popup="metcalfe_movements_graph">
                        Metcalfe Movements <i class="fa fa-info-circle"></i>
                    </h4>
               <div id="metcalfe-movement-chart" class="chart-container line-chart"></div>
                </div>
                <div class="col-sm-4" data-symbol="<?php echo $coin_info_data['symbol']; ?>">
                    <h4 class="rootmont-popup" data-popup="metcalfe_value_graph">
                        Metcalfe Value <i class="fa fa-info-circle"></i>
                    </h4>
               <div id="raw-metcalfe-chart" class="chart-container line-chart"></div>
                </div>
            </div>
        </div>
        <!-- / LINE GRAPHS -->

        <script>
            rootmont.metcalfeCharts("<?php echo $coin_info_data['symbol']; ?>");
        </script>
   <?php endif; ?>
</div>
