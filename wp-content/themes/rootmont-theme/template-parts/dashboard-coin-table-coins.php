<?php
global $user_data, $rootmontCoins, $rootmontDashboard;

$dashboard = $rootmontDashboard->dashboard;

$coins = $user_data['_rootmont_dashboard_top_coins'];
$all_coins = $dashboard->get_user_coins();

//function sort_coins_alpha( $a, $b ) {
//    if ( 0 < strcasecmp( $a->post_title, $b->post_title ) ) {
//        return 1;
//    } else {
//        return -1;
//    }
//    return 0;
//}
//
//uasort( $all_coins, 'sort_coins_alpha' );

?>


<!-- Add Coins Modal -->
<div class="modal fade" id="addCoinModal" tabindex="-1" role="dialog" aria-labelledby="addCoinModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
            <div class="modal-header text-center">
                <p>
                    To add or remove select items and click add or remove. <br/>
                    <em>
                        hint: use shift, command (or control) to select multiple items
                    </em>
                </p>
            </div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-5">
						<h4>Available Coins</h4>
						<select class="form-control" id="coins_from" name="_rootmont_dashboard_top_cols[]" multiple="multiple" size="20">
							<?php foreach( $all_coins as $key => $coin ) : ?>
								<?php if ( in_array( $coin, $coins ) ) { continue; } ?>
								<option value="<?php echo $coin; ?>">
									<?php echo $coin; ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-xs-2" style="padding-top: 125px;">
                        <span class="btn btn-success btn-block" id="coins_right">
                            <i class="fa fa-angle-right"></i> Add
                        </span>
						<span class="btn btn-success btn-block" id="coins_right_all">
                            <i class="fa fa-angle-double-right"></i> Add All
                        </span>
						<br/><br/>
						<span class="btn btn-danger btn-block" id="coins_left">
                            <i class="fa fa-angle-left"></i> Remove
                        </span>
						<span class="btn btn-danger btn-block" id="coins_left_all">
                            <i class="fa fa-angle-double-left"></i> Remove All
                        </span>
					</div>
					<div class="col-xs-5">
						<h4>Your Coins</h4>
						<select name="to[]" id="coins_to" class="form-control" size="20" multiple="multiple">
							<?php foreach( $coins as $key => $coin ) : ?>
								<option value="<?php echo $coin; ?>">
									<?php
									echo $coin;
									?>
								</option>
							<?php endforeach; ?>
						</select>
						<div class="row text-center up-down-wrappper">
							<div class="col-xs-4 col-xs-offset-2">
								<button type="button" id="cols_up" class="btn btn-primary btn-block"><i class="fa fa-arrow-up"></i></button>
							</div>
							<div class="col-xs-4">
								<button type="button" id="cols_down" class="btn btn-primary btn-block"><i class="fa fa-arrow-down"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-warning coins" id="reset-cols">Reset</button>
				<button type="button" class="btn btn-success coins" id="save-cols">Save changes</button>
			</div>
		</div>
	</div>
</div>
<!--/Add Coins Modal -->



<!-- Coins RESET -->

<div class="hidden" id="coins_reset">
	<?php foreach( $coins as $key => $coin ) : ?>
		<option value="<?php echo $coin; ?>">
			<?php
			echo $coin;
			?>
		</option>
	<?php endforeach; ?>
</div>

<div class="hidden" id="all_coins_reset">
	<?php foreach( $all_coins as $key => $coin ) : ?>
		<?php if ( in_array( $coin, $coins ) ) { continue; } ?>
		<option value="<?php echo $coin; ?>">
			<?php
			echo $coin;
			?>
		</option>
	<?php endforeach; ?>
</div>

<!-- /Coins RESET -->
