<?php
global $coin_info_data;
$upside_downside = $coin_info_data['coin_info']->{'up/down'};

if ( ! $upside_downside ) {
	return false;
}

$time = [ '1 week', '1 month', '3 months', '6 months', '1 year', '2 years' ];
$rows = [
   [
      'label' => 'Age - ' . $coin_info_data['coin_info']->categories->{ 'age' },
      'key' => 'age',
   ],
	[
		'label' => 'Industry - ' . $coin_info_data['coin_info']->categories->{ 'industry' },
		'key' => 'industry',
	],
   [
      'label' => 'Market Cap - ' . $coin_info_data['coin_info']->categories->{ 'marketcap' },
      'key' => 'marketcap',
   ],
	[
		'label' => 'All Cryptocurrencies',
		'key' => 'global',
	],
];

?>
<div class="bar gray upside">
    <h2 class="coin-title rootmont-popup" data-popup="upside_downside_ratio">
        Upside & Downside Capture Ratio
        <i class="fa fa-info-circle"></i>
    </h2>
    <div class="container">
        <table id="upside-downside-table">
            <thead>
            <tr>
                <th></th>
                <th>
                    1-Week
                </th>
                <th>
                    1-Month
                </th>
                <th>
                    3-Months
                </th>
                <th>
                    6-Months
                </th>
                <th>
                    1-Year
                </th>
                <th>
                    2-Years
                </th>
            </tr>
            </thead>
            <tbody>
		    <?php foreach( $rows as $key => $value ) : ?>
                <tr>
                    <td><?php echo $value['label']; ?></td>
				    <?php foreach( $time as $upside_key => $upside_value ) : ?>
                        <td>
                            <div class="upside">
                                <?php $upside_num = number_format( round( $upside_downside->{$upside_value}->{$value['key']}->upside->score, 4 ), 3 ); ?>
                                <span
                                    class="number-size-<?php echo 5 < strlen( $upside_num ) ? 'lg' : 'sm'; ?>"
                                ><?php echo $upside_num; ?></span>
                                <i class="fa fa-caret-up"></i>
                            </div>
                            <div class="downside">
                                <?php $downside_num = number_format( round( $upside_downside->{$upside_value}->{$value['key']}->downside->score, 4 ), 3 ); ?>
                                <span
                                    class="number-size-<?php echo 5 < strlen( $downside_num ) ? 'lg' : 'sm'; ?>"
                                ><?php echo $downside_num; ?></span>
                                <i class="fa fa-caret-down"></i>
                            </div>
                        </td>
				    <?php endforeach; ?>
                </tr>
		    <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
