<?php global $coin_info_data; ?>
<div class="bar blue market-cap row">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
                <div class="price-ct" id="coin-price"> <?php echo '$'. $coin_info_data['current_stats']->price; ?></div>
				<div class="price-nm">Price (USD)</div>
			</div>
			<div class="col-sm-3">
                <div class="price-ct" id="coin-mcap"> <?php echo '$' . $coin_info_data['current_stats']->marketcap; ?></div>
				<div class="price-nm">Market Cap (USD)</div>
			</div>
			<div class="col-sm-3">
                <div class="price-ct" id="coin-vol"> <?php echo '$' . $coin_info_data['current_stats']->volume; ?></div>
				<div class="price-nm">Volume (24h)</div>
			</div>
			<div class="col-sm-3">
				<div class="price-ct" id="coin-supply"><?php echo $coin_info_data['current_stats']->supply; ?></div>
				<div class="price-nm">CIRCULATING SUPPLY (<?php echo $coin_info_data['symbol']; ?>)</div>
			</div>
		</div>
	</div>
    <script>
        function update() {
            rootmont.updateCoinReportStats("<?php echo get_the_title(); ?>");
        }
        setInterval(update, 15000);
    </script>
</div>
