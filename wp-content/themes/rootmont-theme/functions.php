<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );
add_filter( 'wp_nav_menu_items', 'FLChildTheme::rootmont_login_item', 10, 2) ;
add_filter( 'rcp_login_url', 'FLChildTheme::rcp_login_url', 10, 2 );
add_action( 'wp_head', 'FLChildTheme::ga_script', 100 );


function rootmon_build_portfolio_table( $data ) {
	$portfolio_skip_keys = [ 'upside', 'downside', 'percentile' ];
	$table = '<table><thead><tr><th></th>';
		foreach( $data as $key => $value ) {
			if ( in_array( $key, $portfolio_skip_keys ) ) { continue; }
			$table .= '<th>' . $key . '</th>';
		}
	$table .= '</tr></thead>';
	$table .= '<tbody>';
		$table .= '<tr><td>Score</td>';
		foreach( $data as $key => $value ) {
			if ( in_array( $key, $portfolio_skip_keys ) ) { continue; }
			$table .= '<td>' . round( $value->score, 4 ) . '</td>';
		}
		$table .= '</tr>';
		$table .= '<tr><td>Percentile</td>';
		foreach( $data as $key => $value ) {
			if ( in_array( $key, $portfolio_skip_keys ) ) { continue; }
			$table .= '<td>' . round( $value->percentile * 100, 4 ) . '</td>';
		}
		$table .= '</tr>';
	$table .= '</tbody>';
	$table .='</table>';

	return $table;
}

function get_benchmark_data( $type ) {
	global $rootmontCoins;
	$request = new \WP_REST_Request( 'GET', '/rootmont/v1/coin-benchmark/' );
	$request->set_query_params( [
		'type' => $type,
	] );

	$response = rest_ensure_response( $rootmontCoins->api->get_benchmark_data( $request ) );

	if ( isset( $_GET, $_GET['debug'] ) ) {
		var_dump( $response );
	}

	$data = $response->data['response'];

	return $data;
}

function rootmont_setup_global_user() {
	global $is_active_user, $user_membership_status, $user_membership, $user_membership_id;
	$user = wp_get_current_user();
	$is_active_user = rcp_is_active( $user->ID );
	$user_membership_status = rcp_get_status( $user->ID );

	if ( $is_active_user ) {
		$user_membership = rcp_get_subscription( $user->ID );
		$user_membership_id = rcp_get_subscription_id( $user->ID );
	}
}

add_action( 'wp', 'rootmont_setup_global_user' );


function rootmont_coin_catalog_tags() {
	add_rewrite_tag('%coin_cat%', '([^&]+)');
	add_rewrite_tag('%coin_cat_term%', '([^&]+)');
}
add_action( 'init', 'rootmont_coin_catalog_tags', 10, 0 );

function rootmont_coin_catalog_rewrite() {
	$frontpage_id = get_option( 'page_on_front' );
	add_rewrite_rule('^coin/([^/]*)/([^/]*)/?','index.php?post_type=coins&coin_cat=$matches[1]&coin_cat_term=$matches[2]','top');
	add_rewrite_rule('^filtered/([^/]*)/([^/]*)/?','index.php?page_id=' . $frontpage_id . '&coin_cat=$matches[1]&coin_cat_term=$matches[2]','top');
}
add_action( 'init', 'rootmont_coin_catalog_rewrite', 11, 0 );


// Add T&C to bottom of Membership Page
function terms_conditions_membership_page() {
	echo '<a href="/terms-and-conditions/">Terms &amp; Conditions</a>';
}
add_action( 'rcp_subscription_details_bottom', 'terms_conditions_membership_page', 10, 0 );

add_action( 'template_redirect', function(){
	if ( is_front_page() ) {
		remove_action( 'template_redirect', 'redirect_canonical' );
	}
}, 0 );

// Number formatting
function rootmont_number( $number , $precision = 2) {

   // is this a number?
   if (!is_numeric($number)) return false;

   // now filter it;
   if ($number > 1000000000000) return round(($number/1000000000000), $precision).' T';
   elseif ($number > 1000000000) return round(($number/1000000000), $precision).' B';
   elseif ($number > 1000000) return round(($number/1000000), $precision).' M';
   elseif ($number > 1000) return round(($number/1000), $precision).' K';
   else return round($number,3);

   return number_format($number, $precision);

}

// redirect admin
function redirect_from_admin() {
	if ( ! current_user_can( 'manage_options' ) && ! defined( 'DOING_AJAX' ) && ( defined( 'DOING_AJAX' ) && ! DOING_AJAX )  ) {
		wp_safe_redirect( get_site_url() );
	}
}
add_action( 'admin_init', 'redirect_from_admin' );

// custom processor for requesting coin info
// adding log pretty printing thing
if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}

function get_coin_recommendation( $form_data ) {

    write_log("Received form: " . $form_data);
    $form_dataz = array(
        'email' => $form_data->fields['33']['value'],
        'industries' => array($form_data->fields['39']['value']),
        'marketcaps' => array($form_data->fields['40']['value']),
        'ages' => array($form_data->fields['41']['value']),
        'risk' => $form_data->fields['42']['value'],
        'social' => $form_data->fields['43']['value'],
        'dev' => $form_data->fields['44']['value'],
        'updown' => $form_data->fields['45']['value'],
        'usage' => $form_data->fields['46']['value'],
    );

    $api_location = get_api_location();

    $url = "$api_location/suggestions/";
    $request = new WP_Http;
    $result = $request->request( $url, array( 'method' => 'POST', 'body' => $form_dataz) );
    if ( !is_wp_error ($result) ) {
        write_log("response was bad somehow: " . print_r($result));
    };
}
// Saving the following action in case we go back to Caldera, otherwise Ninja Forms below
//add_action( 'get_coin_recommendation', 'get_coin_recommendation' );
add_action( 'ninja_forms_after_submission', 'get_coin_recommendation' );


function get_api_location() {
	$api_location = "";
    if (isset($_ENV["PANTHEON_ENVIRONMENT"]) == false) {
        $_ENV["PANTHEON_ENVIRONMENT"] = "dev";
    }
	if ($_ENV["PANTHEON_ENVIRONMENT"] == "dev") {
		$api_location = "http://dev.rootmont.io";
	} else if ($_ENV["PANTHEON_ENVIRONMENT"] == "test") {
    $api_location = "http://staging.rootmont.io";
  } else {
		$api_location = "https://rootmont.io";
	}

	return $api_location;
}
