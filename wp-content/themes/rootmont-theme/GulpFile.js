var gulp = require( 'gulp' ),
    sass = require( 'gulp-sass' ),
    sourcemaps = require( 'gulp-sourcemaps' ),
    watch = require( 'gulp-watch' ),
    terser = require( 'gulp-terser'),
    minifyCss = require('gulp-minify-css'),
    concat = require( 'gulp-concat' );


gulp.task( 'sass', function( done ) {
    gulp.src( './assets/scss/styles.scss' )
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
//        .pipe(minifyCss())
        .pipe(gulp.dest('./build/css'));
    done();
});


var jsFileList = [
    './node_modules/handlebars/dist/handlebars.js',
    './node_modules/jszip/dist/jszip.js',
    './node_modules/pdfmake/build/pdfmake.js',
    './node_modules/pdfmake/build/vfs_fonts.js',
    './node_modules/datatables.net/js/jquery.dataTables.js',
    './node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
    './node_modules/datatables.net-buttons/js/dataTables.buttons.js',
    './node_modules/datatables.net-buttons/js/buttons.flash.js',
    './node_modules/datatables.net-buttons/js/buttons.html5.js',
    './node_modules/datatables.net-buttons/js/buttons.colVis.js',
    './node_modules/datatables.net-buttons/js/buttons.print.js',
    './node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.js',
    './node_modules/datatables.net-fixedheader/js/dataTables.fixedHeader.js',
    './node_modules/datatables.net-fixedheader-bs/js/fixedHeader.bootstrap.js',
    './node_modules/datatables.net-keytable/js/dataTables.keyTable.js',
    './node_modules/datatables.net-responsive/js/dataTables.responsive.js',
    './node_modules/datatables.net-responsive-bs/js/responsive.bootstrap.js',
    './node_modules/d3/dist/d3.js',
    './node_modules/techan/dist/techan.min.js',
    // './node_modules/techan/build/techan-bundle.js',
    './assets/js/lib/multiselect.min.js',
    './assets/js/line/main.js',
    './assets/js/main.js',
    './assets/js/volume.js',
    './assets/js/utils.js',
    './assets/js/oscillator.js',
    './assets/js/line.js',
    './assets/js/line-volume.js',
    './assets/js/metcalfe-chart.js',
    './assets/js/price-chart.js',
    './assets/js/dashboard.js',
    './assets/js/prices.js'
];

gulp.task( 'js', function( done ) {
    gulp.src( jsFileList )
        .pipe(concat('rootmont-scripts.js'))
 //       .pipe(terser())
        .pipe(gulp.dest('./build/js/'));
    done();
});

gulp.task( 'watch', function( done ) {
    gulp.watch( './assets/scss/**/*.scss', gulp.parallel( 'sass' ) );
    gulp.watch( './assets/js/**/*.js', gulp.parallel( 'js' ) );
    done();
});

gulp.task( 'default', gulp.parallel( 'sass', 'js', 'watch' ) );
