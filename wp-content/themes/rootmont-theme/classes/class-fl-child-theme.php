<?php

/**
 * Helper class for child theme functions.
 *
 * @class FLChildTheme
 */
final class FLChildTheme {
    
    /**
	 * Enqueues scripts and styles.
	 *
     * @return void
     */
    static public function enqueue_scripts() {

        // Autocomplete
	    wp_enqueue_style( 'jquery-ui-autocomplete-css', '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css', null, 'all' );
//	    wp_enqueue_script( 'jquery-ui-core' );
//	    wp_enqueue_script( 'jquery-ui-autocomplete' );


	    wp_enqueue_style( 'rootmont-child-theme', FL_CHILD_THEME_URL . '/style.css' );
	    wp_enqueue_style( 'rootmont-child-theme-custom', FL_CHILD_THEME_URL . '/build/css/styles.css', [], '5.0.4', 'all' );

//	    wp_enqueue_script( 'rootmont-d3', '//cdnjs.cloudflare.com/ajax/libs/d3/5.5.0/d3.min.js', array( 'jquery' ), null, false );
//        wp_enqueue_script( 'rootmont-main-scripts', FL_CHILD_THEME_URL . '/build/js/rootmont-scripts.js', array( 'rootmont-d3' ), null, false );
        wp_enqueue_script( 'rootmont-main-scripts', FL_CHILD_THEME_URL . '/build/js/main.min.js', array(), '5.0.4', false );

	    wp_localize_script( 'rootmont-main-scripts', 'rootmontAPI', [
		    'root' => esc_url_raw( rest_url() ),
		    'nonce' => wp_create_nonce( 'wp_rest' ),
            'admin_ajax_url' => admin_url( 'admin-ajax.php' )
        ]);

	    $popups = get_field( 'site_popups', 'option' );
	    if ( ! empty( $popups ) ) {
		    $popup_data = [];
		    foreach( $popups as $popup ) {
		        $popup_data[ $popup['name'] ] = $popup;
            }
		    wp_localize_script( 'rootmont-main-scripts', 'rootmontPopupData', $popup_data );
        }

	    if ( is_page_template( 'template-benchmarks.php' ) ) {
	        //wp_enqueue_style( 'DataTables-css', '//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css', array(), null, 'all' );
	        //wp_enqueue_script( 'DataTables-js', '//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js', array( 'jquery' ), null, true );
        }

    }

	static public function rootmont_login_item( $items, $args ) {
		if ( 'header' === $args->theme_location) {

			if ( is_user_logged_in() ) {
				$user = wp_get_current_user();
				$display_name = $user->display_name ? $user->display_name : $user->user_login;
				$items .= '<li class="menu-item menu-item-has-children logout"><a href="/your-membership"><i class="fa fa-user"></i> ' . $display_name . '</a>';
				$items .= '<ul class="menu-depth-1 sub-menu sub-nav-group" data-limit-columns="2">';
				$items .= '<li class="menu-item logout"><a href="' . wp_logout_url( get_site_url() ) . '"><i class="fa fa-times"></i> logout</a></li>';
				$items .= '</ul>';
				$items .= '</li>';
			} else {
				$items .= '<li class="menu-item login"><a href="/login"><i class="fa fa-user"></i> login</a>';
			}

		}

		return $items;
	}

	static public function rcp_login_url( $url, $redirect ) {
    	$url = get_site_url() . '/login';
    	return $url;
	}

	static public function ga_script() {
    	?>
			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121059717-1"></script>
			<script>
	          window.dataLayer = window.dataLayer || [];
	          function gtag(){dataLayer.push(arguments);}
	          gtag('js', new Date());

	          gtag('config', 'UA-121059717-1');
			</script>
		<?php
	}
}