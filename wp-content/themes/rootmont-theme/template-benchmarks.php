<?php
/**
 * Template Name: Performance Benchmark
 */

if ( ! is_user_logged_in() || ! rcp_user_has_access( $user->ID, 2 ) ) {
	$location = '/login';
	if ( ! rcp_user_has_access( $user->ID, 2 ) ) {
		$location .= '?user_lvl';
	}
	wp_safe_redirect( $location );
}

get_header();
the_post();


global $is_active_user, $user_membership_status, $user_membership, $user_membership_id;

$benchmark_type = get_field( 'performance_breakdown_type' );
$tables         = get_benchmark_data( $benchmark_type );
$column_order   = [ 'mean', 'std', 'min', '25%', '50%', '75%', 'max', 'count' ];

?>
<!-- ACTIVE USER -->
<div class="fl-content-full container-fluid">
	<div class="row">
        <div class="fl-content">
            <div class="col-md-10 col-md-offset-1">
                <header class="fl-post-header">
                    <h1 class="fl-post-title" itemprop="headline"><?php the_title(); ?></h1>
					<?php edit_post_link( _x( 'Edit', 'Edit page link text.', 'fl-automator' ) ); ?>
                </header><!-- .fl-post-header -->

                <div id="benchmark-tables-wrapper">
                    <?php $i = 0; foreach( $tables as $key => $table ) : ?>
	                    <?php $key = key( $table ); ?>
                        <div class="panel-group" id="accordion-<?php echo $key; ?>" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading<?php echo $i; ?>">
                                    <h4 class="panel-title">
                                        <a <?php echo $i !== 0 ? 'class="collapsed"' : ''; ?> role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
											<?php echo str_replace( '_', ' ', $key ); ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse <?php echo $i === 0 ? 'in' : ''; ?>" role="tabpanel" aria-labelledby="heading<?php echo $i; ?>">
                                    <div class="panel-body">
                                        <table id="dataTable" class="display dataTable enterprise" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th></th>
												<?php foreach( $column_order as $col ) : ?>
                                                    <th><?php echo 'count' === $col ? 'Sample Size' : $col; ?></th>
												<?php endforeach; ?>
                                            </tr>
                                            </thead>
                                            <tbody>
											<?php
                                            foreach( $table->$key as $table_row_values ) :
                                                $table_row_key = key( $table_row_values );
                                                if ( 'global' === $table_row_key ) {
                                                    continue;
                                                }
                                            ?>
                                                <tr>
                                                    <td>
														<?php echo $table_row_key; ?>
                                                    </td>
                                                    <?php foreach( $column_order as $col ) : ?>
                                                        <td><?php echo rootmont_number( $table_row_values->$table_row_key->{ $col } ); ?></td>
													<?php endforeach; ?>
                                                </tr>
											<?php endforeach; ?>
											<tr>
											    <td>
											        All Cryptocurrencies
											    </td>
												<?php foreach( $column_order as $col ) : ?>
                                                    <td><?php echo rootmont_number( $table_row_values->{ 'global' }->$col ); ?></td>
												<?php endforeach; ?>
											</tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php $i++; endforeach; ?>
                </div>
            </div>
        </div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.bootstrap4.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/keytable/2.5.0/css/keyTable.bootstrap4.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.js"></script>
<?php get_footer(); ?>
