<div id="coin-scrollspy">
	<ul class="nav navbar-default menu coin-subnav">
        <li class="menu-item">
            <a href="#content">
                Top
            </a>
        </li>
		<li class="menu-item">
			<a href="#performance-summary">
				Performance Summary
			</a>
		</li>
		<li class="menu-item">
			<a href="#description">
				Description
			</a>
		</li>
		<?php if ( rcp_user_has_access( 0, 1 ) || rcp_user_has_access( 0, 2 ) ) : ?>
        <li class="menu-item">
            <a href="#quantitative-performance">
                QUANTITATIVE PERFORMANCE
            </a>
        </li>
        <?php endif; ?>
		<?php if ( rcp_user_has_access( 0, 2 ) ) : ?>
		<li class="menu-item">
			<a href="#historical-performance">
				HISTORICAL PERFORMANCE
			</a>
		</li>
		<li class="menu-item">
			<a href="#upside-downside">
				CAPTURE RATIO
			</a>
		</li>
		<li class="menu-item">
			<a href="#portfolio-theory">
				PORTFOLIO THEORY
			</a>
		</li>
        <?php endif; ?>
	</ul>
</div>