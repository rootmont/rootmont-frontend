<?php
global $rootmontCoins, $rootmontDashboard;

$user = wp_get_current_user();

if ( ! is_user_logged_in() || ! rcp_user_has_access( $user->ID, 2 ) ) {
    $location = '/login';
    if ( ! rcp_user_has_access( $user->ID, 2 ) ) {
        $location .= '?user_lvl';
    }
	wp_safe_redirect( $location );
}

get_header();


$dashboard = $rootmontDashboard->dashboard;

global $user_data;
$user_data = $dashboard->get_user_info( $user->ID );

//var_dump( get_user_meta( $user->ID, '_rootmont_dashboard_charts', true ) );

?>

<div class="fl-content-full container-fluid">
	<div class="row">
		<div class="fl-content col-md-12">
            <!-- Start Coin Table -->
			<?php get_template_part( 'template-parts/dashboard', 'coin-table' ); ?>
            <!-- /End Coin Table -->
		</div>
	</div>
</div>

<hr/>

<div class="fl-content-full container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <script>
                rootmont.dashboardCharts();
            </script>
            <dashboard-root></dashboard-root>
        </div>
    </div>
</div>

<hr/>

<div class="fl-content-full container">
    <div class="row">
        <div class="col-sm-2 range-selector">
            <label for="ntx-movers-range">
                Range:
            </label>
        </div>
        <div class="col-sm-10">
            <select title="ntx-movers-range" name="ntx-movers-range" id="movers-range" class="form-control">
                <option value="1">1 DAY</option>
                <option value="7" selected>7 DAYS</option>
                <option value="30">30 DAYS</option>
                <option value="90">90 DAYS</option>
            </select>
        </div>
    </div>
    <!-- Start Movers -->
    <div class="row">
        <div class="col-sm-6">
            <?php get_template_part( 'template-parts/dashboard', 'price-movers' ); ?>
        </div>
        <div class="col-sm-6">
	        <?php get_template_part( 'template-parts/dashboard', 'ntx-movers' ); ?>
        </div>
    </div>
    <!-- /End Movers -->

</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.bootstrap4.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/keytable/2.5.0/css/keyTable.bootstrap4.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.css"/>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>-->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/keytable/2.5.0/js/dataTables.keyTable.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.js"></script>-->
<?php get_footer(); ?>
