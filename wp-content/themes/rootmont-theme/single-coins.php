<?php get_header();
global $post, $rootmontCoins, $coin_info_data, $is_active_user;

$symbol = get_field( 'symbol' );
$name = get_the_title();
//$cap_id = get_field( 'coin_market_cap_id' );

$cache_bust = isset( $_GET, $_GET['CACHEBUST'] );

$request = new \WP_REST_Request( 'GET', '/rootmont/v1/coin-info/' );
$request->set_query_params( [
    'symbol'     => $symbol,
    'name'       => $name,
//    'cap_id'     => $cap_id,
    'cache_bust' => $cache_bust,
] );

$coin_info_response = rest_ensure_response( $rootmontCoins->api->get_coin_info( $request ) );

if ( is_wp_error( $coin_info_response ) ) {
    var_dump( $coin_info_response->get_error_message() );
    get_footer();

    return;
}

$coin_info_data = $coin_info_response->get_data();
$coin_info_data['symbol'] = $symbol;
?>

<section id="content" role="main">
    <div class="container-fluid">
        <div class="row">
            <div class="page-content-wrapper col-sm-12">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <section class="entry-content">
                        <!-- Coin Header -->
                        <?php
                        get_template_part( 'template-parts/coin-info', 'header' );
                        ?>
                        <!-- / Coin Header -->

                        <!-- Coin Market Cap -->
                        <div id="market-cap">
                            <?php
                            get_template_part( 'template-parts/coin-info', 'market-cap' );
                            ?>
                        </div>
                        <!-- / Coin Market Cap -->

                        <!-- Percentile / Summary -->
                        <?php if ( ! empty( $coin_info_data['coin_info']->percentiles ) ) : ?>
                            <div id="performance-summary">
                                <?php get_template_part( 'template-parts/coin-info', 'percentiles' ); ?>
                            </div>
                        <?php endif; ?>
                        <!-- / Percentile Summary -->

                        <!-- Price Charts -->
                        <div id="">
                            <?php
                            get_template_part('template-parts/coin-info','price-charts');
                            ?>
                        </div>
                        <!-- / Price Charts -->

                        <!-- Description -->
                        <div id="description">
                            <?php get_template_part( 'template-parts/coin-info', 'description' ); ?>
                        </div>
                        <!-- / Description -->

                        <!-- Quantitative Performance -->
                        <div id="quantitative-performance">
                            <?php get_template_part( 'template-parts/coin-info', 'quant-performance' ); ?>
                        </div>
                        <!-- / Quantitative Performance -->

                        <?php if ( ! is_user_logged_in() ) : ?>
                            <a href="/pricing">
                                <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() . '/images/premium-paid-preview.jpg'; ?>" alt="Upgrade Now!" />
                                <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() . '/images/quantitative-performance-preview2.jpg'; ?>" alt="Upgrade Now!" />
                            </a>
                        <?php endif; ?>

                        <?php if ( rcp_user_has_access( 0, 1 ) || rcp_user_has_access( 0, 2 ) ) : ?>

                            <?php if ( ! rcp_user_has_access( 0, 2 ) ) : ?>
                                <a href="/pricing">
                                    <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri() . '/images/premium-paid-preview.jpg'; ?>" alt="Upgrade Now!" />
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if ( rcp_user_has_access( 0, 2 ) ) : ?>
                            <!-- Historical Performance -->
                            <div id="historical-performance">
                                <?php get_template_part( 'template-parts/coin-info', 'historical-performance' ); ?>
                            </div>
                            <!-- / Historical Performance -->

                            <!-- Upside & Downside Ratio -->
                            <div id="upside-downside">
                                <?php get_template_part( 'template-parts/coin-info', 'upside_downside-table' ); ?>
                            </div>
                            <!-- / Upside & Downside Ratio -->

                            <!-- Portfolio Theory -->
                            <div id="portfolio-theory">
                                <?php get_template_part( 'template-parts/coin-info', 'portfolio-table' ); ?>
                            </div>
                            <!-- / Portfolio Theory -->
                        <?php endif; ?>
                    </section>
                </article>
            </div>
        </div>
    </div>
</section>

<!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">-->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

<!-- Disclosure -->
<?php
    get_template_part( 'template-parts/coin-info', 'disclosure' );
?>
<!-- / Disclosure -->
<?php get_footer(); ?>
