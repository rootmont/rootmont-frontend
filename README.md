# Rootmont: Local Development Setup and Guideline.

In order to start developing or making changes in local is needed to complete the following steps:

## Note: It works for MACOS and most UNIX machines.

# GETTING STARTED

# Install Docker.

    sudo apt-get update

SET UP THE REPOSITORY

Update the apt package index:

    $ sudo apt-get update

Install packages to allow apt to use a repository over HTTPS:

    $ sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
        
Add Docker’s official GPG key:

    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

Verify that you now have the key with the fingerprint 9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88, by searching for the last 8 characters of the fingerprint.

    $ sudo apt-key fingerprint 0EBFCD88
 
INSTALL DOCKER CE

Update the apt package index.

    $ sudo apt-get update

Install the latest version of Docker CE and containerd, or go to the next step to install a specific version:

    $ sudo apt-get install docker-ce docker-ce-cli containerd.io

Add docker to sudo users:

    sudo usermod -a -G docker $USER

Restart user session.

    Close and restart user linux session, in order to preload users again.

# Install Lando.

    Lando is designed to work on a wide range of computers. Here are some basic guidelines to ensure your Lando experience is as smooth as possible.

# HARDWARE CONSIDERATIONS

    ## OPERATING SYSTEM

    macOS 10.11 (El Capitan) or newest
    Windows 10 Pro+ or equivalent (eg Windows 10 Enterprise) with Hyper-V running
    Linux with kernel version 4.x or higher

    ## HARDWARE REQUIREMENTS
    Modern x64 architecture multi-core processor
    8GB RAM
    10GB+ of available disk space

    ## HARDWARE RECOMMENDATIONS

    8-core processor
    16GB+ RAM
    100GB+ of available disk space

    ## DOCKER ENGINE REQUIREMENTS

    Please also verify you meet the requirements of our Docker engine backend.

    Linux Docker engine requirements
    Docker for Mac requirements
    Docker for Windows requirements

# Once installed, with all dependencies. Go through interactive prompts to get your site from pantheon

    lando init --source pantheon

# OR do it non-interactively
    lando init \
    --source pantheon \
    --pantheon-auth "$PANTHEON_MACHINE_TOKEN" \
    --pantheon-site "$PANTEHON_SITE_NAME"

# Start it up
    lando start

# Import your database and files
    lando pull

# List information about this app.
    lando info

# Fixing any cert problem using:

    sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ~/.lando/certs/lndo.site.pem

# Once is done, please go to:

    APPSERVER_NGINX URLS  https://localhost:32788
                        http://localhost:32789
    EDGE URLS             http://localhost:32781
                        http://rootmont.lndo.site
                        https://rootmont.lndo.site
    EDGE_SSL URLS         https://localhost:32790


# WordPress

This is a WordPress repository configured to run on the [Pantheon platform](https://pantheon.io).

Pantheon is website platform optimized and configured to run high performance sites with an amazing developer workflow. There is built-in support for features such as Varnish, Redis, Apache Solr, New Relic, Nginx, PHP-FPM, MySQL, PhantomJS and more. 

## Getting Started

### 1. Spin-up a site

If you do not yet have a Pantheon account, you can create one for free. Once you've verified your email address, you will be able to add sites from your dashboard. Choose "WordPress" to use this distribution.

### 2. Load up the site

When the spin-up process is complete, you will be redirected to the site's dashboard. Click on the link under the site's name to access the Dev environment.

![alt](http://i.imgur.com/2wjCj9j.png?1, '')

### 3. Run the WordPress installer

How about the WordPress database config screen? No need to worry about database connection information as that is taken care of in the background. The only step that you need to complete is the site information and the installation process will be complete.

We will post more information about how this works but we recommend developers take a look at `wp-config.php` to get an understanding.

![alt](http://i.imgur.com/4EOcqYN.png, '')

If you would like to keep a separate set of configuration for local development, you can use a file called `wp-config-local.php`, which is already in our .gitignore file.

### 4. Enjoy!

![alt](http://i.imgur.com/fzIeQBP.png, '')



